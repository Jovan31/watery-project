from WateryModules import *
from minorticks import minorticks

'''
This is just a code that calles WAnalysis from WateryModules.
Since WAnalysis can change quite often, this is written so only the analysis
part can be executed without the need to re-run the entire WateryFlow code.
'''

# This is just for measuring the time it takes to execute the code
datum = datetime.datetime.now()

# choose a halo
halo = 'A'
print

# read the data
database = '/data/users/jovan/Aquarius/PresentDay/halo-' + halo + '/'
datafile = 'WateryFlowRes.cPickle'

# Defining the network of observers (degrees)
obsnet = WPerspective(principalAxis=True, datadir=database, halo=halo)

print 'Running WAnalysis..'
print
WAnalysis(obsnet = obsnet,
          datadir = database,
          datafile = datafile,
          halo = halo)

datum1 = datetime.datetime.now()
print
print 'WateryAnalysis successfully completed in: ', datum1 - datum
print 'The rent is too DAMN high!'
print
