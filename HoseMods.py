import pylab as p
from scipy import *
import beerstats as bs
from matplotlib import cm
from minorticks import minorticks
from scipy.optimize import curve_fit
from matplotlib.patches import Circle
from WFunctions import coords_transform
from matplotlib.backends.backend_pdf import PdfPages

################################################################################

def SimpleKinematics(vel,posAng,vsys):
  '''
  Simple algorithm for determining the kinematics of a system
  following the perscription in the oldschool Kinematics code.

  arguments:
  vel    = velocities of the points [km/s]
  posAng = position angle of the points [radians]
  vsys   = the velocity of the system [km/s]

  returns:
  amp    = amplitude of rotation (absolute value) [km/s]
  eamp   = uncertainty of the amplitude [km/s]
  axi    = axis of rotation [deg]
  eaxi   = uncertainty in the axis of rotation [deg]
  sig    = rotation corrected velocity dispersion [km/s]
  esig   = uncertainty in the rotation corrected velocity dispersion [km/s]
  '''

  def kin_func(x,amp,axi):
    return vsys + amp*sin(axi-x)

  # First calculate the rotation of the system

  # the fitting happens here
  kin = curve_fit(kin_func,posAng,vel,maxfev = 1000000,full_output=True)
  # read in the results from the fitting
  amp  = kin[0][0]
  eamp = sqrt(kin[1][0,0])
  axi  = mod(rad2deg(kin[0][1]),360)
  eaxi = rad2deg(sqrt(kin[1][1,1]))

  # Now calculate the dispersion of the system

  # rotation corrected velocities
  rotcor=kin[2]['fvec']

  #mean velocity with jackknife uncertainty
  # beerslocation, locationerror = bs.location(gal_vel[index])
  #velocity dispersion (rotation corrected ) with jackknife uncertainty
  sig, esig = bs.scale(rotcor)

  if amp >= 0:
    pass
  else:
    amp = abs(amp)
    axi = mod(axi+180.,360)

  return amp, eamp, axi, eaxi, sig, esig



################################################################################
def SimpleHaloPlot(GCx, GCy,
                   GCvel, GCpa,
                   amp, axi, sig, vsys,
                   halo, mtrans, nbins = 300,
                   xlimit = [150, -150], ylimit = [-150, 150],
                   spatialRange=([-150,150],[-150,150]),
                   oname = '/Users/users/jovan/Desktop/fig.pdf'):

  '''
  This halo plot shows the result of the simple kinematics algorithm.
  For now it is only for testing and experimenting.
  Greatly inspired by WHaloPlot.

  GCx        = the x positions of the GCs in standard-like coords
  GCy        = the y positions of the GCs in standard-like coords
  GCvel      = velocities of the GCs
  GCpa       = the position angle of the GCs, in radians
  halo       = which halo is being analysed, used to set up the path to
               relevant input files
  amp        = rotation amplitude of the system [km/s]
  axi        = position angle of the rotation axis [deg]
  axi        = velocity dispersion of the mock GC system [km/s]
  vsys       = systemic velocity of the system (the galaxy not the GCs) [km/s]
  mtrans     = the transformation matrix, used to project the dark matter
               axis onto the plot
  oname      = output name (this will always be a .pdf file)
  '''

  print 'Plotting the halo...'

  # initializing the plot output
  plotoutput = PdfPages(oname)

  # Page One
  fulldata = load('/data/users/jovan/Aquarius/PresentDay/halo-' + halo +
                '/dataProducts/hose/Particle.ObservedData.npy')

  sx       = fulldata[0]
  sy       = fulldata[1]

  # clean up
  del fulldata

  # creating the 2d map.
  Z, xedges, yedges = histogram2d(sx,
                                  sy,
                                  bins = nbins,
                                  range = spatialRange)

  # setting the figure properties
  figga = p.figure(figsize = (9,6),dpi = 100,facecolor = 'w',edgecolor = 'K')
  p.axes().set_aspect('equal')

  # limits and labels
  p.xlim(xlimit)
  p.ylim(ylimit)
  minorticks(ftsize = 8, maw = 1.5, miw = 1.1)
  p.xlabel(r'pseudo-$\xi$ [kpc]',  fontsize = 8)
  p.ylabel(r'pseudo-$\eta$ [kpc]', fontsize = 8)

  # defining the extent of the map
  extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]

  # create a log map, since the scale is difficult to set otherwise..
  Z = log10(Z.T+1.)

  # creating the density map
  p.imshow(Z,
           origin = 'lower',
           cmap = 'binary',
           extent = extent,
           vmin = 0,
           vmax = 0.75*(Z).max())

  # Creating the colour mapped points
  points = zip(GCx, GCy)
  points3 = sorted(zip(GCvel,points))

  for i,pp in enumerate(points3):
    GCvel[i] = pp[0]
    GCx[i] = pp[1][0]
    GCy[i] = pp[1][1]

  # the velocity coloured points, representing the GCs
  p.scatter(GCx,
            GCy,
            c = GCvel,
            edgecolor = 'black',
            lw = 0.5)

  # two circles to guide the eye
  rad30 = Circle((0, 0),
                  radius =30,
                  facecolor = 'none',
                  edgecolor = 'purple',
                  lw = 1.0)
  rad100 = Circle((0, 0),
                  radius =100,
                  facecolor = 'none',
                  edgecolor = 'purple',
                  lw = 1.0)

  # stuff
  ax = p.gca()
  fig = p.gcf()

  # setting up additional parameters for the circles
  p.setp(rad30, ls='dashed')
  p.setp(rad100, ls='dashed')
  # adding the circles to the plot
  fig.gca().add_artist(rad30)
  fig.gca().add_artist(rad100)

  # add labels
  if len(GCvel) >5:
    label1 = p.text(-225, 50, 'Line-of-sight velocity [km/s]',
                    rotation = 'vertical',fontsize = 8)
    cbr = p.colorbar()
    cbr.ax.tick_params(labelsize=8)
  else:
    pass

  # adding the legend, which is in fact nothing but the values of the fit..
  p.scatter([],[],color='white',label='Amp '+str('%3.2f' % (amp))+' [km/s]')
  p.scatter([],[],color='white',label='Axi '+str('%3.2f' % (axi))+' [deg]')
  p.scatter([],[],color='white',label='Sig '+str('%3.2f' % (sig))+' [km/s]')

  p.legend(loc = 2,frameon=False, prop={'size':5})

  # now adding the little picture in picture (pip) (inset axes) for the
  # orientiation of the dark matter principal axes..

  # first, calculate the coordinates of the principal axes in the given
  # projection
  pa = load('/data/users/jovan/Aquarius/PresentDay/halo-' + halo +
            '/principalAxis/pa'+halo+'.npy')
  # converting from spherical to cartesian coords
  xx = sin(pa[0])*cos(pa[1])
  yy = sin(pa[0])*sin(pa[1])
  zz = cos(pa[0])
  # projecting the axes onto the plane of observations..
  xo = zeros(3)
  yo = zeros(3)
  for i in range(3):
    xo[i], yo[i] = coords_transform(mtrans, array([xx[i],yy[i],zz[i]]))
  # labels for the orientation vectors
  lbls = ['v1', 'v2','v3']
  # calculating the offsets for the labels of the orientation vectros
  offx = zeros(3)
  offy = zeros(3)
  # the embedded plot starts here
  pip = p.axes([.60, .1, .15, .15], axisbg='w',frameon=False)
  for i in range(3):
    if offx[i] >=0:
      offx[i] += 0.2
    else:
      offx[i] -= 0.2
    if offy[i] >=0:
      offy[i] += 0.2
    else:
        offy[i] -= 0.2
    p.arrow(0,0,xo[i],yo[i],head_width=0.07,head_length=0.07,fc='k',ec='k')
    p.annotate(lbls[i], xy=(xo[i],yo[i]),
               xytext=(xo[i]+offx[i],yo[i]+offy[i]), fontsize = 5)
  p.xlim([1.5,-1.5])
  p.ylim([-1.5,1.5])
  p.setp(pip, xticks=[], yticks=[])

  # first page is done
  plotoutput.savefig()

  # Page two: the remnants of the kinfig
  p.figure(figsize=(5,5),dpi=100,facecolor='w',edgecolor='K')
  p.xticks([0,90,180,270,360])
  p.xlabel(r'$\theta_{0}$ ' + '[deg]', fontsize = 8)
  p.ylabel('$V_{\\rm{los}} $ [km s$^{-1}$]', fontsize = 8)
  p.scatter(rad2deg(GCpa),
            GCvel,
            marker = 'o',
            s = 3,
            color = 'black')
  # dotted line through the mean velocity of the system
  p.plot([0,360], [vsys,vsys], color = 'black', lw = 0.3, ls = ':')
  # showing the rotation
  xvalues = linspace(0., 360., 3600.)
  yvalues = vsys + amp * sin(deg2rad(xvalues) - deg2rad(axi))
  p.plot(xvalues, yvalues, color = 'black', lw = 1)
  p.xlim([0,360])
  tempval = max(max(yvalues), max(abs(GCvel)))
  p.ylim([-(tempval+tempval*0.1),tempval+tempval*0.1])
  minorticks(ftsize = 6, maw = 1,miw = 0.7)

  # Page two is ready, time to save
  plotoutput.savefig()

  # close the document
  plotoutput.close()

  print 'Plot created successfully!'
  print

################################################################################