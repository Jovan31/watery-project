import sys
import datetime
from WateryModules import *

'''
Watery Profiles.
This code will plot the radial number density profiles of M31 together 
with the profile of an Aquarius 'stellar' halo, as a function of position 
of the observer. This is just to see if the kinematics of the halo is similar 
to the kinematics of M31 when the radial profiles are similar/different.

Also determined is the average profile of the halo from all observed directions 
and is compared to the profile of M31.

I've added the true 3D profile to the plots, to compare what we 'observe' to 
what it actually is in nature.
'''

# This is just for measuring the time it takes to execute the code
datum = datetime.datetime.now()

# choosing a halo
halo = 'A'
print 'Aquarius halo chosen: ', halo

# location of the data
database = '/data/users/jovan/Aquarius/PresentDay/halo-' + halo + '/'
dataProducts = database + 'dataProducts/'

# Selecting the particles which might contain GCs
WParticleSelector(database + 'full-data-binary.npy',
                  dataProducts + 'Particle.Data.npy',backfeed = False)

# Centring the particles, so that the origin is at the most dense point
WGalaxyCentring(dataProducts + 'Particle.Data.npy')

# Scaling the galaxy to Aq-A
WGalaxyScaling(dataProducts + 'Particle.Data.npy',
               dataProducts + 'ScaledParticle.Data.npy',
               halo)

# Calculating the 3D profile for the selected halo
data3d = load(dataProducts + 'ScaledParticle.Data.npy')
x3d    = data3d[0]
y3d    = data3d[1]
z3d    = data3d[2]
r3d    = sqrt(x3d**2.0 + y3d**2.0 + z3d**2.0)

# clean up 
del x3d, y3d, z3d, data3d

# choosing the bins and creating the profile
bins = arange(30.,150.,1.)
bv3d, be3d = histogram(r3d, bins = bins)
# calculating the volume of each bin
volume = zeros(len(bv3d))
for q in range(len(bins)-1):
  volume[q] = 4.*pi*(be3d[q+1]**3 - be3d[q])/3.
# find the middle of each bin (bm)
mb = zeros(len(bv3d))
for q in range(len(bins)-1):
  mb[q] = (be3d[q] + be3d[q+1]) * 0.5

# the profile in 3d
pr3d = bv3d / volume

# Now, find the profile for each perspective
# Defining the network of observers (degrees)
obsnet = WPerspective(principalAxis=True, datadir=database, halo=halo)
rad    = 1000.

# Define the master figure
p.figure(figsize=(8,11), dpi = 120, facecolor = 'w', edgecolor = 'k')
# define the grid along which the subplots will be aranged
gs = p.GridSpec(5,4)

# these control the location of the subplots
ii = 0
jj = 0

# This is for the cumulative profile (for the average profile)
# average bin balue (abv). I am just initializing the variable here
abv = 0

# Start of the main phase.. loop through each observer
for i,v in enumerate(obsnet):
  x_obs = int(round(rad*sin(deg2rad(v[0]))*cos(deg2rad(v[1]))))
  y_obs = int(round(rad*sin(deg2rad(v[0]))*sin(deg2rad(v[1]))))
  z_obs = int(round(rad*cos(deg2rad(v[0]))))

  # Observing the particles, given a specific location of the observer
  observer = array([x_obs, y_obs, z_obs])

  # The transformation matrix, i.e. how the observer sees the system
  mtrans = transformation_matrix(observer)

  # Observing the data
  WDataObservation(dataProducts + 'Particle.Data.npy',
                   mtrans, observer,
                   dataProducts + 'Particle.ObservedData.npy',
                   backfeed = False)

  # Here starts the Profiler part.
  # First need to determine the profile of the Aq halo as seen from this 
  # particular observer, and then plot it together along with the M31 halo

  # Determining the profile of the Aq halo for this perspective
  # Read in the master data for this observed halo
  data  = load(dataProducts+'Particle.ObservedData.npy')
  # only need the projected radius of the particles
  Rproj = data[4]
  # determine the particles which lie in the appropriate radial range
  ind   = (Rproj >= 30.) & (Rproj < 150.)

  # clean up
  del Rproj

  # select the appropriate data
  Rproj = data[4][ind]

  # clean up
  del data, ind

  # measure the 'star' profile
  # create the bin edges
  bins = arange(30.,150.,1)
  # bin value (bv), bin edge (be)
  bv, be = histogram(Rproj, bins = bins)

  # find the middle of each bin (bm)
  mb = zeros(len(bv))
  for q in range(len(bins)-1):
    mb[q] = (be[q] + be[q+1]) * 0.5

  # calculating the area of each bin
  # (since i've added the 3D profile, this has changed to volume)
  area = zeros(len(bv))
  for q in range(len(bins)-1):
    # area[q] = pi*(be[q+1]**2.0 - be[q]**2.0)
    area[q] = 4.*pi*(be[q+1]**3 - be[q])/3.

  # this is the profile for this observer
  pr = bv / area
  # adding to the cumulative profile
  abv += pr

  # Now define the profile of M31 
  # (this is highly untested and likely to be problematic)
  m31 = log10(mb**(-3.34)) + 3.3
  # for comparison, plot profiles that have power law of -3p(3) and -4(p4)   
  p3 = log10(mb**(-3.00)) + 3.3
  p4 = log10(mb**(-4.00)) + 3.3

  # Now do the plotting thingy
  # Determining the location of the current subplot
  if jj == 4:
    jj = 0
    ii += 1
    # print ii, jj
    p.subplot(gs[ii,jj])
    jj += 1
  else:
    # print ii, jj
    p.subplot(gs[ii,jj])
    jj += 1

  # custom x-tics, in case the AutoLocator messes things up
  p.xticks(range(0,160,10))
  # label the subplots
  p.xlabel(r'$R_{\rm proj}$ [kpc]', fontsize  = 6)
  p.ylabel(r'$log_{10}({N/kpc^2})$', fontsize  = 6)
  # limits on the ais
  p.xlim([0,160])
  # p.ylim() for now let the y axis run wild

  # The actual plotting of the data
  # The Aq halo
  p.plot(mb, log10(pr), 
         c = 'black',
         lw = 1, 
         ls = '-',
         label = 'System ' + str(int(v[0])) + ' ' + str(int(v[1])))
  p.plot(mb, m31, c = 'blue', lw = 1, ls = '-', label = 'M31\'s profile')
  p.plot(mb, p3, c = 'blue', lw = 0.5, ls = ':', label = r'$\gamma=3$')
  p.plot(mb, p4, c = 'blue', lw = 0.5, ls = '--', label = r'$\gamma=4$', dashes=(2,2))
  p.plot(mb,log10(pr3d), 
       c = 'magenta', 
       lw = 1,
       ls = '-.',
       dashes = (2,2),
       label = '3D profile')
  # legend
  p.legend(loc = 1, frameon=False, prop={'size':4.5})
  # configuring the ticks with my custom rutine
  minorticks(ftsize=4, maw=1.0,mal=2.0,miw=0.5,mil=1.0)

p.tight_layout()
# The plotting is hopefully done by now
# Just need to save the figure to disk
# output name (oname)
oname = database + 'plots/haloProfiles' + halo + '.pdf'
p.savefig(oname, dpi =120,bbox_inches = 'tight')
p.clf()


# Now plot the average profile
p.figure(figsize=(6,4), dpi = 120, facecolor = 'w', edgecolor = 'k')

# nicing up the axis
# custom x-tics, in case the AutoLocator messes things up
p.xticks(range(0,160,10))
# label the subplots
p.xlabel(r'average $R_{\rm proj}$ [kpc]', fontsize  = 6)
p.ylabel(r'$log_{10}({N})$', fontsize  = 6)
# limits on the ais
p.xlim([0,160])
# p.ylim() for now let the y axis run wild

# average profile
apr = abv/len(obsnet)

# at this point, also save the average profile to disk, for future needs
save(dataProducts +'avgProfile'+halo,apr)

p.plot(mb, log10(apr),
       c = 'black',
       lw = 1,
       ls = '-',
       label = 'projected profile')
p.plot(mb, m31, c='blue', lw=1.0, ls='-',  label='M31\'s profile')
p.plot(mb, p3,  c='blue', lw=0.5, ls=':',  label=r'$\gamma=3$')
p.plot(mb, p4,  c='blue', lw=0.5, ls='--', label=r'$\gamma=4$', dashes=(2,2))
p.plot(mb,log10(pr3d), 
       c = 'magenta', 
       lw = 1,
       ls = '-.',
       dashes = (2,2),
       label = '3D profile')
# legend
p.legend(loc = 1, frameon=False, prop={'size':5})
# configuring the ticks with my custom rutine
minorticks(ftsize=4, maw=1.0,mal=2.0,miw=0.5,mil=1.0)

# output name
oname = database + 'plots/avgHaloProfile' + halo + '.pdf'
# save the figure
p.savefig(oname, dpi = 120,bbox_inches = 'tight')
p.clf()


datum1 = datetime.datetime.now()
print
print 'WateryProfiles successfully completed in: ', datum1 - datum
print 'The rent is too DAMN high!'
print






