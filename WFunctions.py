'''
Functions needed for the Aquarius project
'''
import os
import sys
import cPickle
import datetime
import matplotlib
matplotlib.use('Agg')
import pylab as p
from numpy import *
import random as rd
import beerstats as bs
from matplotlib import cm
import WateryModules as WM
import scipy.linalg as linalg
from minorticks import minorticks
from scipy.integrate import simps
from astropy.stats import sigma_clip
from matplotlib.patches import Circle
import scipy.interpolate as interpolate
from matplotlib.backends.backend_pdf import PdfPages
from confidenceLimits import confidence, zeroCase, circularCase
from scipy.stats import binned_statistic_2d as bin_st_2d, spearmanr, rankdata
################################################################################
def los_velocity(pos,vel,origin):
    '''
    Calculates the line of sight velocity for a GCs provided an origin
    (the point of view of the observer)

    arguments:
    pos = array-like, 3d positions
    vel = array-like, 3d velocities
    origin = the location of the observer

    returns the line-of-sight velocity
    '''

    # shift for origin
    shifted_x = pos[0] - origin[0]
    shifted_y = pos[1] - origin[1]
    shifted_z = pos[2] - origin[2]

    # calculate spherical polar coordinates
    r = (shifted_x**2.0 + shifted_y**2.0 + shifted_z**2.0)**0.5
    theta = arccos(shifted_z/r) - deg2rad(90.)
    phi = arctan(shifted_y/shifted_x)

    # this is dot product between the radius vector of the position of the GC as
    # seen from the origin with the velocity vector of the GC, effectively
    # projecting the velocity along the radius vector (line-of-sight)
    los = cos(theta)*cos(phi) * vel[0] + cos(theta) * sin(phi) * vel[1] - \
          sin(theta) * vel[2]

    # return the result
    return los

################################################################################
def perpendicular_vector(v):
    r'''Finds an arbitrary perpendicular normalized vector to *v*.'''
    # for two vectors (x, y, z) and (a, b, c) to be perpendicular,
    # the following equation has to be fulfilled
    #     0 = ax + by + cz

    # x = y = z = 0 is not an acceptable solution
    if v[0] == v[1] == v[2] == 0:
        raise ValueError('zero-vector')

    # If one dimension is zero, this can be solved by setting that to
    # non-zero and the others to zero. Example: (4, 2, 0) lies in the
    # x-y-Plane, so (0, 0, 1) is orthogonal to the plane.
    if v[0] == 0:
        return array([-1., 0., 0.])
    if v[1] == 0:
        return array([0., 1., 0.])
    if v[2] == 0:
        return array([0., 0., 1.])

    # arbitrarily set a = b = 1
    # then the equation simplifies to
    #     c = -(x + y)/z
    # normalizing the vector
    temp =  array([1., 1., -1.0 * (v[0] + v[1]) / v[2]])
    norm_vect = sqrt(temp[0]**2.0 + temp[1]**2.0 + temp[2]**2.0)
    return temp / norm_vect


################################################################################
def transformation_matrix(xdirection, verbose = False):
    '''
    This function determines the transformation of data to a new coordinate
    system where the observer is located along the xdirection (the x axis of the
    new coordinate system). The new coordinate system shares the origin with the
    original one. This transformation is done in order to observe the data from
    the point of view of the observer.
    '''
    # normalize xdirection
    norm_xdir = sqrt(xdirection[0]**2.0+xdirection[1]**2.0+xdirection[2]**2.0)
    xdirection = xdirection / norm_xdir

    # find the ydirection - any normalized vector perpendicular to dxirection
    ydirection = perpendicular_vector(xdirection)
    # check if ydirection is normalized
    if round((ydirection[0]**2.0+ydirection[1]**2.0+ydirection[2]**2.0)**0.5
             )!=1.0:
        raise ValueError('ydirection not normalized!')

    # now find the zdirection by taking the cross product of the
    # xdirection and  ydirection
    zdirection = cross(xdirection,ydirection)

    # normalize zdirection
    norm_zdir = sqrt(zdirection[0]**2.0+zdirection[1]**2.0+zdirection[2]**2.0)
    zdirection = zdirection/norm_zdir

    trans_matrix = matrix([xdirection,ydirection,zdirection])

    if verbose == True:
        print
        print 'Transformation matrix used:'
        print trans_matrix
        print
    else:
        pass

    # now create the transformation matrix
    return trans_matrix


################################################################################
def coords_transform(mtrans, position):
    '''
    This function takes a transformation matrix and position coordinates
    (an array) and returns a pair of new x and y coordinates, on the plane
    perpendicular to the line-of-sight, passing through the origin (basically
    the new ydirection = newx and new zdirection = new y).The distance to the
    particles, aka the xdirection is ignored.
    '''
    # converting the position array to a matrix array for the coming matrix
    # multiplication.
    position = matrix(position)
    new_position = mtrans * position.T
    new_position = array(new_position.T)
    new_position = new_position[0]
    return new_position[1], new_position[2]

################################################################################
def posAngCal(x,y):
    ''' Calculates the position angle (in radians) for set of input
    "standard"-like coordinates x,y. The position angles are measured in the
    astronomical standard, such that north is up and east is left.

    Upgraded to use the numpy arctan2 function. Smartly placing the x,y
    coordinates luckily results in recovering the position angle with the
    astronomy convention preserved.

    arguments
    x,y = array_like, set of input standar coordinates

    returns
    posAng = the position angle of an object in radians
    '''

    # check if arrays x and y are the same legnth
    try:
        len(x) == len(y)
    except ValueError:
        print "Oops! x and y arguments are of different length!"

    posAng = arctan2(x,y)
    posAng = array(posAng)
    posAng[posAng < 0] = posAng[posAng < 0] + 2*pi

    return posAng

################################################################################

def wmapper(halo, mtrans, GCx=[], GCy=[], GCvel=[], nbins = 300,
            xlimit = [150, -150], ylimit = [-150, 150],
            spatialRange=([-150,150],[-150,150])):

    '''
    Produces a density plot of the visible matter in the halo. If needed, the
    density contours of each progenitor are plotted as well. Created for the
    purpose of visualizing the 'stellar' streams of all relevant progenitors in
    an Aquarius halo It also plots the selected GCs, colour coded for their
    line-of-sight velocities. Specially written to work as an integral part of
    the WateryModules.

    Arguments:
    halo          = which halo is being analysed, used to set up the path to
                    relevant input files
    mtrans        = the transformation matrix, used to project the dark matter
                    axis onto the plot
    GCx           = array-like, x positions of the GCs in standard coords
    GCy           = array-like, y positions of the GCs in standard coords
    GCvel         = array-like, the line-of-sight velocities of the GCs
    nbins         = number of bins for visualisation of the map
    xlimit/ylimit = the axes limits of the resulting figure, they are lists
    contours      = if true, contours of the streams are displayed

    Version: 0.7

    '''

    # reading the key data
    fulldata = load('/data/users/jovan/Aquarius/PresentDay/halo-' + halo +
                '/dataProducts/Particle.ObservedData.npy')
    sx       = fulldata[0]
    sy       = fulldata[1]

    # clean up
    del fulldata

    # creating the 2d map.
    Z, xedges, yedges = histogram2d(sx,
                                    sy,
                                    bins = nbins,
                                    range = spatialRange)

    # setting the figure properties
    figga = p.figure(figsize = (9,6),dpi = 100,facecolor = 'w',edgecolor = 'K')
    p.axes().set_aspect('equal')

    # limits and labels
    p.xlim(xlimit)
    p.ylim(ylimit)
    minorticks(mtnum = 5, ftsize = 10, maw = 1.5, miw = 1.1)
    p.xlabel(r'pseudo-$\xi$ [kpc]',  fontsize = 12)
    p.ylabel(r'pseudo-$\eta$ [kpc]', fontsize = 12)

    # defining the extent of the map
    extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]

    # create a log map, since the scale is difficult to set otherwise..
    Z = log10(Z.T+1.)

    # creating the density map
    p.imshow(Z,
             origin = 'lower',
             cmap = 'binary',
             extent = extent,
             vmin = 0,
             vmax = 0.75*(Z).max())

    # before creating the colour mapped points, need to copy the data arrays
    cGCx   = GCx.copy()
    cGCy   = GCy.copy()
    cGCvel = GCvel.copy()

    # Creating the colour mapped points
    points = zip(cGCx, cGCy)
    points3 = sorted(zip(cGCvel,points))

    for i,pp in enumerate(points3):
        cGCvel[i] = pp[0]
        cGCx[i]   = pp[1][0]
        cGCy[i]   = pp[1][1]

    # the velocity coloured points, representing the GCs
    p.scatter(cGCx,
              cGCy,
              c = cGCvel,
              edgecolor = 'black',
              lw = 0.5)

    # two circles to guide the eye
    rad30 = Circle((0, 0),
                    radius =30,
                    facecolor = 'none',
                    edgecolor = 'purple',
                    lw = 1.0)
    rad100 = Circle((0, 0),
                    radius =100,
                    facecolor = 'none',
                    edgecolor = 'purple',
                    lw = 1.0)

    # stuff
    ax = p.gca()
    fig = p.gcf()

    # setting up additional parameters for the circles
    p.setp(rad30, ls='dashed')
    p.setp(rad100, ls='dashed')
    # adding the circles to the plot
    fig.gca().add_artist(rad30)
    fig.gca().add_artist(rad100)

    # add labels
    if len(GCvel) >3:
        label1 = p.text(-225, 50, 'Line-of-sight velocity [km/s]',
                        rotation = 'vertical',fontsize = 12)
        cbr = p.colorbar()
        cbr.ax.tick_params(labelsize=10)
        p.clim(-1.*max(abs(GCvel)), max(abs(GCvel)))
    else:
        pass


    # now adding the little picture in picture (pip) (inset axes) for the
    # orientiation of the dark matter principal axes..

    # set up the path and file to be read by the routine that determines 
    # the angular momentum of the halo
    # the data directory
    tmploc = '/data/users/jovan/Aquarius/PresentDay/halo-'+halo+'/dataProducts/'
    tmpfil = 'ScaledParticle.Data.npy'
    tmp1, tmp2, halo_angmom = WM.WInertia_fullhalo(tmploc,tmpfil)
    # normalize the halo angular momentum
    hh = halo_angmom/linalg.norm(halo_angmom)*-1
    # projecting the axes onto the plane of observations...
    xo, yo = coords_transform(mtrans, array([hh[0],hh[1],hh[2]]))
    # the label for the vector
    lbls = r'$L$'
    # calculating the offsets for the labels of the orientation vectros
    offx = 0.0
    offy = 0.0
    pip = p.axes([.60, .1, .15, .15], axisbg='w',frameon=False)
    if offx >=0:
        offx += 0.2
    else:
        offx -= 0.2
    if offy >=0:
        offy += 0.2
    else:
        offy -= 0.2
    p.arrow(0,0,xo,yo,head_width=0.07,head_length=0.07,fc='k',ec='k')
    p.annotate(lbls, xy=(xo,yo),
               xytext=(xo+offx,yo+offy), fontsize = 8)

    # # first, calculate the coordinates of the principal axes in the given
    # # projection
    # pa = load('/data/users/jovan/Aquarius/PresentDay/halo-' + halo +
    #           '/principalAxis/pa'+halo+'.npy')
    # # converting from spherical to cartesian coords
    # xx = sin(pa[0])*cos(pa[1])
    # yy = sin(pa[0])*sin(pa[1])
    # zz = cos(pa[0])
    # # projecting the axes onto the plane of observations..
    # xo = zeros(3)
    # yo = zeros(3)
    # for i in range(3):
    #     xo[i], yo[i] = coords_transform(mtrans, array([xx[i],yy[i],zz[i]]))
    # labels for the orientation vectors
    # lbls = ['v1', 'v2','v3']
    # calculating the offsets for the labels of the orientation vectros
    # offx = zeros(3)
    # offy = zeros(3)
    # # the embedded plot starts here
    # pip = p.axes([.60, .1, .15, .15], axisbg='w',frameon=False)
    # for i in range(3):
    #     if offx[i] >=0:
    #         offx[i] += 0.2
    #     else:
    #         offx[i] -= 0.2
    #     if offy[i] >=0:
    #         offy[i] += 0.2
    #     else:
    #         offy[i] -= 0.2
    #     p.arrow(0,0,xo[i],yo[i],head_width=0.07,head_length=0.07,fc='k',ec='k')
    #     p.annotate(lbls[i], xy=(xo[i],yo[i]),
    #                xytext=(xo[i]+offx[i],yo[i]+offy[i]), fontsize = 5)
    p.xlim([1.5,-1.5])
    p.ylim([-1.5,1.5])
    p.setp(pip, xticks=[], yticks=[])

    return figga

################################################################################

def sig_diff(a,b,da,db):
    '''
    Checks the number of standard deviations between a and b.
    da and db are the Gaussian uncertainties of a and b.
    The number of standard deviations is returned.
    '''

    temp_up = (abs(float(a) - float(b)))
    temp_down = (float(da)**2.0 + float(db)**2.0)**0.5

    return temp_up/temp_down

################################################################################
def quad_clust_counter(posAngle):
    '''
    This function is similar to the azym_sym_test, bit in only counts the
    number of clusters present in each of the quadrants. The idea here is that
    distribution of clusters will be analysed at some later stage, and at
    the point in which this function is used, only the number of clusters in
    each quadrant is required.

    Input:
    posAngle  = array_like, position angles of the GCs in radians
    Returns
    q1,q2,q3,q4 = the number of clusters in quadrant1, quad2, quad3 and quad 4
    '''

    # number of clusters in each of the 4 quadrants
    q1 = len(posAngle[(posAngle >= 0 ) & (posAngle < pi/2.)])
    q2 = len(posAngle[(posAngle >= pi/2.) & (posAngle < pi)])
    q3 = len(posAngle[(posAngle >= pi) & (posAngle < 3.*pi/2.)])
    q4 = len(posAngle[(posAngle >= 3.*pi/2.) & (posAngle < 2*pi)])

    return q1, q2, q3, q4

################################################################################
def azym_sim_test_mean(q1, q2, q3, q4, sig = 2):
    '''
    This test is proposed by Amina. The mean number of clusters amongst the
    four quadrants is found first. Then, it is check whether any quadrant
    deviates by more then \sig\ from that mean. Information is retained on
    whether the quadrant has an excess or a lack of clusters compared to the
    mean.

    arguments
    q1  = float, number of clusters found in quadrant 1
    q2  = float, number of clusters found in quadrant 2
    q3  = float, number of clusters found in quadrant 3
    q4  = float, number of clusters found in quadrant 4
    sig = float, the number of standard deviation above which significance
          is claimed. Default value is 2.

    Returns a number: 0 if all quadrants have the same number of GCs up to
    1-sigma, or the number of the quadrants deviating from the mean by the
    designated amount. If the quadrant is lacking clusters, than a negative
    number is written.
    '''

    # Poisson uncertainties in each of the quadrants
    eq1 = q1**0.5
    eq2 = q2**0.5
    eq3 = q3**0.5
    eq4 = q4**0.5

    # creating arrays for easier handling
    q = array([q1,q2,q3,q4])
    eq = array([eq1,eq2,eq3,eq4])

    # finding the mean and standard deviations of the number of GC in the
    # four quadrants
    mq = mean(q)
    stdq = std(q)

    # this is the resulting list
    res = []

    # performing the check: the magic happens here
    for i,v in enumerate(q):
        x = (v - mq) / (stdq**2.0 + eq[i]**2.0)**0.5
        if x >= sig:
            res.append(i+1)
        elif x <= -sig:
            res.append(-1*(i+1))
        else:
            pass

    # This is done in case no quadrant deviates away from the mean by at least
    # \sig\
    if len(res) == 0:
        return [0]
    else:
        res = array(res)
        return res.tolist()

################################################################################
def GCbinner(database2D):
    '''
    This is the new version of the now obsolete GCbinner function.

    First, the module randomly decides how many GCs each stream contributes to
    the halo. The decision is based on the mass of the streams. Then the module
    calculates how many clusters should be put into the pre-determined radial
    bins to populate the halo GC system. To do this, the module determines the
    radial number density profile of the 'star' particles. Given the total
    number of GC in the halo has already been determined, the profile of the GCs
    is a scaled down version of the profile 'measured' for the 'star' particles.

    arguments
    database2D = path to the entire binary data file of the observed system

    output
    GC_quota = array, number of clusters donated by that stream
    GC_bin   = array, number of clusters in each radial bin. There are 6 bins.
    uni_str  = array, contains the stream ids of those streams that contribute
               GCs to the halo.

    '''

    # reading the relevant parts of the observed data
    data     = load(database2D)

    # select the appropriate data
    x        = data[0]
    y        = data[1]
    Rproj    = data[4]
    str_id   = data[6]
    str_mass = data[7]

    # clean up
    del data

    # selecting the unique streams, but also keeping knowing the mass of
    # that stream
    str_id_mass     = zip(str_id, str_mass)
    uni_str_id_mass = transpose(array(list(set(str_id_mass))))
    uni_str         = uni_str_id_mass[0]
    uni_mass        = uni_str_id_mass[1]

    # clean up
    del str_id_mass, uni_str_id_mass

    # Combination of the two above ways of selecting cluster bearing streams
    # and deciding their overall contirbution. Pending approval of the boss
    flag   = ones(len(uni_str))
    # donating fraction (dnfrc)
    dnfrc  = ones(len(uni_str))
    for i,stream in enumerate(uni_str):
        tmp1 = float(len(Rproj[(str_id==stream)&(Rproj>=30.)]))
        tmp2 = float(len(Rproj[str_id==stream]))
        dnfrc[i] = tmp1/tmp2
    #     if dnfrc[i] < 0.01:
    #       flag[i] = 0
    #     else:
    #       pass

    # # selecting only the streams that donate clusters only
    # uni_mass = uni_mass[flag  == 1]
    # uni_str  = uni_str[flag == 1]
    # dnfrc    = dnfrc[flag == 1]

    # 'old biased style'
    # dnfrc = ones(len(uni_str))

    # number of streams to consider
    l = len(uni_str)

    # simple check if the number of stream ids is the same as the mass
    try:
        len(uni_mass) == len(uni_str)
    except ValueError:
        print '''Oops! The number of stream ids does not equal the number of
                 masses entered into the module! Please check the input
                 arguments before retrying.'''

    # deciding how many GC each stream will contribute
    GC_quota = zeros(l)  # here the quota is stored, one element per stream
    for i in range(l):
        if (uni_mass[i] > 10.**6.) & (uni_mass[i] < 10.**7):
            GC_quota[i] = int(rd.randint(7,19) * dnfrc[i])
        elif (uni_mass[i] > 10.**7.) & (uni_mass[i] < 10.**8):
            GC_quota[i] = int(rd.randint(15,35) * dnfrc[i])
        elif uni_mass[i] > 10.**8. :
            GC_quota[i] = int(rd.randint(35,55) * dnfrc[i])

    # The total number of GCs donated
    Ntot = sum(GC_quota)

    # the total number of GC to be drawn is determined, as well as the
    # contribution of each stream in turn.

    # Now measure the 'stellar' profile of the halo, and determine how to
    # populate the radial bins with GCs..

    # this is measuring the 'star' profile of the halo
    # creates the bin edges
    bins = arange(30,160,20)
    # bins value (bv), bin edge (be)
    bv, be = histogram(Rproj, bins = bins)
    # normalize the bin values
    bv = bv/float(sum(bv))

    # now populate the bin quota (number of clusters in each radial bin)
    Bin_quota = ones(len(bv)) * Ntot
    # and these should be the bins with the right number of clusters. No noise.
    Bin_quota = rint(Bin_quota * bv)

    # print Ntot
    # print GC_quota
    # print Bin_quota

    # And that's it! Return the results
    return GC_quota, Bin_quota, uni_str

################################################################################

def customclippedmean(a, sigma = 3, iters = 5):
    '''
    Just repackaged mean sigmal clip wrapper. It contans the default parameters.
    And I do not have to remember that long-winded reference..
    '''
    a_clipped = sigma_clip(a, sig = sigma, iters = iters)
    return mean(a_clipped)


################################################################################
def wkinmapper(halo, nbins, mtrans,
               xlimit = [150, -150], ylimit = [-150, 150],
               spatialRange=([-150,150],[-150,150]),
               statistic = customclippedmean,
               colormap = cm.coolwarm,
               colorbarlabel = 'Line-of-sight velocity [km/s]'):
    '''
    This produces a 2d histogram of an Aquarius halo. The colour of each bin
    corresponds to the clipped mean (10-itters, 3-sigma) of the velocity of all
    stellar-like particles in that bin.

    arguments
    halo          = which halo is being analysed, used to set up the path to
                    relevant input files
    nbins         = number of bins for visualisation of the map
    mtrans        = the transformation matrix, used to project the dark matter
                    axis onto the plot
    xlimit/ylimit = the axes limits of the resulting figure, they are lists
    spatialRange  = (2,2) array_like, the leftmost and rightmost edges of the
                    bins along each dimension (if not specified explicitly in
                    the bins parameters): [[xmin, xmax], [ymin, ymax]]. All
                    values outside of this range will be considered outliers
                    and not tallied in the histogram.
    statistic     = string or callable, optional; a function which takes a 1D
                    array of values, and outputs a single numerical statistic.
                    This function will be called on the values in each bin.
                    Empty bins will be represented by function([]), or NaN if
                    this returns an error.
    '''

    print 'Creating the global halo velocity map..'

    data = load('/data/users/jovan/Aquarius/PresentDay/halo-' + halo +
                '/dataProducts/Particle.ObservedData.npy')
    x   = data[0]
    y   = data[1]
    vel = data[2]

    # clean ip
    del data

    figga = p.figure(figsize = (9,6),dpi = 100,facecolor = 'w',edgecolor = 'K')
    p.axes().set_aspect('equal')
    p.xlim(xlimit)
    p.ylim(ylimit)
    minorticks(ftsize = 8, maw = 1.5, miw = 1.1)
    p.xlabel(r'pseudo-$\xi$ [kpc]',  fontsize = 8)
    p.ylabel(r'pseudo-$\eta$ [kpc]', fontsize = 8)

    Z, xedges, yedges, binnumber = bin_st_2d(x,
                                             y,
                                             values = vel,
                                             statistic = statistic,
                                             bins = nbins,
                                             range = spatialRange)
    # defining the extent of the map
    extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
    p.imshow(Z.T,
             interpolation = 'none',
             extent = extent,
             origin = 'lower',
             cmap = colormap)
    cbr = p.colorbar()
    cbr.ax.tick_params(labelsize=8)

    # putting two circles to guide the eye
    rad30 = Circle((0, 0),
                    radius =30,
                    facecolor = 'none',
                    edgecolor = 'purple',
                    lw = 1.0)
    rad100 = Circle((0, 0),
                    radius =100,
                    facecolor = 'none',
                    edgecolor = 'purple',
                    lw = 1.0)

    # making the circles pretty
    p.setp(rad30, ls='dashed')
    p.setp(rad100, ls='dashed')
    # adding the circles to the plot
    ax = p.gca()
    fig = p.gcf()
    fig.gca().add_artist(rad30)
    fig.gca().add_artist(rad100)

    # label and colorbar
    label1 = p.text(-225, 50, colorbarlabel,
                    rotation = 'vertical',fontsize = 8)

    # now adding the little picture in picture (pip) (inset axes) for the
    # orientiation of the dark matter principal axes..

    # first, calculate the coordinates of the principal axes in the given
    # projection
    pa = load('/data/users/jovan/Aquarius/PresentDay/halo-' + halo +
              '/principalAxis/pa'+halo+'.npy')
    # converting from spherical to cartesian coords
    xx = sin(pa[0])*cos(pa[1])
    yy = sin(pa[0])*sin(pa[1])
    zz = cos(pa[0])
    # projecting the axes onto the plane of observations..
    xo = zeros(3)
    yo = zeros(3)
    for i in range(3):
        xo[i], yo[i] = coords_transform(mtrans, array([xx[i],yy[i],zz[i]]))
    # labels for the orientation vectors
    lbls = ['v1', 'v2','v3']
    # calculating the offsets for the labels of the orientation vectros
    offx = zeros(3)
    offy = zeros(3)
    # the embedded plot starts here
    pip = p.axes([.60, .1, .15, .15], axisbg='w',frameon=False)
    for i in range(3):
        if offx[i] >=0:
            offx[i] += 0.2
        else:
            offx[i] -= 0.2
        if offy[i] >=0:
            offy[i] += 0.2
        else:
            offy[i] -= 0.2
        p.arrow(0,0,xo[i],yo[i],head_width=0.07,head_length=0.07,fc='k',ec='k')
        p.annotate(lbls[i], xy=(xo[i],yo[i]),
                   xytext=(xo[i]+offx[i],yo[i]+offy[i]), fontsize = 5)
    p.xlim([1.5,-1.5])
    p.ylim([-1.5,1.5])
    p.setp(pip, xticks=[], yticks=[])

    return figga

################################################################################
def kinfig(posAng,vel,err_vel,vsys,
           postAmp, rangeAmp, postAxi, rangeAxi, postSig, rangeSig):
    '''
    This creates the figure which shows the results of BayesianKinematics2D

    arguments:
    posAng                        = position angle of the stars/clusters [rad]
    vel                           = velocities of the stars/clusters [km/s]
    err_vel                       = uncertinties in the velocities   [km/s]
    vsys                          = systemic velocity of the galaxy  [km/s]
    postAmp / postAxi / postSig   = posterior pdfs of the amplitude, rotation
                                  axis, and velocity dispersion as
                                  determined by the BayesianKinematics2D.py
                                  code.
    rangeAmp/ rangeAxi / rangeSig = the grids (lattices) along which the
                                    amplitude, rotation axis, and the
                                    velocity dispersion are searched.
    '''

    # annotations for the subplots
    try:
        midAmp, lowAmp, highAmp = confidence(postAmp, rangeAmp,
                                                plot_friendly =True,
                                                statistic = 'peak')
    except IndexError:
        midAmp, lowAmp, highAmp = array([0.,0.]),array([0.,0.]),array([0.,0.])

    midAxi, lowAxi, highAxi = circularCase(postAxi, rangeAxi,
                                            plot_friendly = True,
                                            statistic = 'peak')
    try:
        midSig, lowSig, highSig = confidence(postSig, rangeSig,
                                                plot_friendly = True,
                                                statistic = 'peak')
    except IndexError:
        midSig, lowSig, highSig = confidence(postSig, rangeSig,
                                                plot_friendly = True,
                                                statistic = 'mean')

    # defining the general figure properties, size etc on Page Two
    figga = p.figure(figsize=(9,6),dpi=100,facecolor='w',edgecolor='K')
    # defining the subplots arrangement
    gs = p.GridSpec(2,2)

    # the sinusoidal plot
    p.subplot(gs[0,0])
    p.xticks([0,90,180,270,360])
    p.xlabel(r'$\theta_{0}$ ' + '[deg]', fontsize = 12)
    p.ylabel('$V_{\\rm{los}} $ [km s$^{-1}$]', fontsize = 12)
    p.errorbar(rad2deg(posAng),
               vel,
               yerr = err_vel,
               fmt = 'o',
               ecolor = 'black',
               color = 'black',
               ms = 3)
    # dotted line through the mean velocity of the system
    p.plot([0,360], [vsys,vsys], color = 'black', lw = 0.3, ls = ':')
    # showing the rotation
    xvalues = linspace(0., 360., 3600.)
    # yvalues = vsys + midAmp[0] * sin(deg2rad(xvalues) - midAxi[0])
    yvalues = vsys + midAmp[0] * sin(midAxi[0] -deg2rad(xvalues))
    p.plot(xvalues, yvalues, color = 'black', lw = 1)
    # # creating running mean, just for fun
    # sort_posAng, sort_vel = zip(*sorted(zip(rad2deg(posAng),vel)))
    # sort_posAng           = array(sort_posAng)
    # sort_vel              = array(sort_vel)
    # # computing the running mean in the newxt step
    # runmean               = convolve(sort_vel,ones((20,))/20,mode='full')[19:]
    # p.plot(sort_posAng,
    #        runmean,
    #        color = 'black',
    #        ls = ':',
    #        lw = 1.0)
    p.xlim([0,360])
    tempval = max(max(yvalues), max(abs(vel)))
    # p.ylim([-(tempval+tempval*0.1),tempval+tempval*0.1])
    p.xticks([0,45,90,135,180,225,270,315])
    minorticks(mtnum = 4, ftsize = 10, maw = 1,miw = 0.7)

    # this is the pdf for the amplitude
    p.subplot(gs[0,1])
    p.xlabel('$A$ [km s$^{-1}$]', fontsize = 12)
    p.ylabel('Posterior probability', fontsize = 12)
    p.plot(rangeAmp, postAmp, lw =2, color  = 'black')
    # plotting the mean value
    p.plot([midAmp[0], midAmp[0]],[0, midAmp[1]],
            lw = 0.9 ,
            color = 'black',
            ls = ':')
    # plotting the confidence limits
    p.plot([lowAmp[0], lowAmp[0]],[0, lowAmp[1]], lw = 0.7 , color = 'blue')
    p.plot([highAmp[0],highAmp[0]],[0,highAmp[1]], lw = 0.7 ,color = 'blue')
    p.xlim([min(rangeAmp),max(rangeAmp)])
    p.ylim(0,max(postAmp)+max(postAmp)*0.1)
    minorticks(mtnum = 4, ftsize = 10, maw = 1,miw = 0.7)

    # this is the pdf for the rotation axis
    p.subplot(gs[1,0])
    p.xlabel(r'$\theta_{0}$ ' + '[deg]', fontsize = 12)
    p.ylabel('Posterior probability', fontsize = 12)
    p.xticks([0,90,180,270,360])
    p.plot(rad2deg(rangeAxi),postAxi,lw = 2, color = 'black')
    # plotting the peak value
    p.plot([rad2deg(midAxi[0]), rad2deg(midAxi[0])], [0, midAxi[1]],
            lw = 0.9,
            color = 'black',
            ls = ':')
    # plotting the confidence limits
    p.plot([(rad2deg(lowAxi[0])), (rad2deg(lowAxi[0]))], [0, lowAxi[1]],
            lw = 0.7,
            color = 'blue')
    p.plot([(rad2deg(highAxi[0])), (rad2deg(highAxi[0]))], [0, highAxi[1]],
            lw = 0.7,
            color = 'blue')
    p.xlim([0,360])
    p.ylim(0,max(postAxi)+max(postAxi)*0.1)
    p.xticks([0,45,90,135,180,225,270,315])
    minorticks(mtnum = 4, ftsize = 10, maw = 1,miw = 0.7)

    # this is the pdf for the velocity dispersion
    p.subplot(gs[1,1])
    p.xlabel(r'$\sigma_{0}$ ' +'[km s$^{-1}$]', fontsize = 12)
    p.ylabel('Posterior probability', fontsize = 12)
    p.plot(rangeSig,postSig, lw = 2, color = 'black')
    # plotting the mean value
    p.plot([midSig[0], midSig[0]],[0, midSig[1]],
            lw = 0.9 ,
            color = 'black',
            ls = ':')
    # plotting the confidence limits
    p.plot([lowSig[0], lowSig[0]],[0, lowSig[1]],
            lw = 0.7,
            color = 'blue')
    p.plot([highSig[0], highSig[0]],[0, highSig[1]],
            lw = 0.7,
            color = 'blue')
    p.xlim([min(rangeSig),max(rangeSig)])
    p.ylim(0,max(postSig)+max(postSig)*0.1)
    minorticks(mtnum = 4, ftsize = 10, maw = 1,miw = 0.7)

    p.tight_layout()

    return figga


################################################################################
def WAnalysisTask1(obsnet, datadir, datafile, halo, sigma, fres):
    '''
    Creates a multiplot with histograms showing the kinematic ratio
    (amplitude/velocity_dispersion) for all instances of each perspective
    the halo is observed from.

    Also calculates the probability of observing a kinematic ratio as large or
    larger than that seen in M31 for each location of the virtual observer.
    '''

    print >>fres, 'WAnalysisTask1'
    print >>fres, ''

    # loading up the data, which is saved via cPickle
    data = cPickle.load(open(datadir+datafile, 'rb'))
    # because of the mess that pickle creates, unpack things manually..
    full_theta    = array(zip(*data)[0])
    full_phi      = array(zip(*data)[1])
    full_instance = array(zip(*data)[2])
    full_amp      = array(zip(*data)[3])
    full_axi      = array(zip(*data)[4])
    full_sig      = array(zip(*data)[5])
    full_eigval   = array(zip(*data)[6])
    full_eigvec   = array(zip(*data)[7])
    full_angmom   = array(zip(*data)[8])
    full_q1       = array(zip(*data)[9])
    full_q2       = array(zip(*data)[10])
    full_q3       = array(zip(*data)[11])
    full_q4       = array(zip(*data)[12])

    print 'Creating the histogram multiplot...'

    # Defining the master figure
    figga, axs = p.subplots(4,4, sharex=True, sharey=True, figsize=(9,11))
    # axes ranges
    p.ylim([0,90.])
    p.xlim([0,1.6])
    # p.xlim([0,2.4])
    # the labels of ticks
    # xtlabels = [0,0.3,0.9,1.5,2.1]
    xtlabels = [0,' ', 0.4,' ', 0.8,' ', 1.2,' ']
    # xtlabels = [0,' ', 1.0,' ', 2.0,' ', 3.0, ' ', 4.0,' ']
    ytlabels = range(0,90,10)

    # these control the locating of the subplots
    ii = 0
    jj = 0

    # the master loop begins
    for i,v in enumerate(obsnet):
        # selecting and reading in the relevant portion of the data
        ind    = (full_theta == v[0]) & (full_phi == v[1])

        amp    = full_amp[ind]
        axi    = full_axi[ind]
        sig    = full_sig[ind]
        eigval = full_eigval[ind]
        eigvec = full_eigvec[ind]
        angmom = full_angmom[ind]
        q1     = full_q1[ind]
        q2     = full_q2[ind]
        q3     = full_q3[ind]
        q4     = full_q4[ind]

        # calculating the main kinematic ratio, between the maximum
        # amplitude and the velocity dispersion
        kinrat = amp/sig

        # the mean of the kinrat in the full sample
        meanKR = bs.location(kinrat)[0]

        # now choosing which systems are most similar to M31
        idx = [True] * len(q1)
        idx = array(idx)

        # constructing the index showing which systems are akin to M31
        for k in range(len(q1)):
            ta = azym_sim_test_mean(q1[k],q2[k],q3[k],q4[k],sig=sigma)

            if ((len(ta) == 2) and
                (any([tmp < 0 for tmp in ta])) and
                (any([tmp > 0 for tmp in ta]))):
                pass
            else:
                idx[k] = False

        # the mean of the kinrat in the selected sample
        if len(amp[idx]) >= 10 :
            meanKRsub = bs.location(kinrat[idx])[0]
        else:
            meanKRsub = NaN

        # determining the bins of the histogram(s)
        bins = [0.0,0.2,0.4,0.6,0.8,1,1.2,1.4,1.6]
        # bins = linspace(0,2.5,11)
        # bins = linspace(0.0, 5.0, 11.0)
        # histogram, only done to determine the ylim of the subplots
        hh, ee = histogram(kinrat, bins = bins)
        # the master histograms
        axs[ii,jj].hist(kinrat,
                        bins = bins,
                        histtype = 'stepfilled',
                        color = 'lightblue',
                        ec = 'black',
                        lw = 1.25,
                        label = 'System '+str(int(v[0]))+' '+str(int(v[1])))
        # the histogram for the data more similar to M31.
        # check first how many points are left, if any.
        if len(kinrat[idx]) == 0:
            pass
        else:
            axs[ii,jj].hist(kinrat[idx],
                            bins = bins,
                            histtype = 'step',
                            color = 'black',
                            lw = 1.25,
                            ls = 'dashed')
        # this is only done to add a random text in the legend :)
        axs[ii,jj].errorbar([],
                            [],
                            ls = '--',
                            c = 'black',
                            label = str(len(amp[idx])) + '/100')
        # the average line of the full sample
        axs[ii,jj].plot([meanKR,meanKR],
                        [0.,90.],
                        lw = 0.5,
                        ls = '-',
                        color = 'black')
        # the average line of the selected sample
        axs[ii,jj].plot([meanKRsub,meanKRsub],
                        [0,90],
                        lw = 0.5,
                        ls = ':',
                        color = 'black')
        # legend
        axs[ii,jj].legend(loc = 1, 
                          frameon=False, 
                          handlelength=4, 
                          prop={'size':8})

        # labels of the subplots
        if jj==0:
            axs[ii,jj].set_ylabel('Number of systems', fontsize=12)
            axs[ii,jj].set_yticklabels(ytlabels, fontsize=10)

        if ii==3:
            axs[ii,jj].set_xlabel(r'$A/\sigma_0$', fontsize=12)
            axs[ii,jj].xaxis.set_ticks(bins)
            axs[ii,jj].set_xticklabels(xtlabels, fontsize=10)

        # Now calculating the probability of observing a kinematic ratio as
        # large or larder than that seen in M31, for this particular view-point
        # of the Aquarius halo.

        # create the histogram
        newbins = arange(0.0,1.61,0.01)
        hh, ee  = histogram(kinrat, bins = newbins)
        # calculating the centre of each bin
        bm = zeros(len(hh)) # the centres of each bin
        for i in range(len(hh)):
            bm[i] = 0.5 * (ee[i+1]+ee[i])
        # converting the histogram values to float (they are in int originally)
        hh = hh.astype('float')
        hh = hh/sum(hh)
        # The location of the M31 kinematic ratio
        ll   = min(enumerate(bm), key=lambda x: abs(x[1] - 0.93))[0]
        prob = sum(hh[ll:])

        # printing the results
        print 'The probability of observing a kinematic ratio equal or larger'
        print 'than 0.67 from observer located at', v, 'is', '%-1.3f' % (prob)

        # printing the same thing to a file
        print >>fres, 'The probability of observing a kinematic ratio equal '
        print >>fres, 'or larger than 0.67 frp, observer located at ', v,
        print >>fres, 'is ', '%-1.3f' % (prob)

        # Now do the same, but for the selected sample
        hh, ee = histogram(kinrat[idx], bins = newbins)
        # calculating the bin centres
        bm = zeros(len(hh)) # the bin centres (middles)
        for i in range(len(hh)):
            bm[i] = 0.5 * (ee[i+1]+ee[i])
        # converting the histogram values to floats, they were ints originally
        hh   = hh.astype('float')
        hh   = hh/sum(hh)
        # The location of the M31 kinematic ratio (0.67)
        ll   = min(enumerate(bm), key=lambda x: abs(x[1] - 0.93))[0]
        prob = sum(hh[ll:])

        # printing the results
        print 'The probability of observing a kinematic ratio equal or larger'
        print 'than 0.67, but for the azimuthally selected sample of mock '
        print 'systems, as seen from an observer located at ', v,
        print 'is', '%-1.3f' % (prob)
        print
        # now print the same thing to file
        print >>fres, 'The probability of observing a kinematic ratio equal or'
        print >>fres, 'larger than 0.67, but for the azimuthally selected '
        print >>fres, 'sample of mock systems, as seen from an observer'
        print >>fres, 'located at', v, 'is', '%-1.3f' % (prob)
        print >>fres, ''

        # clean up
        del amp, axi, sig, q1, q2, q3, q4, eigvec, angmom, eigval
        del meanKRsub, meanKR, hh, ee, prob, bm

        # calculating the position of the next subplot
        ii, jj = gridSpecEvo(ii,jj)

    print
    print >>fres, ''

    # clear up
    del ii, jj
    
    # formating the subplots, no space between them
    figga.subplots_adjust(wspace=0, hspace=0)
    # saving the plot to disk
    p.savefig(datadir +'plots/histfig' + halo + '.pdf',
                dpi=120,
                bbox_inches = 'tight')
    p.clf()

    print 'The histogram multiplot is created successfully!'
    print

################################################################################
def WAnalysisTask2(obsnet, datadir, datafile, halo, sigma, fres):
    '''
    Creates a single histogram using all data, in a way it is the
    average kinematics of the halo as seen from all perspectives.

    Also calculates the probability of observing a kinematic ratio as
    large or larger than that seen in M31, regardless of the location of the
    observer.
    '''

    print >>fres, 'WAnalysisTask2'
    print >>fres, ''

    # loading up the data, which is saved via cPickle
    data = cPickle.load(open(datadir+datafile, 'rb'))
    # because of the mess that pickle creates, unpack things manually..
    full_theta    = array(zip(*data)[0])
    full_phi      = array(zip(*data)[1])
    full_instance = array(zip(*data)[2])
    full_amp      = array(zip(*data)[3])
    full_axi      = array(zip(*data)[4])
    full_sig      = array(zip(*data)[5])
    full_eigval   = array(zip(*data)[6])
    full_eigvec   = array(zip(*data)[7])
    full_angmom   = array(zip(*data)[8])
    full_q1       = array(zip(*data)[9])
    full_q2       = array(zip(*data)[10])
    full_q3       = array(zip(*data)[11])
    full_q4       = array(zip(*data)[12])

    print 'Creating an average histogram from all perspectives...'

    # defining the figure right at the top
    p.figure(figsize = (6,4), dpi = 100, facecolor = 'w', edgecolor = 'K')

    # reading in the full data, regardless of perspective
    amp    = full_amp
    axi    = full_axi
    sig    = full_sig
    eigval = full_eigval
    eigvec = full_eigvec
    angmom = full_angmom
    q1     = full_q1
    q2     = full_q2
    q3     = full_q3
    q4     = full_q4

    # checking which systems are a bit more similar to M31
    idx = [True] * len(q1)
    idx = array(idx)

    for k in range(len(q1)):
        ta = azym_sim_test_mean(q1[k],q2[k],q3[k],q4[k],sig=sigma)

        if ((len(ta) == 2) and
            (any([tmp < 0 for tmp in ta])) and
            (any([tmp > 0 for tmp in ta]))):
            pass
        else:
            idx[k] = False

    # the mean of the full sample
    kinrat = amp/sig
    meanKR = bs.location(kinrat)[0]

    # the mean of the selected sample
    meanKRsub = bs.location(kinrat[idx])[0]

    # determining the bins of the histogram
    bins = [0.0,0.2,0.4,0.6,0.8,1,1.2,1.4,1.6]
    bins = linspace(0,2.4,9)
    # bins = linspace(0,5.0,11)
    # custom x-tics
    p.xticks(around(bins,1))
    # labels of the subplots
    p.xlabel(r'$A/\sigma_0$', fontsize  = 12)
    p.ylabel('Number of systems', fontsize  = 12)
    # histogram, only done to determine the ylim of the subplots
    hh, ee = histogram(kinrat, bins = bins)
    p.ylim([0,1070])
    p.xlim([0,2.5])
    # p.xlim([0.0,5.0])
    # the master histograms
    p.hist(kinrat,
           bins = bins,
           histtype = 'stepfilled',
           color = 'lightblue',
           ec = 'black',
           lw = 1.5,
           label = 'average kinematics for halo ' + halo)
    # the histogram for the data more similar to M31.
    # check first how many points are left, if any.
    if len(kinrat[idx]) == 0:
        pass
    else:
        p.hist(kinrat[idx],
               bins = bins,
               histtype = 'step',
               color = 'black',
               lw = 1.5,
               ls = 'dashed')
    # the average line of the full sample
    p.plot([meanKR,meanKR],
           [0,1070],
           lw = 0.7,
           ls = '-',
           color = 'black')
    # the average line of the selected sample
    p.plot([meanKRsub,meanKRsub],
           [0,1070],
           lw = 0.7,
           ls = ':',
           color = 'black')
    # this is only done to add a random text in the legend :)
    p.errorbar([],
               [],
               c = 'black',
               ls = '--',
               label = str(len(amp[idx])) + '/' + str(len(amp)))
    # legend
    p.legend(loc = 1, frameon=False, handlelength=4, prop={'size':10})

    # configuring the ticks with my custom rutine
    minorticks(mtnum=10, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)

    # saving the plot to disk
    p.savefig(datadir + 'plots/avghist' + halo + '.pdf',
                dpi = 120,
                bbox_inches = 'tight')
    p.clf()

    print 'The average histogram is created successfully!'
    print

    print 'Mean kinematic ratio for the full sample: ', \
           '%-1.3f' % (bs.location(kinrat)[0])
    print 'Mean kinematic ratio for the sele sample: ', \
           '%-1.3f' % (bs.location(kinrat[idx])[0])
    print
    # print the same thing to a file
    print >>fres, 'Mean kinematic ratio for the full sample: ', \
           '%-1.3f' % (bs.location(kinrat)[0])
    print >>fres,'Mean kinematic ratio for the sele sample: ', \
           '%-1.3f' % (bs.location(kinrat[idx])[0])
    print >>fres, ''

    # Now calculating the probability of observing a kinematic ratio as large
    # or larger than seen in M31.

    # create the histogram
    newbins = arange(0.0,1.61,0.01)
    hh, ee  = histogram(kinrat, bins = newbins)
    # calculating the centres of each bin
    bm = zeros(len(hh)) # the bin centres (middles)
    for i in range(len(hh)):
        bm[i] = 0.5 * (ee[i+1]+ee[i])
    # converting the histogram values to float (they are in int originally)
    hh   = hh.astype('float')
    hh   = hh/sum(hh)
    # The location of the M31 kinematic ratio
    ll   = min(enumerate(bm), key=lambda x: abs(x[1] - 0.93))[0]
    prob = sum(hh[ll:])

    print 'The probability of observing a kinematic ratio as large or larger'
    print 'than that seen in M31 is: ', '%-1.3f' % (prob)
    # print the same thing to a file
    print >>fres, 'The probability of observing a kinematic ratio as large or '
    print >>fres, 'larger than that seen in M31 is: ', '%-1.3f' % (prob)

    # Now do the same, but for the selected sample
    hh, ee  = histogram(kinrat[idx], bins = newbins)
    # calculating the centres of each bin
    bm = zeros(len(hh)) # the bin centres (middles)
    for i in range(len(hh)):
        bm[i] = 0.5 * (ee[i+1]+ee[i])
    # converting the histogram values to floats, they were ints originally
    hh   = hh.astype('float')
    hh   = hh/sum(hh)
    # The location of the M31 kinematic ratio
    ll   = min(enumerate(bm), key=lambda x: abs(x[1] - 0.93))[0]
    prob = sum(hh[ll:])

    # printing the results
    print 'The probability of observing a kinematic ratio as large or larger'
    print 'than that seen in M31, but for the selected sample, '
    print 'is: ', '%-1.3f' % (prob)
    print
    # print the same thing to a file
    print >>fres, 'The probability of observing a kinematic ratio as large or '
    print >>fres, 'larger than that seen in M31, but for the selected sample '
    print >>fres, 'is: ', '%-1.3f' % (prob)
    print >>fres, ''

    # clean up
    del amp, axi, sig, q1,q2,q3,q4, meanKRsub, meanKR, kinrat
    del eigvec, eigval, angmom, hh, ee

################################################################################
def WAnalysisTask3(obsnet, datadir, datafile, halo, sigma):
    '''
    Creates a latex-friendly table containing the kinematic averaged kinematic
    information as seen from each perspective of the observed halo.

    Also added is the probability of observing a kinematic ratio equal or
    larger than that see in M31, for the full and selected sample for each
    perspective.
    '''

    # loading up the data, which is saved via cPickle
    data = cPickle.load(open(datadir+datafile, 'rb'))
    # because of the mess that pickle creates, unpack things manually..
    full_theta    = array(zip(*data)[0])
    full_phi      = array(zip(*data)[1])
    full_instance = array(zip(*data)[2])
    full_amp      = array(zip(*data)[3])
    full_axi      = array(zip(*data)[4])
    full_sig      = array(zip(*data)[5])
    full_eigval   = array(zip(*data)[6])
    full_eigvec   = array(zip(*data)[7])
    full_angmom   = array(zip(*data)[8])
    full_q1       = array(zip(*data)[9])
    full_q2       = array(zip(*data)[10])
    full_q3       = array(zip(*data)[11])
    full_q4       = array(zip(*data)[12])

    print 'Creating results table for all perspectives of halo ', halo, '..'

    # where the output text file is to be saved
    textdir = datadir + 'latexStuff/'

    # opening the text file
    fout = open(textdir + '100table' + halo, 'w')

    # printing the header
    print >>fout, '%-15s %5s %-10s %5s %-10s %5s %16s %5s %5s %-10s %5s ' %  \
                  ('Perspective',
                   ' $ ',
                   'Amp/Sig',
                   ' $ ',
                   'P(0.67<=)',
                   ' $ ',
                   'N*   $ ',
                   'Amp/Sig*',
                   ' $ ',
                   'P(0.67<=)',
                   ' \\\\ ')
    print >>fout, ''

    # the master loop begins
    for i,v in enumerate(obsnet):
        # selecting and reading in the relevant portion of the data
        ind = (full_theta == v[0]) & (full_phi == v[1])

        amp    = full_amp[ind]
        axi    = full_axi[ind]
        sig    = full_sig[ind]
        eigval = full_eigval[ind]
        eigvec = full_eigvec[ind]
        angmom = full_angmom[ind]
        q1     = full_q1[ind]
        q2     = full_q2[ind]
        q3     = full_q3[ind]
        q4     = full_q4[ind]

        kinrat = amp/sig

        meanKR = bs.location(kinrat)[0]
        meanKRerr = bs.scale(kinrat)[0]

        # selecting systems which are most similar to M31
        idx = [True] * len(q1)
        idx = array(idx)

        # constructing the index point to systems similar to M31
        for k in range(len(q1)):
            ta = azym_sim_test_mean(q1[k],q2[k],q3[k],q4[k],sig=sigma)

            if ((len(ta) == 2) and
                (any([tmp < 0 for tmp in ta])) and
                (any([tmp > 0 for tmp in ta]))):
                pass
            else:
                idx[k] = False

        # check if there are sufficient number of similar clusters left
        if len(amp[idx]) >= 10 :
            meanKRsub = bs.location(amp[idx]/sig[idx])[0]
            meanKRsuberr = bs.scale(amp[idx]/sig[idx])[0]
        else:
            meanKRsub = NaN
            meanKRsuberr = NaN

        # Calculating the probability of observing a kinematic ratio as large
        # or larder than that seen in M31

        bins = arange(0.0,1.61,0.01)
        hh, ee = histogram(kinrat, bins = bins)
        # calculating the centre of each bin
        bm = zeros(len(hh)) # the centres of each bin
        for i in range(len(hh)):
            bm[i] = 0.5 * (ee[i+1]+ee[i])
        # converting the histogram values to float (they are in int originally)
        hh = hh.astype('float')
        hh = hh/sum(hh)
        # The location of the M31 kinematic ratio
        ll   = min(enumerate(bm), key=lambda x: abs(x[1] - 0.67))[0]
        prob = sum(hh[ll:])

        # Now, the same but for the selected (sub)sample..
        hh, ee = histogram(kinrat[idx], bins= bins)
        # calculating the centre of each bin
        bm = zeros(len(hh)) # the centres of each bin
        for i in range(len(hh)):
            bm[i] = 0.5 * (ee[i+1]+ee[i])
        # converting the histogram values to float (they are in int originally)
        hh = hh.astype('float')
        hh = hh/sum(hh)
        # The location of the M31 kinematic ratio
        ll   = min(enumerate(bm), key=lambda x: abs(x[1] - 0.67))[0]
        probsub = sum(hh[ll:])

        # The printing happens now... the true magic of the function.. :)
        print >>fout,'%-5s %-5s %-5s %-5s %-12s %5s %5s %9s %-10s %-12s %9s %5s %5s' % (
                    str(v[0]),
                    ' $ ',
                    str(v[1]),
                    ' $ ',
                    str('%2.2f' % (meanKR)) + ' \pm ' + \
                    str('%1.2f' % (meanKRerr)),
                    ' $ ',
                    str('%1.3f' % (prob)),
                    ' % ',
                    str(len(amp[idx])) + '   $',
                    str('%2.2f' % (meanKRsub)) + ' \pm ' + \
                    str('%1.2f' %(meanKRsuberr)),
                    ' $ ',
                    str('%1.3f' % (probsub)),
                    ' \\\\ ')

        #clean up
        del meanKR, meanKRerr, meanKRsuberr, meanKRsub
        del amp, axi, sig, q1, q2, q3, q4
        del eigvec, eigval, angmom
    fout.close()

    print 'Latex-friendly table successfully created.'
    print


################################################################################
def WAnalysisTask4(obsnet, datadir, datafile, halo, sigma, fres):
    '''
    Prints to screen the perspective for which the maximum rotation is
    observed, and what the kinematic ratio of that perspective is.
    Same is done for a selected subsample of the data, meaning that
    only systems that are similar to that of M31 are kept for the analysis.

    Update: in addition, this task now also does a master-figure combining
    the plots that are created in WAnalysisTask6,7 and 8 but only for the
    perspective that show the minimum and maximum rotational signal.
    '''

    print >>fres, ''
    print >>fres, 'WAnalysisTask4'
    print >>fres, ''

    # loading up the data, which is saved via cPickle
    data = cPickle.load(open(datadir+datafile, 'rb'))
    # because of the mess that pickle creates, unpack things manually..
    full_theta    = array(zip(*data)[0])
    full_phi      = array(zip(*data)[1])
    full_instance = array(zip(*data)[2])
    full_amp      = array(zip(*data)[3])
    full_axi      = array(zip(*data)[4])
    full_sig      = array(zip(*data)[5])
    full_eigval   = array(zip(*data)[6])
    full_eigvec   = array(zip(*data)[7])
    full_angmom   = array(zip(*data)[8])
    full_q1       = array(zip(*data)[9])
    full_q2       = array(zip(*data)[10])
    full_q3       = array(zip(*data)[11])
    full_q4       = array(zip(*data)[12])

    kinratres    = zeros(len(obsnet))
    kinratsubres = zeros(len(obsnet))
    finAxi       = zeros(len(obsnet))
    finAxisub    = zeros(len(obsnet))
    coords       = zeros((len(obsnet),2))
    counter      = 0

    # the master loop begins
    for i,v in enumerate(obsnet):
        # selecting and reading in the relevant portion of the data
        coords[counter,0] = (v[0])
        coords[counter,1] = (v[1])

        ind = (full_theta == v[0]) & (full_phi == v[1])

        amp    = full_amp[ind]
        axi    = full_axi[ind]
        sig    = full_sig[ind]
        eigval = full_eigval[ind]
        eigvec = full_eigvec[ind]
        angmom = full_angmom[ind]
        q1     = full_q1[ind]
        q2     = full_q2[ind]
        q3     = full_q3[ind]
        q4     = full_q4[ind]

        # checking which systems are a bit more similar to M31
        idx = [True] * len(q1)
        idx = array(idx)

        # constructing the index for systems similar to M31
        for k in range(len(q1)):
            ta = azym_sim_test_mean(q1[k],q2[k],q3[k],q4[k], sig = 1.0)

            if ta[0] == 0:
                idx[k] = False
            else:
                idx[k] = True

        kinratres[counter] = bs.location(amp/sig)[0]
        finAxi[counter]    = bs.location(axi)[0]
        # check if there are sufficient number of similar clusters left
        if len(amp[idx]) >= 10 :
            kinratsubres[counter] = bs.location(amp[idx]/sig[idx])[0]
            finAxisub[counter]    = bs.location(axi[idx])[0]
        else:
            kinratsubres[counter] = NaN
            finAxisub[counter]    = NaN

        counter += 1

    # perspective with maximum rotation (maximum rotation perspective == maxrp)
    maxrp  = coords[nanargmax(kinratres)]
    # perspective with minimum rotation
    minrp  = coords[nanargmin(kinratres)]

    print 'Results for the full sample:'
    print 'Maximum kinetic ratio: ', '%1.3f' % (max(kinratres))
    print 'Observed for perspective: ', coords[nanargmax(kinratres)]
    print
    print 'Minimum kinetic ratio: ', '%1.3f' % (min(kinratres))
    print 'Observed for perspective: ', coords[nanargmin(kinratres)]
    print

    print 'Results for the selected sample'
    print 'Maximum kinetic ratio: ', '%1.3f' % (max(kinratsubres))
    print 'Observed for perspective: ', coords[nanargmax(kinratsubres)]
    print
    print 'Minimum kinetic ratio: ', '%1.3f' % (min(kinratsubres))
    print 'Observed for perspective: ', coords[nanargmin(kinratsubres)]
    print

    # printing the results to file
    print >>fres, 'Results for the full sample:'
    print >>fres, 'Maximum kinetic ratio: ', '%1.3f' % (max(kinratres))
    print >>fres, 'Observed for perspective: ', coords[nanargmax(kinratres)]
    print >>fres, ''
    print >>fres, 'Minimum kinetic ratio: ', '%1.3f' % (min(kinratres))
    print >>fres, 'Observed for perspective: ', coords[nanargmin(kinratres)]
    print >>fres, ''

    print >>fres, 'Results for the selected sample'
    print >>fres, 'Maximum kinetic ratio: ', '%1.3f' % (max(kinratsubres))
    print >>fres, 'Observed for perspective: ', coords[nanargmax(kinratsubres)]
    print >>fres, ''
    print >>fres, 'Minimum kinetic ratio: ', '%1.3f' % (min(kinratsubres))
    print >>fres, 'Observed for perspective: ', coords[nanargmin(kinratsubres)]
    print >>fres, ''

    # clean up
    del amp, axi, sig, q1, q2, q3, q4, kinratsubres, kinratres, counter,
    del finAxi, idx, ta, finAxisub, coords
    del eigvec, eigval, angmom, data

    # In what follows, I recreate the analysis done by WAnalysisTask6,
    # WAnalysisTask7, WAnalysisTask8, but only for the perspectives which
    # show the minimum and maximum kinematic ratio.

    # The case of the perspective with maximum rotation

    # Selecting the appropriate data
    ind    = (full_theta == maxrp[0]) & (full_phi == maxrp[1])
    amp    = full_amp[ind]
    axi    = full_axi[ind]
    sig    = full_sig[ind]
    eigval = full_eigval[ind]
    eigvec = full_eigvec[ind]
    angmom = full_angmom[ind]
    q1     = full_q1[ind]
    q2     = full_q2[ind]
    q3     = full_q3[ind]
    q4     = full_q4[ind]

    # calculating the kinematic ratio for this perspective:
    kinrat = amp/sig

    # load the appropriate transformation matrix
    mtrans = load(datadir+'dataProducts/transMat/'+\
                  'TM'+'t'+str(int(maxrp[0]))+'p'+str(int(maxrp[1]))+'.npy')


    # projected angular momentum
    projangmom = []
    proj_e1    = []
    proj_e2    = []
    proj_e3    = []

    # now choosing which systems are most similar to M31
    idx = [True] *len(q1)
    idx = array(idx)

    for ll in range(len(q1)):
        ta = azym_sim_test_mean(q1[ll],q2[ll],q3[ll],q4[ll],sig = sigma)

        if ((len(ta) == 2) and
            (any([tmp < 0 for tmp in ta])) and
            (any([tmp > 0 for tmp in ta]))):
            pass
        else:
            idx[ll] = False

    # do the projection on the sky..
    for ll,am in enumerate(angmom):
        projangmom.append(coords_transform(mtrans, array(am)))

    # dealing with the eigenvectors...
    for ll in range(len(q1)):
        # ordering the eigenvectos by length (e1>e2>e3)
        order = (rankdata(eigval[ll], method = 'min')).astype(int)
        order = len(order) - order
        # projecting them on the plane of the observations
        proj_e1.append(coords_transform(mtrans,
                                        eigvec[ll,order.tolist().index(0)]))
        proj_e2.append(coords_transform(mtrans,
                                        eigvec[ll,order.tolist().index(1)]))
        proj_e3.append(coords_transform(mtrans,
                                        eigvec[ll,order.tolist().index(2)]))

    # converting to array and transposing for convenience
    projangmom = array(projangmom).T
    proj_e1    = array(proj_e1).T
    proj_e2    = array(proj_e2).T
    proj_e3    = array(proj_e3).T

    # position angle of each eigenvector, in degrees for convenience
    pa_e1   = rad2deg(posAngCal(proj_e1[0], proj_e1[1]))
    pa_e2   = rad2deg(posAngCal(proj_e2[0], proj_e2[1]))
    pa_e3   = rad2deg(posAngCal(proj_e3[0], proj_e3[1]))

    # calculate the position angle of the angular momentum
    PAangmom = posAngCal(projangmom[0],projangmom[1])

    # find the difference between the rotation axis and PAangmom
    angdiff = 180.0 - abs(abs(rad2deg(PAangmom) - axi) - 180.0)

    # the orientation of the axis is arbitrary +/- 180 deg once it is set
    angdiff[angdiff > 90.] = 180.0 - angdiff[angdiff >90]

    # finding the difference between the rotation axis and principal axes
    angdif1 = 180.0 - abs(abs(pa_e1 - axi) - 180.0)
    angdif2 = 180.0 - abs(abs(pa_e2 - axi) - 180.0)
    angdif3 = 180.0 - abs(abs(pa_e3 - axi) - 180.0)

    angdif1[angdif1 > 90.] = 180.0 - angdif1[angdif1 >90]
    angdif2[angdif2 > 90.] = 180.0 - angdif2[angdif2 >90]
    angdif3[angdif3 > 90.] = 180.0 - angdif3[angdif3 >90]

    # calculating the total angular momentum of the entire halo..
    # the full halo data
    fullhalodata = datadir + 'dataProducts/Particle.Data.npy'

    # Calculating the full angular momentum of the entire halo
    # read in the ids of all particles:
    data = load(fullhalodata)
    xx        = data[0]
    yy        = data[1]
    zz        = data[2]
    all_ids   = data[6]

    # selecting IDs outside the 30 kpc limit
    rr  = sqrt(xx**2.0 + yy**2.0 + zz**2.0)
    ind = rr >= 30.
    ids = all_ids[ind]

    # Now calculating the principal axes of the entire halo +
    # the angular momentum
    halo_eigval, halo_eigvec, halo_angmom = WM.WInertia(ids,fullhalodata)

    # clean up
    del data, all_ids, xx, yy, zz, rr, ind, ids

    # now determine the difference in angle between the (3D) angular
    # momentum of the entire halo, to that of each GC system
    angdif3D = zeros(len(angmom))
    # doing it manually..
    for ii in range(len(angmom)):
        angdif3D[ii] = rad2deg(arccos((dot(angmom[ii],halo_angmom))/\
                               (linalg.norm(angmom[ii])*linalg.norm(halo_angmom))))

    # Now.. everything of interest is calculated..
    # Let the plotting begin!

    # defining the general figure properties, size etc on Page Two
    figga = p.figure(figsize=(9,6),dpi=100,facecolor='w',edgecolor='K')
    # defining the subplots arrangement
    gs = p.GridSpec(3,3)

    # The plot of the difference in the angle between the position angle of
    # the rotation axis of the GC system and the projection of the 3D angular
    # momentum on the sky on one side, and the kinematic ratio on another side.
    p.subplot(gs[0,:])
    p.title('Perspective with maximum rotation signal: '+str(maxrp),fontsize=8)
    p.xlabel(r'$|L_{\rm GCS,proj} - \theta|$ [deg]', fontsize = 8)
    p.ylabel('kinematic ratio', fontsize = 8)
    p.xlim([0,90])
    p.ylim([0,2.5])
    minorticks(mtnum=4, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdiff,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdiff[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

    # now the plots with the eigenvectors...
    p.subplot(gs[1,0])
    p.xlabel(r'$|e^{1}_{\rm proj} - \theta|$ [deg]', fontsize = 8)
    p.ylabel('kinematic ratio', fontsize = 8)
    p.xlim([0,90])
    p.ylim([0,2.5])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif1,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif1[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e2
    p.subplot(gs[1,1])
    p.xlabel(r'$|e^{2}_{\rm proj} - \theta|$ [deg]', fontsize = 8)
    p.xlim([0,90])
    p.ylim([0,2.5])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif2,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif2[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e3
    p.subplot(gs[1,2])
    p.xlabel(r'$|e^{3}_{\rm proj} - \theta|$ [deg]', fontsize = 8)
    p.xlim([0,90])
    p.ylim([0,2.5])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif3,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif3[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

    # The plot Comparing the angle of the angular momentum vector (3D) of the
    # entire system of particles (the whole halo), to that of the individual
    # angular momentum vector of each GC system.
    p.subplot(gs[2,:])
    p.xlabel(r'$|L_{\rm halo} - L_{\rm GCS}|$ [deg]', fontsize = 8)
    p.ylabel('kinematic ratio', fontsize = 8)
    p.xlim([0,angdif3D.max()+10.0])
    p.ylim([0,kinrat.max()+0.1])
    minorticks(mtnum=4, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif3D,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif3D[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,angdif3D.max()+10.0],[0.93,0.93], ls=':', lw=0.9, color='black')

    # keeping it tight
    p.tight_layout()

    # saving the figure to disk
    p.savefig(datadir + 'plots/maxrot-analysis-' + halo + '.pdf',
              dpi = 120,
              bbox_inches = 'tight')

    # clean up
    del idx, angdiff, angdif3, angdif2, angdif1, angdif3D


    # The case of the perspective with minimum rotation

    # Selecting the appropriate data
    ind    = (full_theta == minrp[0]) & (full_phi == minrp[1])
    amp    = full_amp[ind]
    axi    = full_axi[ind]
    sig    = full_sig[ind]
    eigval = full_eigval[ind]
    eigvec = full_eigvec[ind]
    angmom = full_angmom[ind]
    q1     = full_q1[ind]
    q2     = full_q2[ind]
    q3     = full_q3[ind]
    q4     = full_q4[ind]

    # calculating the kinematic ratio for this perspective:
    kinrat = amp/sig

    # # now determining the difference in angle between the rotation axis
    # # and the projection of the angular momenutm (in 3D) in the plane
    # # of the sky...

    # load the appropriate transformation matrix
    mtrans = load(datadir+'dataProducts/transMat/'+\
                  'TM'+'t'+str(int(minrp[0]))+'p'+str(int(minrp[1]))+'.npy')

    # projected angular momentum
    projangmom = []
    proj_e1    = []
    proj_e2    = []
    proj_e3    = []

    # now choosing which systems are most similar to M31
    idx = [True] *len(q1)
    idx = array(idx)

    for ll in range(len(q1)):
        ta = azym_sim_test_mean(q1[ll],q2[ll],q3[ll],q4[ll],sig = sigma)

        if ((len(ta) == 2) and
            (any([tmp < 0 for tmp in ta])) and
            (any([tmp > 0 for tmp in ta]))):
            pass
        else:
            idx[ll] = False

    # do the projection on the sky..
    for ll,am in enumerate(angmom):
        projangmom.append(coords_transform(mtrans, array(am)))

    # dealing with the eigenvectors...
    for ll in range(len(q1)):
        # ordering the eigenvectos by length (e1>e2>e3)
        order = (rankdata(eigval[ll], method = 'min')).astype(int)
        order = len(order) - order
        # projecting them on the plane of the observations
        proj_e1.append(coords_transform(mtrans,
                                        eigvec[ll,order.tolist().index(0)]))
        proj_e2.append(coords_transform(mtrans,
                                        eigvec[ll,order.tolist().index(1)]))
        proj_e3.append(coords_transform(mtrans,
                                        eigvec[ll,order.tolist().index(2)]))

    # converting to array and transposing for convenience
    projangmom = array(projangmom).T
    proj_e1 = array(proj_e1).T
    proj_e2 = array(proj_e2).T
    proj_e3 = array(proj_e3).T

    # position angle of each eigenvector, in degrees for convenience
    pa_e1   = rad2deg(posAngCal(proj_e1[0], proj_e1[1]))
    pa_e2   = rad2deg(posAngCal(proj_e2[0], proj_e2[1]))
    pa_e3   = rad2deg(posAngCal(proj_e3[0], proj_e3[1]))

    # calculate the position angle of the angular momentum
    PAangmom = posAngCal(projangmom[0],projangmom[1])

    # find the difference between the rotation axis and PAangmom
    angdiff = 180.0 - abs(abs(rad2deg(PAangmom) - axi) - 180.0)

    # the orientation of the axis is arbitrary +/- 180 deg once it is set
    angdiff[angdiff > 90.] = 180.0 - angdiff[angdiff >90]

    # finding the difference between the rotation axis and principal axes
    angdif1 = 180.0 - abs(abs(pa_e1 - axi) - 180.0)
    angdif2 = 180.0 - abs(abs(pa_e2 - axi) - 180.0)
    angdif3 = 180.0 - abs(abs(pa_e3 - axi) - 180.0)

    angdif1[angdif1 > 90.] = 180.0 - angdif1[angdif1 >90]
    angdif2[angdif2 > 90.] = 180.0 - angdif2[angdif2 >90]
    angdif3[angdif3 > 90.] = 180.0 - angdif3[angdif3 >90]

    # calculating the total angular momentum of the entire halo..
    # the full halo data
    fullhalodata = datadir + 'dataProducts/Particle.Data.npy'

    # Calculating the full angular momentum of the entire halo
    # read in the ids of all particles:
    data = load(fullhalodata)
    xx        = data[0]
    yy        = data[1]
    zz        = data[2]
    all_ids   = data[6]

    # selecting IDs outside the 30 kpc limit
    rr  = sqrt(xx**2.0 + yy**2.0 + zz**2.0)
    ind = rr >= 30.
    ids = all_ids[ind]

    # Now calculating the principal axes of the entire halo +
    # the angular momentum
    halo_eigval, halo_eigvec, halo_angmom = WM.WInertia(ids,fullhalodata)

    # clean up
    del data, all_ids, xx, yy, zz, rr, ind, ids

    # now determine the difference in angle between the (3D) angular
    # momentum of the entire halo, to that of each GC system
    angdif3D = zeros(len(angmom))
    # doing it manually..
    for ii in range(len(angmom)):
        angdif3D[ii] = rad2deg(arccos((dot(angmom[ii],halo_angmom))/\
                               (linalg.norm(angmom[ii])*linalg.norm(halo_angmom))))

    # Now.. everything of interest is calculated..
    # Let the plotting begin!

    # defining the general figure properties, size etc on Page Two
    figga = p.figure(figsize=(9,6),dpi=100,facecolor='w',edgecolor='K')
    # defining the subplots arrangement
    gs = p.GridSpec(3,3)

    # The plot of the difference in the angle between the position angle of
    # the rotation axis of the GC system and the projection of the 3D angular
    # momentum on the sky on one side, and the kinematic ratio on another side.
    p.subplot(gs[0,:])
    p.title('Perspective with minimum rotation signal: '+str(minrp),fontsize=8)
    p.xlabel(r'$|L_{\rm proj} - \theta|$ [deg]', fontsize = 8)
    p.ylabel('kinematic ratio', fontsize = 8)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=4, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdiff,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdiff[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

    # now the plots with the eigenvectors...
    p.subplot(gs[1,0])
    p.xlabel(r'$|e^{1}_{\rm proj} - \theta|$ [deg]', fontsize = 8)
    p.ylabel('kinematic ratio', fontsize = 8)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif1,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif1[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e2
    p.subplot(gs[1,1])
    p.xlabel(r'$|e^{2}_{\rm proj} - \theta|$ [deg]', fontsize = 8)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif2,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif2[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e3
    p.subplot(gs[1,2])
    p.xlabel(r'$|e^{3}_{\rm proj} - \theta|$ [deg]', fontsize = 8)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif3,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif3[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

    # The plot Comparing the angle of the angular momentum vector (3D) of the
    # entire system of particles (the whole halo), to that of the individual
    # angular momentum vector of each GC system.
    p.subplot(gs[2,:])
    p.xlabel(r'$|L_{\rm halo} - L_{\rm GCS}|$ [deg]', fontsize = 8)
    p.ylabel('kinematic ratio', fontsize = 8)
    p.xlim([0,angdif3D.max()+10.0])
    p.ylim([-0.05,kinrat.max()+0.1])
    minorticks(mtnum=4, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif3D,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif3D[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,angdif3D.max()+10.0],[0.93,0.93], ls=':', lw=0.9, color='black')

    # keeping it tight
    p.tight_layout()

    # saving the figure to disk
    p.savefig(datadir + 'plots/minrot-analysis-' + halo + '.pdf',
              dpi = 120,
              bbox_inches = 'tight')

    # clean up
    del idx, angdiff, angdif3, angdif2, angdif1, angdif3D


################################################################################
def WAnalysisTask5(obsnet, datadir, datafile, halo, sigma, fres):
    '''
    Prints the mean kinematic ratio of all perspectives, basically
    the mean of the histogram created in WAnalysisTask2().
    '''

    print >>fres, ''
    print >>fres, 'WAnalysisTask5'
    print >>fres, ''

    # loading up the data, which is saved via cPickle
    data = cPickle.load(open(datadir+datafile, 'rb'))
    # because of the mess that pickle creates, unpack things manually..
    full_theta    = array(zip(*data)[0])
    full_phi      = array(zip(*data)[1])
    full_instance = array(zip(*data)[2])
    full_amp      = array(zip(*data)[3])
    full_axi      = array(zip(*data)[4])
    full_sig      = array(zip(*data)[5])
    full_eigval   = array(zip(*data)[6])
    full_eigvec   = array(zip(*data)[7])
    full_angmom   = array(zip(*data)[8])
    full_q1       = array(zip(*data)[9])
    full_q2       = array(zip(*data)[10])
    full_q3       = array(zip(*data)[11])
    full_q4       = array(zip(*data)[12])

    kinrat  = zeros(len(obsnet))
    coords  = zeros((len(obsnet),2))
    coords  = zeros((len(obsnet),2))
    counter = 0


    # the master loop begins
    for i,v in enumerate(obsnet):
        # selecting and reading in the relevant portion of the data
        coords[counter,0] = (v[0])
        coords[counter,1] = (v[1])

        ind = (full_theta == v[0]) & (full_phi == v[1])

        amp    = full_amp[ind]
        axi    = full_axi[ind]
        sig    = full_sig[ind]

        kinrat[counter] = bs.location(amp/sig)[0]

        counter += 1

    # finding which perspective sees maximum rotation
    ii = coords[nanargmax(kinrat)][0]
    jj = coords[nanargmax(kinrat)][1]

    # clean up
    del ind, amp, axi, sig, kinrat, coords

    # now select those instances one again (wow this is super sub optimal..)
    ind       = (full_theta == ii) & (full_phi == jj)
    amp       = full_amp[ind]
    sig       = full_sig[ind]

    # finding the instance  featuring minimum and maximum rotation..
    kinrat = amp/ sig

    print
    print 'Maximum rotation observed is ', max(kinrat), \
    ' and is seen in instance ' \
    't'+str(int(ii))+'p'+str(int(jj))+'run'+str(nanargmax(kinrat))+'.pdf'
    print 'Minimum rotation observed is ', min(kinrat), \
    ' and is seen in instance ' \
    't'+str(int(ii))+'p'+str(int(jj))+'run'+str(nanargmin(kinrat))+'.pdf'
    print 'A system very similar to M31 has a kinematic ration of ', \
    kinrat[nanargmin([abs(tmp - 0.68) for tmp in kinrat])], \
    ' and it is seen in instance', \
    't'+str(int(ii))+'p'+str(int(jj))+'run'+str(nanargmin([abs(tmp - 0.68) for tmp in kinrat]))+'.pdf'
    print

    # print results to file
    print >>fres, ''
    print >>fres, 'Maximum rotation observed is ', max(kinrat), \
    ' and is seen in instance ' \
    't'+str(int(ii))+'p'+str(int(jj))+'run'+str(nanargmax(kinrat))+'.pdf'
    print >>fres, 'Minimum rotation observed is ', min(kinrat), \
    ' and is seen in instance ' \
    't'+str(int(ii))+'p'+str(int(jj))+'run'+str(nanargmin(kinrat))+'.pdf'
    print >>fres, 'A system very similar to M31 has a kinematic ration of ', \
    kinrat[nanargmin([abs(tmp - 0.68) for tmp in kinrat])], \
    ' and it is seen in instance', \
    't'+str(int(ii))+'p'+str(int(jj))+'run'+str(nanargmin([abs(tmp - 0.68) for
                                                tmp in kinrat]))+'.pdf'
    print >>fres, ''


################################################################################
def WAnalysisTask6(obsnet, datadir, datafile, halo, sigma, fres):
    '''
    Creates a plot which displays the difference in the angle between the
    position angle of the rotation axis of the GC system and the projection
    of the 3D angular momentum on the sky on one side, and the kinematic
    ratio on another side.

    The plot shows all instances of all perspectives. The red points are
    those that satify the criteria for 'similarity' with M31 based on the
    azimuthal distribution of the GCs in each system (the azymuthal test)
    '''

    print >>fres, ''
    print >>fres, 'WAnalysisTask6'
    print >>fres, ''

    # loading up the data, which is saved via cPickle
    data = cPickle.load(open(datadir+datafile, 'rb'))
    # because of the mess that pickle creates, unpack things manually..
    full_theta    = array(zip(*data)[0])
    full_phi      = array(zip(*data)[1])
    full_instance = array(zip(*data)[2])
    full_amp      = array(zip(*data)[3])
    full_axi      = array(zip(*data)[4])
    full_sig      = array(zip(*data)[5])
    full_eigval   = array(zip(*data)[6])
    full_eigvec   = array(zip(*data)[7])
    full_angmom   = array(zip(*data)[8])
    full_q1       = array(zip(*data)[9])
    full_q2       = array(zip(*data)[10])
    full_q3       = array(zip(*data)[11])
    full_q4       = array(zip(*data)[12])

    # Part 6
    print 'Starting the part with the angular momentum correlation...'
    # Looking for correlation between the rotation amplitude or the kinematic
    # ration for the GC systems when the difference between the rotation axis
    # and the projected angular momentum.

    full_idx    = []
    full_kinrat = []
    full_angdif = []

    rad = 1000. # distance of the observer from the centre of the galaxy
    for i,v in enumerate(obsnet):
        ind = (full_theta == v[0]) & (full_phi == v[1])

        amp    = full_amp[ind]
        axi    = full_axi[ind]
        sig    = full_sig[ind]
        eigval = full_eigval[ind]
        eigvec = full_eigvec[ind]
        angmom = full_angmom[ind]
        q1     = full_q1[ind]
        q2     = full_q2[ind]
        q3     = full_q3[ind]
        q4     = full_q4[ind]

        # calculating the kinematic ratio for this perspective:
        kinrat = amp/sig

        # now determining the difference in angle between the rotation axis
        # and the projection of the angular momenutm (in 3D) in the plane
        # of the sky...

        # load the appropriate transformation matrix
        mtrans = load(datadir+'dataProducts/transMat/'+\
                      'TM'+'t'+str(int(v[0]))+'p'+str(int(v[1]))+'.npy')

        # projected angular momentum
        projangmom = []

        # now choosing which systems are most similar to M31
        idx = [True] *len(q1)
        idx = array(idx)

        for ll in range(len(q1)):
            ta = azym_sim_test_mean(q1[ll],q2[ll],q3[ll],q4[ll],sig = sigma)

            if ((len(ta) == 2) and
                (any([tmp < 0 for tmp in ta])) and
                (any([tmp > 0 for tmp in ta]))):
                pass
            else:
                idx[ll] = False

        # do the projection on the sky..
        for ll,am in enumerate(angmom):
            projangmom.append(coords_transform(mtrans, array(am)))
        # converting to array and transposing for convenience
        projangmom = array(projangmom).T

        # calculate the position angle of the angular momentum
        PAangmom = posAngCal(projangmom[0],projangmom[1])

        # find the difference between the rotation axis and PAangmom
        angdiff = 180.0 - abs(abs(rad2deg(PAangmom) - axi) - 180.0)

        # the orientation of the axis is arbitrary +/- 180 deg once it is set
        angdiff[angdiff > 90.] = 180.0 - angdiff[angdiff >90]

        # calculate the Spearman correlation rank:
        print 'The Spearman correlation rank for perspective ', v,' is ', \
               '%-1.3f' % (spearmanr(angdiff,kinrat)[0])
        print 'The Spearman correlation rank for the selected sample is ', \
               '%-1.3f' % (spearmanr(angdiff[idx],kinrat[idx])[0])

        # Writing the result to file
        # calculate the Spearman correlation rank:
        print >>fres, 'The Spearman correlation rank for perspective ', \
                v, ' is ', '%-1.3f' % (spearmanr(angdiff,kinrat)[0])
        print >>fres, 'The Spearman correlation rank for the selected sample',\
                ' is ', '%-1.3f' % (spearmanr(angdiff[idx],kinrat[idx])[0])

        full_idx.append(idx)
        full_kinrat.append(kinrat)
        full_angdif.append(angdiff)

        # clean up
        del angdiff, kinrat, projangmom, PAangmom, amp, axi, sig, eigvec,
        del eigval, q1, q2, q3, q4, angmom


    # doing the neat conversions for convenience
    idx     = array(full_idx).flatten()
    kinrat  = array(full_kinrat).flatten()
    angdiff = array(full_angdif).flatten()

    # calculating the Spearman rank coefficient for the entire halo
    print
    print 'Spearman rank coefficient for all instances in the halo: ', \
    '%-1.3f' % (spearmanr(angdiff,kinrat)[0])
    print 'Spearman rank coefficient for all selected instances in the halo: ',\
    '%-1.3f' % (spearmanr(angdiff[idx],kinrat[idx])[0])

    # Writing the results to file
    # calculating the Spearman rank coefficient for the entire halo
    print >>fres, ''
    print >>fres, 'Spearman rank coefficient for all instances in the halo :', \
            '%-1.3f' % (spearmanr(angdiff,kinrat)[0])
    print >>fres, 'Spearman rank coefficient for all selected instances in', \
            'halo :','%-1.3f' % (spearmanr(angdiff[idx],kinrat[idx])[0])

    # The calculation is done
    # Now do the figure...

    p.figure(figsize = (6,4),dpi = 100,facecolor = 'w',edgecolor = 'K')
    p.xlabel(r'$|L_{\rm GCS,proj} - \theta_0|$ [deg]', fontsize = 12)
    p.ylabel(r'$A/\sigma_0$', fontsize=12)
    p.xlim([0,90])
    # p.ylim([-0.05,max(kinrat)+0.1])
    p.ylim([-0.05,5.0])
    minorticks(mtnum=4, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdiff,kinrat, color='black', lw=0.5, s=5)
    p.scatter([],[], color='white', lw=0.1, s=0.01, label='all viewing angles')
    p.legend(loc = 1, frameon=False, prop={'size':10}, scatterpoints = 1)
    # p.scatter(angdiff[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # save the figure to disk
    p.savefig(datadir + 'plots/angmom' + halo + '.pdf',
              dpi = 120,
              bbox_inches = 'tight')
    # clean up
    del idx, kinrat, angdiff
    print 'Angular momentum correclation part completed'


################################################################################
def WAnalysisTask7(obsnet, datadir, datafile, halo, sigma, fres):
    '''
    Exactly the same idea as with WAnalysisTask6, but now instead of working
    with the angular momentum, the quantities of interest are the eigenvectors
    of the inertia tensor of each GC system.

    The eigenvectors are ordered according to the value of their corresponding
    eigenvalues e1 > e2 > e3. A multiplot is produced, each eigenvector
    per plot.
    '''

    print >>fres, ''
    print >>fres, 'WAnalysisTask7'
    print >>fres, ''

    # loading up the data, which is saved via cPickle
    data = cPickle.load(open(datadir+datafile, 'rb'))
    # because of the mess that pickle creates, unpack things manually..
    full_theta    = array(zip(*data)[0])
    full_phi      = array(zip(*data)[1])
    full_instance = array(zip(*data)[2])
    full_amp      = array(zip(*data)[3])
    full_axi      = array(zip(*data)[4])
    full_sig      = array(zip(*data)[5])
    full_eigval   = array(zip(*data)[6])
    full_eigvec   = array(zip(*data)[7])
    full_angmom   = array(zip(*data)[8])
    full_q1       = array(zip(*data)[9])
    full_q2       = array(zip(*data)[10])
    full_q3       = array(zip(*data)[11])
    full_q4       = array(zip(*data)[12])

    print 'Starting the part with the Inertia Tensor analysis...'

    full_idx    = []
    full_dif1   = []
    full_dif2   = []
    full_dif3   = []
    full_kinrat = []

    rad = 1000. # distance of the observer from the centre of the galaxy
    for i,v in enumerate(obsnet):
        ind = (full_theta == v[0]) & (full_phi == v[1])

        amp    = full_amp[ind]
        axi    = full_axi[ind]
        sig    = full_sig[ind]
        eigval = full_eigval[ind]
        eigvec = full_eigvec[ind]
        angmom = full_angmom[ind]
        q1     = full_q1[ind]
        q2     = full_q2[ind]
        q3     = full_q3[ind]
        q4     = full_q4[ind]

        kinrat = amp/sig

        # Now determining the difference in angle between the principal axis
        # of the GC system and the rotation axis that was determined
        # (everything is projected in the plane of the sky)

        # load the appropriate transformation matrix
        mtrans = load(datadir+'dataProducts/transMat/'+\
                      'TM'+'t'+str(int(v[0]))+'p'+str(int(v[1]))+'.npy')

        # the projected principal axes
        proj_e1 = []
        proj_e2 = []
        proj_e3 = []

        # now choosing which systems are most similar to M31
        idx = [True] * len(q1)
        idx = array(idx)

        for ll in range(len(q1)):
            ta = azym_sim_test_mean(q1[ll],q2[ll],q3[ll],q4[ll],sig=sigma)

            if ((len(ta) == 2) and
                (any([tmp < 0 for tmp in ta])) and
                (any([tmp > 0 for tmp in ta]))):
                pass
            else:
                idx[ll] = False

        # dealing with the eigenvectors..
        for ll in range(len(q1)):
            # ordering the eigenvectors by length (e1>e2>e3)
            order = (rankdata(eigval[ll], method = 'min')).astype(int)
            order = len(order) - order

            # projecting them on the plane of observations
            proj_e1.append(coords_transform(mtrans,
                                            eigvec[ll,order.tolist().index(0)]))
            proj_e2.append(coords_transform(mtrans,
                                            eigvec[ll,order.tolist().index(1)]))
            proj_e3.append(coords_transform(mtrans,
                                            eigvec[ll,order.tolist().index(2)]))

        # converting to array and transposing for convenience
        proj_e1 = array(proj_e1).T
        proj_e2 = array(proj_e2).T
        proj_e3 = array(proj_e3).T

        # position angle of each eigenvector, in degrees for convenience
        pa_e1   = rad2deg(posAngCal(proj_e1[0], proj_e1[1]))
        pa_e2   = rad2deg(posAngCal(proj_e2[0], proj_e2[1]))
        pa_e3   = rad2deg(posAngCal(proj_e3[0], proj_e3[1]))

        # finding the difference between the rotation axis and principal axes
        angdif1 = 180.0 - abs(abs(pa_e1 - axi) - 180.0)
        angdif2 = 180.0 - abs(abs(pa_e2 - axi) - 180.0)
        angdif3 = 180.0 - abs(abs(pa_e3 - axi) - 180.0)

        angdif1[angdif1 > 90.] = 180.0 - angdif1[angdif1 >90]
        angdif2[angdif2 > 90.] = 180.0 - angdif2[angdif2 >90]
        angdif3[angdif3 > 90.] = 180.0 - angdif3[angdif3 >90]

        # The spearman coefficient rank for each of the primary axes:
        print 'Perspective: ', v
        print 'Spearman coefficient rank for each primary axes: ',\
        '%-1.3f %-1.3f %-1.3f' % (spearmanr(angdif1,kinrat)[0],
                                  spearmanr(angdif2,kinrat)[0],
                                  spearmanr(angdif3,kinrat)[0])

        print 'Spearman coefficient rank for the selected sample for each', \
        'primary axes: ',\
        '%-1.3f %-1.3f %-1.3f' % (spearmanr(angdif1[idx],kinrat[idx])[0],
                                  spearmanr(angdif2[idx],kinrat[idx])[0],
                                  spearmanr(angdif3[idx],kinrat[idx])[0])

        # Writing the results to a file
        # The spearman coefficient rank for each of the primary axes:
        print >>fres, 'Perspective: ', v
        print >>fres, 'Spearman coefficient rank for each primary axes: ',\
        '%-1.3f %-1.3f %-1.3f' % (spearmanr(angdif1,kinrat)[0],
                                  spearmanr(angdif2,kinrat)[0],
                                  spearmanr(angdif3,kinrat)[0])

        print >>fres, 'Spearman coefficient rank for the selected sample for',\
                'each primary axes: ',\
        '%-1.3f %-1.3f %-1.3f' % (spearmanr(angdif1[idx],kinrat[idx])[0],
                                  spearmanr(angdif2[idx],kinrat[idx])[0],
                                  spearmanr(angdif3[idx],kinrat[idx])[0])

        # adding the to the full lists for the final product
        full_idx.append(idx)
        full_dif1.append(angdif1)
        full_dif2.append(angdif2)
        full_dif3.append(angdif3)
        full_kinrat.append(kinrat)

        # clean up
        del amp, axi, sig, kinrat, eigvec, eigval, angmom, q1,q2,q3,q4
        del angdif3, angdif1, angdif2, pa_e3, pa_e2, pa_e1, proj_e3, proj_e2
        del proj_e1, order

    # done for convenience..
    idx     = array(full_idx).flatten()
    kinrat  = array(full_kinrat).flatten()
    angdif1 = array(full_dif1).flatten()
    angdif2 = array(full_dif2).flatten()
    angdif3 = array(full_dif3).flatten()

    # Spearman rank for the entire halo (all systems in all perspectives)
    print
    print 'Spearman rank coefficient for all instances in the halo: ', \
    '%-1.3f %-1.3f %-1.3f' % (spearmanr(angdif1,kinrat)[0],
                              spearmanr(angdif2,kinrat)[0],
                              spearmanr(angdif3,kinrat)[0])
    print 'Spearman coefficient rank for the selected sample for each', \
    'primary axes: ',\
    '%-1.3f %-1.3f %-1.3f' % (spearmanr(angdif1[idx],kinrat[idx])[0],
                              spearmanr(angdif2[idx],kinrat[idx])[0],
                              spearmanr(angdif3[idx],kinrat[idx])[0])

    # Now write the same thing to a file
    print >>fres, ''
    print >>fres, 'Spearman rank coefficient for all instances in the halo: ',\
    '%-1.3f %-1.3f %-1.3f' % (spearmanr(angdif1,kinrat)[0],
                              spearmanr(angdif2,kinrat)[0],
                              spearmanr(angdif3,kinrat)[0])
    print >>fres, 'Spearman coefficient rank for the selected sample for each',\
    'primary axes: ',\
    '%-1.3f %-1.3f %-1.3f' % (spearmanr(angdif1[idx],kinrat[idx])[0],
                              spearmanr(angdif2[idx],kinrat[idx])[0],
                              spearmanr(angdif3[idx],kinrat[idx])[0])

    # The calculations are done
    # Now do the figure
    p.figure(figsize = (9,6), dpi = 100, facecolor = 'w', edgecolor = 'K')
    # for e1
    su1 = p.subplot(1,3,1)
    p.xlabel(r'$|e^{1}_{\rm proj} - \theta|$ [deg]', fontsize = 10)
    p.ylabel('kinematic ratio', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif1,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif1[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e2
    su2 = p.subplot(1,3,2, sharey = su1)
    p.xlabel(r'$|e^{2}_{\rm proj} - \theta|$ [deg]', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif2,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif2[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e3
    su3 = p.subplot(1,3,3, sharey = su1)
    p.xlabel(r'$|e^{3}_{\rm proj} - \theta|$ [deg]', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif3,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif3[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

    # tight layout
    p.tight_layout()
    # save the figure to disk
    p.savefig(datadir + 'plots/inertiaAxes' + halo + '.pdf',
              dpi = 120,
              bbox_inches = 'tight')
    # clean up
    del full_dif1, full_kinrat, full_dif3, full_dif2,
    del kinrat, angdif1, angdif2, angdif3


################################################################################
def WAnalysisTask8(obsnet, datadir, datafile, halo, sigma, fres):
    '''
    Comparing the angle of the angular momentum vector (3D) of the entire system
    of particles (the whole halo), to that of the individual angular momentum
    vector of each GC system.

    The difference in angle is then correlated to the kinematic ratio, and
    the correlation is quantified via the Spearman rank coefficient.

    Done per perspective and for everything put together. The resulting figure
    is for every system of this halo (for all perspecives).
    '''

    print >>fres, ''
    print >>fres, 'WAnalysisTask8'
    print >>fres, ''

    print 'Starting the 3D angular momentum comparison...'

    # location of the file to read
    tmploc = datadir + 'dataProducts/'
    tmpfil ='ScaledParticle.Data.npy'
    halo_eigval, halo_eigvec, halo_angmom = WM.WInertia_fullhalo(tmploc,tmpfil)

    # loading up the data, which is saved via cPickle
    data = cPickle.load(open(datadir+datafile, 'rb'))
    # because of the mess that pickle creates, unpack things manually..
    full_theta    = array(zip(*data)[0])
    full_phi      = array(zip(*data)[1])
    full_instance = array(zip(*data)[2])
    full_amp      = array(zip(*data)[3])
    full_axi      = array(zip(*data)[4])
    full_sig      = array(zip(*data)[5])
    full_eigval   = array(zip(*data)[6])
    full_eigvec   = array(zip(*data)[7])
    full_angmom   = array(zip(*data)[8])
    full_q1       = array(zip(*data)[9])
    full_q2       = array(zip(*data)[10])
    full_q3       = array(zip(*data)[11])
    full_q4       = array(zip(*data)[12])

    full_idx = []
    full_kinrat = []
    full_angdif = []

    rad = 1000. # distance of the observer from the centre of the galaxy
    for i,v in enumerate(obsnet):
        ind = (full_theta == v[0]) & (full_phi == v[1])

        amp    = full_amp[ind]
        axi    = full_axi[ind]
        sig    = full_sig[ind]
        eigval = full_eigval[ind]
        eigvec = full_eigvec[ind]
        angmom = full_angmom[ind]
        q1     = full_q1[ind]
        q2     = full_q2[ind]
        q3     = full_q3[ind]
        q4     = full_q4[ind]

        # now choosing which systems are most similar to M31
        idx = [True] *len(q1)
        idx = array(idx)

        for ll in range(len(q1)):
            ta = azym_sim_test_mean(q1[ll],q2[ll],q3[ll],q4[ll],sig = sigma)

            if ((len(ta) == 2) and
                (any([tmp < 0 for tmp in ta])) and
                (any([tmp > 0 for tmp in ta]))):
                pass
            else:
                idx[ll] = False

        # calculating the kinematic ratio for this perspective:
        kinrat = amp/sig

        # now determine the difference in angle between the (3D) angular
        # momentum of the entire halo, to that of each GC system
        angdif = zeros(len(angmom))

        # doing it manually.. (for some reason it will not work vectorially)
        for ii in range(len(angmom)):
            angdif[ii] = rad2deg(arccos((dot(angmom[ii],halo_angmom))/\
                                  (linalg.norm(angmom[ii])*linalg.norm(halo_angmom))))

        # calculate the Spearman correlation rank:
        print 'The Spearman correlation rank for perspective ', v,' is ', \
               '%-1.3f' % (spearmanr(angdif,kinrat)[0])
        print 'The Spearman correlation rank for the selected sample is ', \
               '%-1.3f' % (spearmanr(angdif[idx],kinrat[idx])[0])

        # Writing the result to file
        # calculate the Spearman correlation rank:
        print >>fres, 'The Spearman correlation rank for perspective ', \
                v, ' is ', '%-1.3f' % (spearmanr(angdif,kinrat)[0])
        print >>fres, 'The Spearman correlation rank for the selected sample',\
                ' is ', '%-1.3f' % (spearmanr(angdif[idx],kinrat[idx])[0])

        full_idx.append(idx)
        full_kinrat.append(kinrat)
        full_angdif.append(angdif)

        # clean up
        del angdif, kinrat, amp, axi, sig, eigvec,
        del eigval, q1, q2, q3, q4, angmom

    # doing the neat conversions for convenience
    idx     = array(full_idx).flatten()
    kinrat  = array(full_kinrat).flatten()
    angdiff = array(full_angdif).flatten()

    # calculating the Spearman rank coefficient for the entire halo
    print
    print 'Spearman rank coefficient for all instances in the halo: ', \
    '%-1.3f' % (spearmanr(angdiff,kinrat)[0])
    print 'Spearman rank coefficient for all selected instances in the halo: ',\
    '%-1.3f' % (spearmanr(angdiff[idx],kinrat[idx])[0])

    # Writing the results to file
    # calculating the Spearman rank coefficient for the entire halo
    print >>fres, ''
    print >>fres, 'Spearman rank coefficient for all instances in the halo :', \
            '%-1.3f' % (spearmanr(angdiff,kinrat)[0])
    print >>fres, 'Spearman rank coefficient for all selected instances in the \
            halo :','%-1.3f' % (spearmanr(angdiff[idx],kinrat[idx])[0])

    # The calculation is done
    # Now do the figure...
    p.figure(figsize = (6,4),dpi = 120,facecolor = 'w',edgecolor = 'K')
    p.xlabel(r'$|L_{\rm halo} - L_{\rm GCS}|$ [deg]', fontsize = 12)
    p.ylabel(r'$A/\sigma_0$', fontsize=12)
    # p.xlim([0,angdiff.max()+10.0])
    p.xlim([0,180])
    # p.ylim([-0.05,max(kinrat)+0.1])
    p.ylim([-0.05, 2.5])
    minorticks(mtnum=4, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdiff,kinrat, color='black', lw=0.5, s=5)
    p.scatter([],[], color='white', lw=0.1, s=0.01, label='all viewing angles')
    # p.scatter(angdiff[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    # p.plot([0.0,angdiff.max()+10.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    p.plot([0.0,180.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # save the figure to disk
    p.legend(loc = 1, frameon=False, prop={'size':10}, scatterpoints = 1)
    p.savefig(datadir + 'plots/angmom-3D' + halo + '.pdf',
              dpi = 120,
              bbox_inches = 'tight')
    # clean up
    del idx, kinrat, angdiff, full_idx, full_angdif, full_kinrat
    print 'The 3D angular momentum comparison is completed.'



################################################################################
def WAnalysisTask9(obsnet, datadir, datafile, halo, sigma):
    '''
    This task creates the compilation of plots form WAnalysisTask6,7,8 but
    for all perspectives. The plots are put into a separate folder in the
    relevant plots directory

    Similar is done by WAnalysisTask4, but only for the perspectives showing
    minimum and maximum rotation signature.
    '''

    print
    print 'Creating diagnostic plots for each perspective...'

    # loading up the data, which is saved via cPickle
    data = cPickle.load(open(datadir+datafile, 'rb'))
    # because of the mess that pickle creates, unpack things manually..
    full_theta    = array(zip(*data)[0])
    full_phi      = array(zip(*data)[1])
    full_instance = array(zip(*data)[2])
    full_amp      = array(zip(*data)[3])
    full_axi      = array(zip(*data)[4])
    full_sig      = array(zip(*data)[5])
    full_eigval   = array(zip(*data)[6])
    full_eigvec   = array(zip(*data)[7])
    full_angmom   = array(zip(*data)[8])
    full_q1       = array(zip(*data)[9])
    full_q2       = array(zip(*data)[10])
    full_q3       = array(zip(*data)[11])
    full_q4       = array(zip(*data)[12])

    # clean up
    del data

    # directory in which the plots are to be saved
    savedir = datadir + 'plots/perspective/'

    # check if directory exists. If it does not, create it!
    if not os.path.exists(savedir):
        os.makedirs(savedir)

    # Now we can get to work to calcula things which are needed for the plots..
    # calculating the total angular momentum of the entire halo...
    # this is needed for the creating of the coming plot.
    fullhalodata = datadir + 'dataProducts/ScaledParticle.Data.npy'

    # Calculating the full angular momentum of the entire halo
    # read in the ids of all particles:
    data = load(fullhalodata)
    xx        = data[0]
    yy        = data[1]
    zz        = data[2]
    all_ids   = data[6]

    # selecting IDs outside the 30 kpc limit
    rr  = sqrt(xx**2.0 + yy**2.0 + zz**2.0)
    ind = rr >= 30.
    ids = all_ids[ind]

    # Now calculating the principal axes of the entire halo +
    # the angular momentum
    halo_eigval, halo_eigvec, halo_angmom = WM.WInertia(ids,fullhalodata)

    # clean up
    del data, all_ids, xx, yy, zz, rr, ind, ids

    # the master loop begins here.. it deals with individual perspectives
    for i,v in enumerate(obsnet):
        ind = (full_theta == v[0]) & (full_phi == v[1])
        # selecting the relevant data for this perspective
        amp    = full_amp[ind]
        axi    = full_axi[ind]
        sig    = full_sig[ind]
        eigval = full_eigval[ind]
        eigvec = full_eigvec[ind]
        angmom = full_angmom[ind]
        q1     = full_q1[ind]
        q2     = full_q2[ind]
        q3     = full_q3[ind]
        q4     = full_q4[ind]

        # checking which systems are a bit more similar to M31
        idx = [True] * len(q1)
        idx = array(idx)

        # constructing the index for systems similar to M31
        for k in range(len(q1)):
            ta = azym_sim_test_mean(q1[k],q2[k],q3[k],q4[k], sig = 1.0)

            if ta[0] == 0:
                idx[k] = False
            else:
                idx[k] = True

        # calculating the kinematic ratio for this perspective:
        kinrat = amp/sig

        # load the appropriate transformation matrix
        mtrans = load(datadir+'dataProducts/transMat/'+\
                      'TM'+'t'+str(int(v[0]))+'p'+str(int(v[1]))+'.npy')


        # Placeholders for the projected quantities
        # (the angular momentum and the inertia vectors)
        projangmom = []
        proj_e1    = []
        proj_e2    = []
        proj_e3    = []

        # projecting things on the sky..
        # the angular momentum..
        for ll,am in enumerate(angmom):
            projangmom.append(coords_transform(mtrans, array(am)))
        # the inertia tensor eigenvectors
        # dealing with the eigenvectors...
        for ll in range(len(q1)):
            # ordering the eigenvectos by length (e1>e2>e3)
            order = (rankdata(eigval[ll], method = 'min')).astype(int)
            order = len(order) - order
            # projecting them on the plane of the observations
            proj_e1.append(coords_transform(mtrans,
                                            eigvec[ll,order.tolist().index(0)]))
            proj_e2.append(coords_transform(mtrans,
                                            eigvec[ll,order.tolist().index(1)]))
            proj_e3.append(coords_transform(mtrans,
                                            eigvec[ll,order.tolist().index(2)]))

        # now this is for the 3D difference in angle between the angular
        # momentum of the GCsystem and the direction of the inertia tensor
        # eigenvectors
        ad3d_e1 = zeros(len(angmom))
        ad3d_e2 = zeros(len(angmom))
        ad3d_e3 = zeros(len(angmom))
        for ll in range(len(angmom)):
            # ordering the eigenvectos by length (e1>e2>e3)
            order = (rankdata(eigval[ll], method = 'min')).astype(int)
            order = len(order) - order
            tmp_e1 = eigvec[ll,order.tolist().index(0)]
            tmp_e2 = eigvec[ll,order.tolist().index(1)]
            tmp_e3 = eigvec[ll,order.tolist().index(2)]
            ad3d_e1[ll] = rad2deg(arccos((dot(angmom[ll],tmp_e1))/\
                                         (linalg.norm(angmom[ll])*
                                          linalg.norm(tmp_e1))))
            ad3d_e2[ll] = rad2deg(arccos((dot(angmom[ll],tmp_e2))/\
                                         (linalg.norm(angmom[ll])*
                                          linalg.norm(tmp_e2))))
            ad3d_e3[ll] = rad2deg(arccos((dot(angmom[ll],tmp_e3))/\
                                         (linalg.norm(angmom[ll])*
                                          linalg.norm(tmp_e3))))

        # the orientation of the inertia tensor eigenvectors is arbitrary so
        ad3d_e1[ad3d_e1 > 90.] = 180.0 - ad3d_e1[ad3d_e1 >90]
        ad3d_e2[ad3d_e2 > 90.] = 180.0 - ad3d_e2[ad3d_e2 >90]
        ad3d_e3[ad3d_e3 > 90.] = 180.0 - ad3d_e3[ad3d_e3 >90]

        # converting the lists to arrays and transposing them for convenience
        projangmom = array(projangmom).T
        proj_e1    = array(proj_e1).T
        proj_e2    = array(proj_e2).T
        proj_e3    = array(proj_e3).T

        # calculate the position angle of the angular momentum
        PAangmom = posAngCal(projangmom[0],projangmom[1])

        # position angle of each eigenvector, in degrees for convenience
        pa_e1   = rad2deg(posAngCal(proj_e1[0], proj_e1[1]))
        pa_e2   = rad2deg(posAngCal(proj_e2[0], proj_e2[1]))
        pa_e3   = rad2deg(posAngCal(proj_e3[0], proj_e3[1]))

        # find the difference between the rotation axis and PAangmom
        angdiff = 180.0 - abs(abs(rad2deg(PAangmom) - axi) - 180.0)

        # # the orientation of the rotation axis is arbitrary so ...
        angdiff[angdiff > 90.] = 180.0 - angdiff[angdiff >90]

        # finding the difference between the rotation axis and principal axes
        angdif1 = 180.0 - abs(abs(pa_e1 - axi) - 180.0)
        angdif2 = 180.0 - abs(abs(pa_e2 - axi) - 180.0)
        angdif3 = 180.0 - abs(abs(pa_e3 - axi) - 180.0)

        # The direction of the eigenvectors is arbitrary so ..
        angdif1[angdif1 > 90.] = 180.0 - angdif1[angdif1 >90]
        angdif2[angdif2 > 90.] = 180.0 - angdif2[angdif2 >90]
        angdif3[angdif3 > 90.] = 180.0 - angdif3[angdif3 >90]

        # now determine the difference in angle between the (3D) angular
        # momentum of the entire halo, to that of each GC system
        angdif3D = zeros(len(angmom))
        # doing it manually..
        for ii in range(len(angmom)):
            angdif3D[ii] = rad2deg(arccos((dot(angmom[ii],halo_angmom))/\
                                   (linalg.norm(angmom[ii])*
                                    linalg.norm(halo_angmom))))

        # Now.. everything of interest is calculated..
        # Let the plotting begin!

        # defining the general figure properties
        p.figure(figsize=(9,6), dpi=100, facecolor='w', edgecolor='K')
        # defining the subplots arrangement
        gs = p.GridSpec(3,3)

        # The plot of the difference in the angle between the position angle of
        # the rotation axis of the GC system and the projection of the 3D
        # angular momentum on the sky on one side, and the kinematic ratio
        # on another side.
        p.subplot(gs[0,:])
        p.title('Perspective: ' + str(v) , fontsize = 8)
        p.xlabel(r'$|L_{\rm GCS,proj} - \theta|$ [deg]', fontsize = 8)
        p.ylabel('kinematic ratio', fontsize = 8)
        p.xlim([0,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=4, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdiff,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdiff[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

        # now the plots with the eigenvectors...
        p.subplot(gs[1,0])
        p.xlabel(r'$|e^{1}_{\rm proj} - \theta|$ [deg]', fontsize = 8)
        p.ylabel('kinematic ratio', fontsize = 8)
        p.xlim([0,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif1,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif1[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        # for e2
        p.subplot(gs[1,1])
        p.xlabel(r'$|e^{2}_{\rm proj} - \theta|$ [deg]', fontsize = 8)
        p.xlim([0,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif2,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif2[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        # for e3
        p.subplot(gs[1,2])
        p.xlabel(r'$|e^{3}_{\rm proj} - \theta|$ [deg]', fontsize = 8)
        p.xlim([0,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif3,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif3[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

        # The plot Comparing the angle of the angular momentum vector (3D)
        # of the entire system of particles (the whole halo), to that of the
        # individual angular momentum vector of each GC system.
        p.subplot(gs[2,:])
        p.xlabel(r'$|L_{\rm halo} - L_{\rm GCS}|$ [deg]', fontsize = 8)
        p.ylabel('kinematic ratio', fontsize = 8)
        p.xlim([0,angdif3D.max()+10.0])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=4, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif3D,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif3D[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,angdif3D.max()+10.0],[0.93,0.93],ls=':',lw=0.9,color='black')

        # keeping it tight
        p.tight_layout()

        # define the plot output name (pon)
        pon = 'analysis-t'+str(int(v[0]))+'p'+str(int(v[1]))+'.pdf'

        p.savefig(savedir+pon, dpi=120, bbox_inches='tight')
        p.clf()

        # Now plot the same plots but separately..
        # This is done in case I want to nicely use individual plots for the
        # paper instead of the multiplot figure.

        # Difference between the projected angular momentum of the GCS
        # and the rotation axis.
        # defining the general figure properties
        p.figure(figsize=(6,4), dpi=100, facecolor='w', edgecolor='K')
        p.xlabel(r'$|L_{\rm GCS,proj} - \theta_0|$ [deg]', fontsize = 12)
        p.ylabel(r'$A/\sigma_0$', fontsize  = 12)
        p.xlim([0,90])
        p.ylim([-0.05,1.7])
        minorticks(mtnum=4, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdiff,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdiff[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        # p.scatter([],[],color='white',lw=1,s=1,label='favourable orientation')
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        p.legend(loc = 1, frameon=False, handlelength=4, prop={'size':10})

        # define the plot output name (pon)
        pon = 'angmom-t'+str(int(v[0]))+'p'+str(int(v[1]))+'.pdf'

        p.savefig(savedir+pon, dpi=120, bbox_inches='tight')
        p.clf()

        # The figure with the comparison between the inertia tensor vectors
        # and the rotation axis
        p.figure(figsize=(6,4), dpi=100, facecolor='w', edgecolor='K')
        su1 = p.subplot(1,3,1)
        p.xlabel(r'$|e^{1}_{\rm proj} - \theta|$ [deg]', fontsize = 12)
        p.ylabel('kinematic ratio', fontsize = 12)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif1,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif1[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        su2 = p.subplot(1,3,2, sharey = su1)
        p.xlabel(r'$|e^{2}_{\rm proj} - \theta|$ [deg]', fontsize = 12)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif2,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif2[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        su3 = p.subplot(1,3,3, sharey = su1)
        p.xlabel(r'$|e^{3}_{\rm proj} - \theta|$ [deg]', fontsize = 12)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif3,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif3[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

        # define the plot output name (pon)
        pon = 'inertia-t'+str(int(v[0]))+'p'+str(int(v[1]))+'.pdf'

        p.tight_layout()

        p.savefig(savedir+pon, dpi=120, bbox_inches='tight')
        p.clf()

        # The plot comparing the 3D angular momentum of the halo to the 3D
        # angular momentum of a GCS

        p.figure(figsize=(6,4), dpi=100, facecolor='w', edgecolor='K')
        p.xlabel(r'$|L_{\rm halo} - L_{\rm GCS}|$ [deg]', fontsize = 12)
        p.ylabel('kinematic ratio', fontsize = 12)
        p.xlim([0,angdif3D.max()+10.0])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=4, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif3D,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif3D[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,angdif3D.max()+10.0],[0.93,0.93],ls=':',lw=0.9,color='black')

        # define the plot output name (pon)
        pon = 'angmom3D-t'+str(int(v[0]))+'p'+str(int(v[1]))+'.pdf'

        p.savefig(savedir+pon, dpi=120, bbox_inches='tight')
        p.clf()

        # Now the new set of figures, the 3d angle between the angular momentum
        # of a GCS and the orientation of the eigenvectors of the inertia
        # tensor

        p.figure(figsize=(6,4), dpi=100, facecolor='w', edgecolor='K')
        su1 = p.subplot(1,3,1)
        p.xlabel(r'$|e^{1}_{\rm proj} - L_{\rm GCS}|$ [deg]', fontsize = 12)
        p.ylabel('kinematic ratio', fontsize = 12)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=4, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(ad3d_e1,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(ad3d_e1[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        su2 = p.subplot(1,3,2, sharey = su1)
        p.xlabel(r'$|e^{2}_{\rm proj} - L_{\rm GCS}|$ [deg]', fontsize = 12)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(ad3d_e2,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(ad3d_e2[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        su3 = p.subplot(1,3,3, sharey = su1)
        p.xlabel(r'$|e^{3}_{\rm proj} - L_{\rm GCS}|$ [deg]', fontsize = 12)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(ad3d_e3,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(ad3d_e3[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

        # define the plot output name (pon)
        pon = 'inertia3D-t'+str(int(v[0]))+'p'+str(int(v[1]))+'.pdf'

        p.tight_layout()

        p.savefig(savedir+pon, dpi=120, bbox_inches='tight')
        p.clf()



        # clean up
        del idx, angdif3, angdif2, angdif1, angdif3D, kinrat, pon, ta
        del pa_e1, pa_e2, pa_e3, projangmom, proj_e3, proj_e2, proj_e1, PAangmom
        del ind, amp, axi, sig, eigvec, eigval, angmom, q1, q2, q3, q4, mtrans

    print
    print 'All diagnostic plots completed succesfully!'
    print

################################################################################
def WAnalysisTask10(obsnet, datadir, datafile, halo, sigma):
    '''
    This is similar to WAnalysisTask1, it creates a multiplot with histograms
    the position angle of the rotation axis for all instances of each
    perspective the halo os observed from.
    '''

    # loading up the data, which is saved via cPickle
    data = cPickle.load(open(datadir+datafile, 'rb'))
    # because of the mess that pickle creates, unpack things manually
    full_theta    = array(zip(*data)[0])
    full_phi      = array(zip(*data)[1])
    full_instance = array(zip(*data)[2])
    full_amp      = array(zip(*data)[3])
    full_axi      = array(zip(*data)[4])
    full_sig      = array(zip(*data)[5])
    full_eigval   = array(zip(*data)[6])
    full_eigvec   = array(zip(*data)[7])
    full_angmom   = array(zip(*data)[8])
    full_q1       = array(zip(*data)[9])
    full_q2       = array(zip(*data)[10])
    full_q3       = array(zip(*data)[11])
    full_q4       = array(zip(*data)[12])

    print 'Creating the multiplot...'

    # Defining the master figure

    p.figure(figsize=(8,11), dpi=120, facecolor='w', edgecolor='k')
    gs = p.GridSpec(5,4)

    # these control the locating of the subplots
    ii = 0
    jj = 0

    # the master loop begins
    for i,v in enumerate(obsnet):
        # selecting and reading in the relevant portion of the data
        ind    = (full_theta == v[0]) & (full_phi == v[1])

        axi    = full_axi[ind]
        q1     = full_q1[ind]
        q2     = full_q2[ind]
        q3     = full_q3[ind]
        q4     = full_q4[ind]

        # calculate the mean of the rotation axis
        mean_axi = bs.location(axi)[0]

        # now choosing which systems are most similar to M31
        idx = [True] * len(q1)
        idx = array(idx)

        # constructing the index showing which systems are akin to M31
        for k in range(len(q1)):
            ta = azym_sim_test_mean(q1[k],q2[k],q3[k],q4[k],sig=sigma)

            if ((len(ta) == 2) and
                (any([tmp < 0 for tmp in ta])) and
                (any([tmp > 0 for tmp in ta]))):
                pass
            else:
                idx[k] = False

        # the mean of the kinrat in the selected sample
        if len(axi[idx]) >= 10 :
            mean_axi_sub = bs.location(axi[idx])[0]
        else:
            mean_axi_sub = NaN

        # determining the location of the current subplot
        if jj == 4:
            jj = 0
            ii += 1
            # print ii, jj
            p.subplot(gs[ii,jj])
            jj += 1
        else:
            # print ii, jj
            p.subplot(gs[ii,jj])
            jj += 1

        # determining the bins of the histogram(s)
        bins = linspace(0.,360.,9.)
        # custom x-tics
        p.xticks(bins)
        # labels of the subplots
        p.xlabel(r'$\theta_0$', fontsize  = 7)
        p.ylabel('N', fontsize  = 7)
        # histogram, only done to determine the ylim of the subplots
        hh, ee = histogram(axi, bins = bins)
        p.ylim([0,int(round(max(hh)/10.0)*10.0) + 10])
        p.xlim([0,360])
        # the master histograms
        p.hist(axi,
               bins = bins,
               histtype = 'stepfilled',
               color = 'blue',
               ec = 'None',
               lw = 2,
               label = 'System ' + str(int(v[0])) + ' ' + str(int(v[1])))
        # the histogram for the data more similar to M31.
        # check first how many points are left, if any.
        if len(axi[idx]) == 0:
            pass
        else:
            p.hist(axi[idx],
                   bins = bins,
                   histtype = 'step',
                   color = 'black',
                   lw = 1.5,
                   ls = 'dashed')
        # this is only done to add a random text in the legend :)
        p.errorbar([],
                   [],
                   lw = 0,
                   ls = 'dashed',
                   c = 'white',
                   label = str(len(axi[idx])) + '/100')
        # the average line of the full sample
        p.plot([mean_axi,mean_axi],
               [0,int(round(max(hh)/10.0)*10.0)+10],
               lw = 0.5,
               ls = '-',
               color = 'black')
        # the average line of the selected sample
        p.plot([mean_axi_sub,mean_axi_sub],
               [0,int(round(max(hh)/10.0)*10.0)+10],
               lw = 0.5,
               ls = ':',
               color = 'black')
        # legend
        p.legend(loc = 1, frameon=False, prop={'size':5})
        # configuring the ticks with my custom rutine
        minorticks(ftsize=5, maw=1.0,mal=2.0,miw=0.5,mil=1.0)

    # clean up
    del data, ii,jj, axi, mean_axi, mean_axi_sub, q1, q2, q3, q4

    # formatting cause there are lots of subplots
    p.tight_layout()
    # saving the plot to disk
    p.savefig(datadir + 'plots/histaxi' + halo + '.pdf',
              dpi=120,
              bbox_inches = 'tight')
    p.clf()

################################################################################
def WAnalysisTask11(obsnet, datadir, datafile, halo, sigma):
    '''
    This is similar to WAnalysis7, I plot the difference between the position 
    angle of the rotation axis of the GC systems and the principal axis 
    of the DM halo vs the kinematic ratio

    The eigenvectors are ordered according to the value of their corresponding
    eigenvalues e1 > e2 > e3. A multiplot is produced, each eigenvector
    per plot.
    '''

    # Part 11
    print 'Starting the part with the angle between the rotation axis and the '
    print 'principal components of the Dark Matter halo'

    full_idx    = []
    full_kinrat = []
    full_dif1   = []
    full_dif2   = []
    full_dif3   = []

    # loading up the data, which is saved via cPickle
    data = cPickle.load(open(datadir+datafile, 'rb'))
    # because of the mess that pickle creates, unpack things manually..
    full_theta    = array(zip(*data)[0])
    full_phi      = array(zip(*data)[1])
    full_instance = array(zip(*data)[2])
    full_amp      = array(zip(*data)[3])
    full_axi      = array(zip(*data)[4])
    full_sig      = array(zip(*data)[5])
    full_eigval   = array(zip(*data)[6])
    full_eigvec   = array(zip(*data)[7])
    full_angmom   = array(zip(*data)[8])
    full_q1       = array(zip(*data)[9])
    full_q2       = array(zip(*data)[10])
    full_q3       = array(zip(*data)[11])
    full_q4       = array(zip(*data)[12])

    # clean up 
    del data

    # directory in which the plots are to be saved
    savedir = datadir + 'plots/perspective/'

    # check if directory exists. If it does not, create it!
    if not os.path.exists(savedir):
        os.makedirs(savedir)

    # read the principal axis of the DM halo..
    pa = load('/data/users/jovan/Aquarius/PresentDay/halo-' + halo + 
              '/principalAxis/pa'+halo+'.npy')
    # converting the axes to cartesian coords
    pax = sin(pa[0])*cos(pa[1])
    pay = sin(pa[0])*sin(pa[1])
    paz = cos(pa[0])

    rad = 1000.
    for i,v in enumerate(obsnet):
        ind = (full_theta == v[0]) & (full_phi == v[1])

        amp    = full_amp[ind]
        axi    = full_axi[ind]
        sig    = full_sig[ind]
        eigval = full_eigval[ind]
        eigvec = full_eigvec[ind]
        angmom = full_angmom[ind]
        q1     = full_q1[ind]
        q2     = full_q2[ind]
        q3     = full_q3[ind]
        q4     = full_q4[ind]

        kinrat = amp/sig

        # Now determining the difference in angle between the principal axis
        # of the dark matter halo and the rotation axiss 
        # (everything is projected in the plane of the sky)

        # load the appropriate transformation matrix
        mtrans = load(datadir+'dataProducts/transMat/'+\
                      'TM'+'t'+str(int(v[0]))+'p'+str(int(v[1]))+'.npy')

        # projecting the axes onto the plane of observations..
        xo = zeros(3)
        yo = zeros(3)
        for i in range(3):
            xo[i], yo[i] = coords_transform(mtrans, 
                                            array([pax[i],pay[i],paz[i]]))

        # calculate the position angle of the principal axes
        proj_pa1, proj_pa2, proj_pa3 = rad2deg(posAngCal(xo,yo))

        # now choosing which systems are most similar to M31
        idx = [True] * len(q1)
        idx = array(idx)

        for ll in range(len(q1)):
            ta = azym_sim_test_mean(q1[ll],q2[ll],q3[ll],q4[ll],sig=sigma)

            if ((len(ta) == 2) and
                (any([tmp < 0 for tmp in ta])) and
                (any([tmp > 0 for tmp in ta]))):
                pass
            else:
                idx[ll] = False

        # finding the difference in angle
        angdif1 = 180.0 - abs(abs(proj_pa1 - axi) - 180.0)
        angdif2 = 180.0 - abs(abs(proj_pa2 - axi) - 180.0)
        angdif3 = 180.0 - abs(abs(proj_pa3 - axi) - 180.0)

        # orientation of the principal axes does not matter thus...
        angdif1[angdif1 > 90.] = 180.0 - angdif1[angdif1 >90]
        angdif2[angdif2 > 90.] = 180.0 - angdif2[angdif2 >90]
        angdif3[angdif3 > 90.] = 180.0 - angdif3[angdif3 >90]

        # Do the plot but just for this one perspective
        p.figure(figsize=(6,4), dpi=100, facecolor='w', edgecolor='K')
        su1 = p.subplot(1,3,1)
        p.xlabel(r'$|e^{1}_{\rm proj} - \theta_0|$ [deg]', fontsize = 10)
        p.ylabel('kinematic ratio', fontsize = 10)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif1,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif1[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        su2 = p.subplot(1,3,2, sharey = su1)
        p.xlabel(r'$|e^{2}_{\rm proj} - \theta_0|$ [deg]', fontsize = 10)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif2,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif2[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        su3 = p.subplot(1,3,3, sharey = su1)
        p.xlabel(r'$|e^{3}_{\rm proj} - \theta_0|$ [deg]', fontsize = 10)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif3,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif3[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

        # define the plot output name (pon)
        pon = 'PAdm-t'+str(int(v[0]))+'p'+str(int(v[1]))+'.pdf'

        p.tight_layout()

        p.savefig(savedir+pon, dpi=120, bbox_inches='tight')
        p.clf()

        # adding the to the full lists for the final product
        full_idx.append(idx)
        full_dif1.append(angdif1)
        full_dif2.append(angdif2)
        full_dif3.append(angdif3)
        full_kinrat.append(kinrat)

    # done for convenience..
    idx     = array(full_idx).flatten()
    kinrat  = array(full_kinrat).flatten()
    angdif1 = array(full_dif1).flatten()
    angdif2 = array(full_dif2).flatten()
    angdif3 = array(full_dif3).flatten()

    # The calculations are done
    # Now do the figure
    p.figure(figsize = (9,6), dpi = 100, facecolor = 'w', edgecolor = 'K')
    # for e1
    su1 = p.subplot(1,3,1)
    p.xlabel(r'$|e^{1}_{\rm proj} - \theta_0|$ [deg]', fontsize = 10)
    p.ylabel('kinematic ratio', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif1,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif1[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e2
    su2 = p.subplot(1,3,2, sharey = su1)
    p.xlabel(r'$|e^{2}_{\rm proj} - \theta_0|$ [deg]', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif2,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif2[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e3
    su3 = p.subplot(1,3,3, sharey = su1)
    p.xlabel(r'$|e^{3}_{\rm proj} - \theta_0|$ [deg]', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif3,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif3[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

    # tight layout
    p.tight_layout()
    # save the figure to disk
    p.savefig(datadir + 'plots/PAdm' + halo + '.pdf',
              dpi = 120,
              bbox_inches = 'tight')
    # clean up

################################################################################
def WAnalysisTask12(obsnet, datadir, datafile, halo, sigma):
    '''
    This is similar to WAnalysis11, I plot the difference between the angle 
    of the angular momentum of the GC systems and the principal axis 
    of the DM halo in 3D

    The eigenvectors are ordered according to the value of their corresponding
    eigenvalues e1 > e2 > e3. A multiplot is produced, each eigenvector
    per plot.
    '''

    # Part 12
    print 'Starting the part with the angle between 3D angular momentum of the '
    print 'GC systems and the principal axes of the DM halo'
    print

    full_idx    = []
    full_kinrat = []
    full_dif1   = []
    full_dif2   = []
    full_dif3   = []

    # loading up the data, which is saved via cPickle
    data = cPickle.load(open(datadir+datafile, 'rb'))
    # because of the mess that pickle creates, unpack things manually..
    full_theta    = array(zip(*data)[0])
    full_phi      = array(zip(*data)[1])
    full_instance = array(zip(*data)[2])
    full_amp      = array(zip(*data)[3])
    full_axi      = array(zip(*data)[4])
    full_sig      = array(zip(*data)[5])
    full_eigval   = array(zip(*data)[6])
    full_eigvec   = array(zip(*data)[7])
    full_angmom   = array(zip(*data)[8])
    full_q1       = array(zip(*data)[9])
    full_q2       = array(zip(*data)[10])
    full_q3       = array(zip(*data)[11])
    full_q4       = array(zip(*data)[12])

    # clean up 
    del data

    # directory in which the plots are to be saved
    savedir = datadir + 'plots/perspective/'

    # check if directory exists. If it does not, create it!
    if not os.path.exists(savedir):
        os.makedirs(savedir)

    # read the principal axis of the DM halo..
    pa = load('/data/users/jovan/Aquarius/PresentDay/halo-' + halo + 
              '/principalAxis/pa'+halo+'.npy')
    # converting the axes to cartesian coords
    pax = sin(pa[0])*cos(pa[1])
    pay = sin(pa[0])*sin(pa[1])
    paz = cos(pa[0])

    pa1 = array([pax[0],pay[0],paz[0]])
    pa2 = array([pax[1],pay[1],paz[1]])
    pa3 = array([pax[2],pay[2],paz[2]])

    rad = 1000.
    for i,v in enumerate(obsnet):
        ind = (full_theta == v[0]) & (full_phi == v[1])

        amp    = full_amp[ind]
        axi    = full_axi[ind]
        sig    = full_sig[ind]
        eigval = full_eigval[ind]
        eigvec = full_eigvec[ind]
        angmom = full_angmom[ind]
        q1     = full_q1[ind]
        q2     = full_q2[ind]
        q3     = full_q3[ind]
        q4     = full_q4[ind]

        kinrat = amp/sig

        # now choosing which systems are most similar to M31
        idx = [True] *len(q1)
        idx = array(idx)

        for ll in range(len(q1)):
            ta = azym_sim_test_mean(q1[ll],q2[ll],q3[ll],q4[ll],sig = sigma)

            if ((len(ta) == 2) and
                (any([tmp < 0 for tmp in ta])) and
                (any([tmp > 0 for tmp in ta]))):
                pass
            else:
                idx[ll] = False

        # calculate the angular difference between the angular momentum 
        # of the GC systems and the principal axis of the DM halo
        angdif1 = zeros(len(angmom))
        angdif2 = zeros(len(angmom))
        angdif3 = zeros(len(angmom))
        for ll in range(len(angmom)):
            angdif1[ll] = rad2deg(arccos((dot(angmom[ll],pa1))/\
                                         (linalg.norm(angmom[ll])*
                                          linalg.norm(pa1))))
            angdif2[ll] = rad2deg(arccos((dot(angmom[ll],pa2))/\
                                         (linalg.norm(angmom[ll])*
                                          linalg.norm(pa2))))
            angdif3[ll] = rad2deg(arccos((dot(angmom[ll],pa3))/\
                                         (linalg.norm(angmom[ll])*
                                          linalg.norm(pa3))))

        # the orientation of the principal axes is arbitrary 
        angdif1[angdif1 > 90.] = 180.0 - angdif1[angdif1 > 90.]
        angdif2[angdif2 > 90.] = 180.0 - angdif2[angdif2 > 90.]
        angdif3[angdif3 > 90.] = 180.0 - angdif3[angdif3 > 90.]

        # now to the plots for each perspective
        p.figure(figsize=(6,4), dpi=100, facecolor='w', edgecolor='K')
        su1 = p.subplot(1,3,1)
        p.xlabel(r'$|e^{1} - L_{\rm GCS}|$ [deg]', fontsize = 10)
        p.ylabel('kinematic ratio', fontsize = 10)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=4, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif1,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif1[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        su2 = p.subplot(1,3,2, sharey = su1)
        p.xlabel(r'$|e^{2} - L_{\rm GCS}|$ [deg]', fontsize = 10)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif2,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif2[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        su3 = p.subplot(1,3,3, sharey = su1)
        p.xlabel(r'$|e^{3} - L_{\rm GCS}|$ [deg]', fontsize = 10)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif3,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif3[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

        # define the plot output name (pon)
        pon = 'PAdm3D-t'+str(int(v[0]))+'p'+str(int(v[1]))+'.pdf'

        p.tight_layout()

        p.savefig(savedir+pon, dpi=120, bbox_inches='tight')
        p.clf()

        # adding the to the full lists for the final product
        full_idx.append(idx)
        full_dif1.append(angdif1)
        full_dif2.append(angdif2)
        full_dif3.append(angdif3)
        full_kinrat.append(kinrat)

    # done for convenience..
    idx     = array(full_idx).flatten()
    kinrat  = array(full_kinrat).flatten()
    angdif1 = array(full_dif1).flatten()
    angdif2 = array(full_dif2).flatten()
    angdif3 = array(full_dif3).flatten()


    # The calculations are done
    # Now do the figure
    p.figure(figsize = (9,6), dpi = 100, facecolor = 'w', edgecolor = 'K')
    # for e1
    su1 = p.subplot(1,3,1)
    p.xlabel(r'$|e^{1} - L_{\rm GCS}|$ [deg]', fontsize = 10)
    p.ylabel('kinematic ratio', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif1,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif1[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e2
    su2 = p.subplot(1,3,2, sharey = su1)
    p.xlabel(r'$|e^{2} - L_{\rm GCS}|$ [deg]', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif2,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif2[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e3
    su3 = p.subplot(1,3,3, sharey = su1)
    p.xlabel(r'$|e^{3} - L_{\rm GCS}|$ [deg]', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif3,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif3[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

    # tight layout
    p.tight_layout()
    # save the figure to disk
    p.savefig(datadir + 'plots/PAdm3D' + halo + '.pdf',
              dpi = 120,
              bbox_inches = 'tight')
    p.clf()

################################################################################
def WAnalysisTask13(obsnet, datadir, datafile, halo, sigma):
    '''
    This is similar to WAnalysis11 and 12, but instead of dealing with the 
    principal axes of the DM halo, we deal with the principal axes of the 
    stellar halo.

    The eigenvectors are ordered according to the value of their corresponding
    eigenvalues e1 > e2 > e3. A multiplot is produced, each eigenvector
    per plot.
    '''

    print 'Starting the part with the angle between the rotation and the '
    print 'principal axes of the stellar halo.'
    print

    full_idx     = []
    full_kinrat  = []
    full_dif1    = []
    full_dif2    = []
    full_dif3    = []
    full_dif1_3d = []
    full_dif2_3d = []
    full_dif3_3d = []

    # before anything else, calculate the principal axis of the stellar halo 
    # of the halo in question
    tmploc = datadir + 'dataProducts/'
    tmpfil ='ScaledParticle.Data.npy'
    h_eval, h_evec, h_angmom = WM.WInertia_fullhalo(tmploc,tmpfil)

    # loading up the data, which is saved via cPickle
    data = cPickle.load(open(datadir+datafile, 'rb'))
    # because of the mess that pickle creates, unpack things manually..
    full_theta    = array(zip(*data)[0])
    full_phi      = array(zip(*data)[1])
    full_instance = array(zip(*data)[2])
    full_amp      = array(zip(*data)[3])
    full_axi      = array(zip(*data)[4])
    full_sig      = array(zip(*data)[5])
    full_eigval   = array(zip(*data)[6])
    full_eigvec   = array(zip(*data)[7])
    full_angmom   = array(zip(*data)[8])
    full_q1       = array(zip(*data)[9])
    full_q2       = array(zip(*data)[10])
    full_q3       = array(zip(*data)[11])
    full_q4       = array(zip(*data)[12])

    # directory in which the plots are to be saved
    savedir = datadir + 'plots/perspective/'

    # check if directory exists. If it does not, create it!
    if not os.path.exists(savedir):
        os.makedirs(savedir)

    rad = 1000. # distance of the observer from the centre of the galaxy
    for i,v in enumerate(obsnet):
        ind = (full_theta == v[0]) & (full_phi == v[1])

        amp    = full_amp[ind]
        axi    = full_axi[ind]
        sig    = full_sig[ind]
        eigval = full_eigval[ind]
        eigvec = full_eigvec[ind]
        angmom = full_angmom[ind]
        q1     = full_q1[ind]
        q2     = full_q2[ind]
        q3     = full_q3[ind]
        q4     = full_q4[ind]

        # now choosing which systems are most similar to M31
        idx = [True] *len(q1)
        idx = array(idx)

        for ll in range(len(q1)):
            ta = azym_sim_test_mean(q1[ll],q2[ll],q3[ll],q4[ll],sig = sigma)

            if ((len(ta) == 2) and
                (any([tmp < 0 for tmp in ta])) and
                (any([tmp > 0 for tmp in ta]))):
                pass
            else:
                idx[ll] = False

        # calculating the kinematic ratio for this perspective:
        kinrat = amp/sig

        # load the appropriate transformation matrix
        mtrans = load(datadir+'dataProducts/transMat/'+\
                      'TM'+'t'+str(int(v[0]))+'p'+str(int(v[1]))+'.npy')

        # # working with the projections of the eigenvectors..
        # for ll in range(3):
        # ordering the principal eigenvectors of the halo by length 
        order = (rankdata(h_eval, method = 'min')).astype(int)
        order = len(order) - order

        # project the on the plane of the sky
        proj_e1 = coords_transform(mtrans,
                                   h_evec[order.tolist().index(0)])
        proj_e2 = coords_transform(mtrans,
                                   h_evec[order.tolist().index(1)])
        proj_e3 = coords_transform(mtrans,
                                   h_evec[order.tolist().index(2)])

        # position angle of each principal eigenvector
        pa_e1   = rad2deg(posAngCal([proj_e1[0]], [proj_e1[1]]))
        pa_e2   = rad2deg(posAngCal([proj_e2[0]], [proj_e2[1]]))
        pa_e3   = rad2deg(posAngCal([proj_e3[0]], [proj_e3[1]]))

        # finding the difference between the rotation axis and principal axes
        angdif1 = 180.0 - abs(abs(pa_e1 - axi) - 180.0)
        angdif2 = 180.0 - abs(abs(pa_e2 - axi) - 180.0)
        angdif3 = 180.0 - abs(abs(pa_e3 - axi) - 180.0)

        # orientation of the eigenvectors does not really matter
        angdif1[angdif1 > 90.] = 180.0 - angdif1[angdif1 >90]
        angdif2[angdif2 > 90.] = 180.0 - angdif2[angdif2 >90]
        angdif3[angdif3 > 90.] = 180.0 - angdif3[angdif3 >90]

        # Now deal with the 3D stuff
        angdif1_3d = zeros(len(angmom))
        angdif2_3d = zeros(len(angmom))
        angdif3_3d = zeros(len(angmom))

        for ll in range(len(angmom)):
            angdif1_3d[ll] = rad2deg(arccos((dot(angmom[ll],h_evec[order.tolist().index(0)]))/\
                                  (linalg.norm(angmom[ll])*linalg.norm(h_evec[order.tolist().index(0)]))))
            angdif2_3d[ll] = rad2deg(arccos((dot(angmom[ll],h_evec[order.tolist().index(1)]))/\
                                  (linalg.norm(angmom[ll])*linalg.norm(h_evec[order.tolist().index(1)]))))
            angdif3_3d[ll] = rad2deg(arccos((dot(angmom[ll],h_evec[order.tolist().index(2)]))/\
                                  (linalg.norm(angmom[ll])*linalg.norm(h_evec[order.tolist().index(2)]))))

        # the orientation of the principal axes is arbitrary 
        angdif1_3d[angdif1_3d > 90.] = 180.0 - angdif1_3d[angdif1_3d > 90.]
        angdif2_3d[angdif2_3d > 90.] = 180.0 - angdif2_3d[angdif2_3d > 90.]
        angdif3_3d[angdif3_3d > 90.] = 180.0 - angdif3_3d[angdif3_3d > 90.]

        # Right, now lets the the perspective plots..

        # now to the plots for each perspective (this is for the 3D case)
        p.figure(figsize=(6,4), dpi=100, facecolor='w', edgecolor='K')
        su1 = p.subplot(1,3,1)
        p.xlabel(r'$|e^{1} - L_{\rm GCS}|$ [deg]', fontsize = 10)
        p.ylabel('kinematic ratio', fontsize = 10)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=4, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif1_3d,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif1_3d[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        su2 = p.subplot(1,3,2, sharey = su1)
        p.xlabel(r'$|e^{2} - L_{\rm GCS}|$ [deg]', fontsize = 10)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif2_3d,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif2_3d[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        su3 = p.subplot(1,3,3, sharey = su1)
        p.xlabel(r'$|e^{3} - L_{\rm GCS}|$ [deg]', fontsize = 10)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif3_3d,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif3_3d[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

        # define the plot output name (pon)
        pon = 'PAsh3D-t'+str(int(v[0]))+'p'+str(int(v[1]))+'.pdf'

        p.tight_layout()

        p.savefig(savedir+pon, dpi=120, bbox_inches='tight')
        p.clf()

        # Do the plot but just for this one perspective (this is the projected)
        p.figure(figsize=(6,4), dpi=100, facecolor='w', edgecolor='K')
        su1 = p.subplot(1,3,1)
        p.xlabel(r'$|e^{1}_{\rm proj} - \theta_0|$ [deg]', fontsize = 10)
        p.ylabel('kinematic ratio', fontsize = 10)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif1,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif1[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        su2 = p.subplot(1,3,2, sharey = su1)
        p.xlabel(r'$|e^{2}_{\rm proj} - \theta_0|$ [deg]', fontsize = 10)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif2,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif2[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
        su3 = p.subplot(1,3,3, sharey = su1)
        p.xlabel(r'$|e^{3}_{\rm proj} - \theta_0|$ [deg]', fontsize = 10)
        p.xlim([0,90])
        p.xticks([0,30,60,90])
        p.ylim([-0.05,max(kinrat)+0.1])
        minorticks(mtnum=5, ftsize=10, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
        p.scatter(angdif3,kinrat, color='black', lw=0.5, s=5)
        # p.scatter(angdif3[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
        p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

        # define the plot output name (pon)
        pon = 'PAsh-t'+str(int(v[0]))+'p'+str(int(v[1]))+'.pdf'

        p.tight_layout()

        p.savefig(savedir+pon, dpi=120, bbox_inches='tight')
        p.clf()

        # adding the to the full lists for the final product
        full_idx.append(idx)
        full_dif1.append(angdif1)
        full_dif2.append(angdif2)
        full_dif3.append(angdif3)
        full_kinrat.append(kinrat)
        full_dif1_3d.append(angdif1_3d)
        full_dif2_3d.append(angdif2_3d)
        full_dif3_3d.append(angdif3_3d)

    # done for convenience
    idx        = array(full_idx).flatten()
    kinrat     = array(full_kinrat).flatten()
    angdif1    = array(full_dif1).flatten()
    angdif2    = array(full_dif2).flatten()
    angdif3    = array(full_dif3).flatten()
    angdif1_3d = array(full_dif1_3d).flatten()
    angdif2_3d = array(full_dif2_3d).flatten()
    angdif3_3d = array(full_dif3_3d).flatten()

    # Now do the final figure
    # (projected case)
    p.figure(figsize = (9,6), dpi = 100, facecolor = 'w', edgecolor = 'K')
    # for e1
    su1 = p.subplot(1,3,1)
    p.xlabel(r'$|e^{1}_{\rm proj} - \theta_0|$ [deg]', fontsize = 10)
    p.ylabel('kinematic ratio', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif1,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif1[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e2
    su2 = p.subplot(1,3,2, sharey = su1)
    p.xlabel(r'$|e^{2}_{\rm proj} - \theta_0|$ [deg]', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif2,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif2[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e3
    su3 = p.subplot(1,3,3, sharey = su1)
    p.xlabel(r'$|e^{3}_{\rm proj} - \theta_0|$ [deg]', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif3,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif3[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

    # tight layout
    p.tight_layout()
    # save the figure to disk
    p.savefig(datadir + 'plots/PAsh' + halo + '.pdf',
              dpi = 120,
              bbox_inches = 'tight')
    p.clf()

    # This is for the 3D case
    p.figure(figsize = (9,6), dpi = 100, facecolor = 'w', edgecolor = 'K')
    # for e1
    su1 = p.subplot(1,3,1)
    p.xlabel(r'$|e^{1} - L_{\rm GCS}|$ [deg]', fontsize = 10)
    p.ylabel('kinematic ratio', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif1_3d,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif1_3d[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e2
    su2 = p.subplot(1,3,2, sharey = su1)
    p.xlabel(r'$|e^{2} - L_{\rm GCS}|$ [deg]', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif2_3d,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif2_3d[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')
    # for e3
    su3 = p.subplot(1,3,3, sharey = su1)
    p.xlabel(r'$|e^{3} - L_{\rm GCS}|$ [deg]', fontsize = 10)
    p.xlim([0,90])
    p.ylim([-0.05,max(kinrat)+0.1])
    minorticks(mtnum=5, ftsize=8, maw=1.0,mal=3.5,miw=0.5,mil=2.5)
    p.scatter(angdif3_3d,kinrat, color='black', lw=0.5, s=5)
    # p.scatter(angdif3_3d[idx],kinrat[idx], color='red', lw=0.5, s=5.05)
    p.plot([0.0,90.0],[0.93,0.93], ls=':', lw=0.9, color='black')

    # tight layout
    p.tight_layout()
    # save the figure to disk
    p.savefig(datadir + 'plots/PAsh3D' + halo + '.pdf',
              dpi = 120,
              bbox_inches = 'tight')
    p.clf()

################################################################################
def gridSpecEvo(ii,jj, ni=4,nj=4):
    '''
    Moves along the gridspec position.
    It requires the position of the current subfigure ii,jj, 
    and the size of the total grid ni, nj. It returns the 
    location of the next subplot. 
    
    This is written for convenience.
    '''
    if jj == nj-1:
        jj  = 0
        ii += 1
    else:
        jj += 1
    return ii, jj
    