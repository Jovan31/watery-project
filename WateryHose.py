from WateryModules import *
import HoseMods as HM

'''
WateryHose.

This is essentially the WateryFlow code, but the kinematics of a mock GC 
system is not calculated with the Bayesian routine, but with a simple fit, 
akin to what I did for the M31 letter.

This primarily meant to be a check, alternative, for speed.
Also, testing the placement of the rotation axis/amplitude parameters.

The output is the following (stored in cPickle file):
v[0]   = the theta coordinate of the observer
v[1]   = the phi coordinate of the observer
k      = the kth iteration from the given perspective, by default n/100
amp    = the amplitude for the kth run for a given perspective
axi    = the axis for the kth run for a given perspective
dis    = the dispersion for the kth run for a given perspective
eigval = the eigenvalues of the Inertia Tensor for a system of GCs
eigvec = the eigenvectors of the Inertia Tensor for a system of GCs
angmom = the angular momentum in 3D for a system of GCs
quad1  = the number of clusters generated in quadrant 1
quad2  = the number of clusters generated in quadrant 2
quad3  = the number of clusters generated in quadrant 3
quad4  = the number of clusters generated in quadrant 4
'''

# This is just for measuring the time it takes to execute the code
datum = datetime.datetime.now()

# choosing a halo
halo = 'A'
print 'Aquarius halo chosen: ', halo

# location of the data
database     = '/data/users/jovan/Aquarius/PresentDay/halo-' + halo + '/'
dataProducts = database + 'dataProducts/hose/'

# this is where everything is kept; at the end it will be turned to an numpy
# array and saved to disk as a numpy binary file
results = []

# Selecting the particles which might contain GCs
WParticleSelector(database + 'full-data-binary.npy',
                  dataProducts + 'Particle.Data.npy',backfeed = False)

# Centring the particles, so that the origin is at the most dense point
WGalaxyCentring(dataProducts + 'Particle.Data.npy')

# Defining the network of observers (degrees)
obsnet = WPerspective(principalAxis=True, datadir=database, halo=halo)
rad    = 1000.

# Start of the main phase.. loop through each observer
for i,v in enumerate([obsnet[0]]):
  x_obs = int(round(rad*sin(deg2rad(v[0]))*cos(deg2rad(v[1]))))
  y_obs = int(round(rad*sin(deg2rad(v[0]))*sin(deg2rad(v[1]))))
  z_obs = int(round(rad*cos(deg2rad(v[0]))))

  # Observing the particles, given a specific location of the observer
  observer = array([x_obs, y_obs, z_obs])

  # The transformation matrix, i.e. how the observer sees the system
  mtrans = transformation_matrix(observer)
  # save the transformation matrix, so the same can be used for any further
  # transformations from this perspective
  # tmn = transformation matrix name
  tmn = dataProducts + 'transMat/' + \
        'TM'+'t'+str(int(v[0]))+'p'+str(int(v[1]))+'.npy'
  # save the transformation maxtrix to disk
  save(tmn,mtrans)

  # Observing the data
  WDataObservation(dataProducts + 'Particle.Data.npy',
                   mtrans, observer,
                   dataProducts + 'Particle.ObservedData.npy',
                   backfeed = False)

  # Determining the systemic velocity of the halo from this point of view
  galvel   = load(dataProducts + 'Particle.ObservedData.npy')[2]
  galRproj = load(dataProducts + 'Particle.ObservedData.npy')[4]
  vsys     = mean(galvel[galRproj < 30.0])

  # clean up
  del galvel, galRproj

  flow_control = 1 # number of repetition for this perspective

  # Observe a GCS k times for this perspective
  for k in range(flow_control):
    print
    print 'OBSERVER: ', observer, '   ITERATION :', k
    print

    # The generated GC system will be saved to disk in case futher analysis
    # is needed.
    # the son = 'system output name'
    son = dataProducts + 'systems/'+\
                         't'+str(int(v[0]))+\
                         'p'+str(int(v[1]))+\
                         'run'+str(k)+'.npy'

    # Selecting the actual GCs from the observed data
    WGlobularSelector(dataProducts + 'Particle.ObservedData.npy', son)

    # Load the GC observed data
    GCdata = load(son)

    x        = GCdata[0]
    y        = GCdata[1]
    vel      = GCdata[2]
    posAng   = GCdata[4]
    dm_id    = GCdata[5]


    # clean up
    del GCdata

    # the kinematic analysis
    amp, eamp, axi, eaxi, sig, esig = HM.SimpleKinematics(vel,posAng,vsys)

    # Plotting the results of this instance
    # the 'plot output name'
    pon = database + 'plots/hose_instances/'+\
                     't'+str(int(v[0]))+\
                     'p'+str(int(v[1]))+\
                     'run'+str(k)+'.pdf'


    # To avoid the matplotlib memory issue, create a fork of the progarm
    # to execute separately
    pid = os.fork()
    if pid == 0:
      print 'Forking...'
      # Create the diagnostic plot
      HM.SimpleHaloPlot(x, y,
                        vel, posAng,
                        amp, axi, 
                        sig, vsys,
                        halo, mtrans,
                        oname = pon)
      os._exit(0)
    else:
      os.waitpid(pid, 0)



    # Calculate the Inertia tensor eigenvalues and eigenvectors
    # Also calulate the angular momentum (magnitude and spin)
    eigval, eigvec, angmom = WInertia(dm_id,
                                      dataProducts + 'Particle.Data.npy')

    # Checking the distribution of position angles of the GCs,
    # in other works the quadrants test
    quad1, quad2, quad3, quad4 = quad_clust_counter(posAng)

    # Saving the relevant results for this observation of this
    # perspective. The output should get updated to include more
    # stuff too...
    results.append([v[0],      # theta (polar angle)
                   v[1],       # phi (azimuthal angle)
                   k,          # instance
                   amp,        # final amplitude
                   axi,        # final rotation axis
                   sig,        # final velocity dispersion
                   eigval,     # array of InerTensr eigenvalues
                   eigvec,     # Inertia Tensor eigenvectors
                   angmom,     # angular momentum vector
                   quad1,      # number of clusters in quadrant 1
                   quad2,      # number of clusters in quadrant 2
                   quad3,      # number of clusters in quadrant 3
                   quad4])     # number of clusters in quadrant 4

    # clean up
    del quad1, quad2, quad3, quad4, amp, axi, sig, eamp, esig, eaxi
    del x, y, posAng, vel, eigvec, eigval, angmom

  print
  print 'Analysis of observation ', observer, ' is complete.'
  print


# saving the list to disk with cPickle
cPickle.dump(results, open(database + 'WateryHoseRes.cPickle', 'wb'))

# # Analyse the results, produce plots and latex friendly tables
# WAnalysis(obsnet = obsnet,
#           datadir = database,
#           datafile = 'WateryHoseRes.cPickle',
#           halo = halo)

datum1 = datetime.datetime.now()
print
print 'Watery successfully completed in: ', datum1 - datum
print 'The rent is too DAMN high!!!'
print



