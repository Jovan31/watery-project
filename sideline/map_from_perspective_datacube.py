import sys
import astropy.io.fits as pf
# import vaex as vx

# the dark corner of python
sys.path.append('/data/users/jovan/Aquarius/PresentDay/Python-JV/')
from WateryModules import *

'''
This creates a fits file containing all the perspectives and line-of-sight
velocities for an Aquarius halo. Made for the purpose of me testing stuff, 
and finding out how the rotation is viewed from different angles.
'''

# This is just for measuring the time it takes to execute the code
datum0 = datetime.datetime.now()

# choosing a halo
halo = 'C'
print 'Aquarius halo chosen: ', halo

# location of the data
database = '/data/users/jovan/Aquarius/PresentDay/halo-' + halo + '/'
dataProducts = database + 'dataProducts/'

# Selecting the particles which might contain GCs
WParticleSelector(database + 'full-data-binary.npy',
                  dataProducts + 'Particle.Data.npy',backfeed = False)

# Centring the particles, so that the origin is at the most dense point
WGalaxyCentring(dataProducts + 'Particle.Data.npy')

# Scaling the galaxy to Aq-A
WGalaxyScaling(dataProducts + 'Particle.Data.npy',
               dataProducts + 'ScaledParticle.Data.npy',
               halo)

# Defining the network of observers (degrees)
obsnet = WPerspective(principalAxis=True, datadir=database, halo=halo)
rad    = 1000.

# list of columns for the fits file
col_list = []

# Start of the main phase.. loop through each observer
for i,j in enumerate(obsnet):
  print 'Obsering from ', j[0], j[1]
  x_obs = int(round(rad*sin(deg2rad(j[0]))*cos(deg2rad(j[1]))))
  y_obs = int(round(rad*sin(deg2rad(j[0]))*sin(deg2rad(j[1]))))
  z_obs = int(round(rad*cos(deg2rad(j[0]))))

  # Observing the particles, given a specific location of the observer
  observer = array([x_obs, y_obs, z_obs])

  # load the appropriate transformation matrix
  mtrans = load(dataProducts+'transMat/'+\
                'TM'+'t'+str(int(j[0]))+'p'+str(int(j[1]))+'.npy')

  # Observing the data
  WDataObservation(dataProducts + 'ScaledParticle.Data.npy',
                   mtrans, observer,
                   dataProducts + 'Particle.ObservedData.npy',
                   backfeed = False)

  # loading the relevant parmeters
  x = load(dataProducts + 'Particle.ObservedData.npy')[0]
  y = load(dataProducts + 'Particle.ObservedData.npy')[1]
  v = load(dataProducts + 'Particle.ObservedData.npy')[2]
  s = load(dataProducts + 'Particle.ObservedData.npy')[6]

  # converting them to fits columns
  col_x = pf.Column(name='x_t'+str(int(j[0]))+'p'+str(int(j[1])), format='E', array=x)
  col_y = pf.Column(name='y_t'+str(int(j[0]))+'p'+str(int(j[1])), format='E', array=y)
  col_v = pf.Column(name='v_t'+str(int(j[0]))+'p'+str(int(j[1])), format='E', array=v)
  col_s = pf.Column(name='s_t'+str(int(j[0]))+'p'+str(int(j[1])), format='E', array=s)

  # adding the colums to a list 
  col_list.append(col_x)
  col_list.append(col_y)
  col_list.append(col_v)
  col_list.append(col_s)

# once the halo has been observed from every perspective..
cols = pf.ColDefs(col_list)
# aaand create a table from the columns
tbhdu = pf.BinTableHDU.from_columns(cols)

# save this baby to disk
tbhdu.writeto(dataProducts + 'ObsHalo'+halo+'_datacube.fits')

datum1 = datetime.datetime.now()
print
print 'Process completed in: ', datum1 - datum0
print 'The rent is too DAMN high!'
print

