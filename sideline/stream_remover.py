import sys

# the dark corner of python...
sys.path.append('/data/users/jovan/Aquarius/PresentDay/Python-JV')
from WateryModules import *

'''
This script takes the main data file for an Aquarius halo and removes selected
streams. Done for testing of halo C, if removing one of the dominant stellar 
streams would improve the results.
'''

# location of the data
database        = '/data/users/jovan/Aquarius/PresentDay/halo-Cc/'
output_database = database+'full-data-binary-Cc.npy'

# read in the full database
fulldata = load(database + 'full-data-binary.npy')

# make a index where the desired stream is not selected
ind = fulldata[7] == 31000000.

# reading in the particular subset of the data
x = fulldata[0][ind]
y = fulldata[1][ind]
z = fulldata[2][ind]
vel_x = fulldata[3][ind]
vel_y = fulldata[4][ind]
vel_z = fulldata[5][ind]
dm_id = fulldata[6][ind]
str_id = fulldata[7][ind]
str_mass = fulldata[8][ind]

# clean up
del fulldata

# saving the selected particles to disk, as a numpy array
save(output_database, array([x,
                             y,
                             z,
                             vel_x,
                             vel_y,
                             vel_z,
                             dm_id,
                             str_id,
                             str_mass]))

print
print 'Modifying the master database for halo C completed.'
