import sys

# the dark corner of python
sys.path.append('/data/users/jovan/Aquarius/PresentDay/Python-JV/')
from WateryModules import *


'''
Watery Flow, debugger version.

Originally created to test the WGlobularSelector, to know that the code will 
not crash during that phase as it is the most sensitive. So all the analysis 
is commented out, as it is not needed at the moment.

'''

# This is just for measuring the time it takes to execute the code
datum = datetime.datetime.now()

# choosing a halo
halo = 'A'
print 'Aquarius halo chosen: ', halo

# location of the data
database = '/data/users/jovan/Aquarius/PresentDay/halo-' + halo + '/'
dataProducts = database + 'dataProducts/'

# Selecting the particles which might contain GCs
WParticleSelector(database + 'full-data-binary.npy',
                  dataProducts + 'Particle.Data.npy',backfeed = False)

# Centring the particles, so that the origin is at the most dense point
WGalaxyCentring(dataProducts + 'Particle.Data.npy')

# Scaling the galaxy to Aq-A
WGalaxyScaling(dataProducts + 'Particle.Data.npy',
               dataProducts + 'ScaledParticle.Data.npy',
               halo)

# Defining the 'network' of observers..
obsnet = WPerspective(principalAxis=True, datadir=database, halo=halo)
rad   = 1000.
print 
# average number of GC generated per perspective:
avg_gen_count = []
for i,v in enumerate(obsnet):
      x_obs = int(rad*sin(deg2rad(v[0]))*cos(deg2rad(v[1])))
      y_obs = int(rad*sin(deg2rad(v[0]))*sin(deg2rad(v[1])))
      z_obs = int(rad*cos(deg2rad(v[0])))

      # Observing the particles, given a specific location of the observer
      observer = array([x_obs, y_obs, z_obs])

      # The transformation matrix, i.e. how the observer sees the system
      mtrans = transformation_matrix(observer)

      # Observing the data
      WDataObservation(dataProducts + 'ScaledParticle.Data.npy',
                       mtrans, observer,
                       dataProducts + 'Particle.ObservedData.npy',
                       backfeed = False)

      # Here is where WateryFlow forks from Watery..
      # Clusters will be drawn many times and the analysis will be
      # repeated each sample again and again

      flow_control = 100 # number of repetition for this perspective
      # number of GC generated in each mock
      gen_count = zeros(flow_control)
      for k in range(flow_control):
          print
          print 'OBSERVER: ', observer, '   ITERATION :', k
          print

          # The generated GC system will be saved to disk in case futher analysis
          # is needed.
          # the son = 'system output name'
          son = dataProducts + 'systems/'+\
                               't'+str(int(v[0]))+\
                               'p'+str(int(v[1]))+\
                               'run'+str(k)+'.npy'

          # Selecting the actual GCs from the observed data
          WGlobularSelector(dataProducts + 'Particle.ObservedData.npy', son)

          # Load the GC observed data
          x = load(son)[0]
          print 'Number of generated GCs in this mock system: ', len(x)
          gen_count[k] = len(x)


      print
      print 'Analysis of observation ', observer, ' is complete.'
      avg_gen_count.append(mean(gen_count))
      print


print mean(avg_gen_count)
datum1 = datetime.datetime.now()
print
print 'WateryFlowDebugger successfully completed in: ', datum1 - datum
print 'The rent is too DAMN high!!!'
print
