import sys
import scipy as s
from readcol import readcol as rcol

'''
This little code is made to practice my overall programing skills. 

It accepts an input file containing the 3 principle axis of a Aquarius halo 
(in Cartesian coordinates, in the Aquarius universe reference frame), and 
outputs a file with the same principle axis, but in spherical polar coordinates.
'''

# Check if there are enough arguments when the code is run
if len(sys.argv) != 3:
    print 'ERROR: 3 arguments are needed. You entered ', len(sys.argv)
    sys.exit()

# define which argument is what in the code
inputfile  = str(sys.argv[1])
outputfile = str(sys.argv[2])

# read in the input file
x, y, z = rcol(inputfile, twod = False)

# calculating the position of the principal axis in spherical polar coordinates
# everyting is in radians
theta = s.arccos(z  / (x**2.0 + y**2.0 + z**2.0)**0.5)
phi = s.arctan(y/x)

# this is the combined result, for easier storage into the numpy binary
# 0th dimension is theta (result[0], 1st dimension is phi (result[1])
result = s.array([theta,phi])

# saving the result to disk
s.save(outputfile, result)

# That's it, I hope

print
print 'The rent is too DAMN high!'
print


