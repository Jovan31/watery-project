import sys
import astropy.wcs as pywcs
import astropy.io.fits as pf
from numpy import histogram2d as histogram2d
from scipy.ndimage.filters import gaussian_filter
from scipy.stats import binned_statistic_2d as bin_st_2d

# the dark corner of python
sys.path.append('/data/users/jovan/Aquarius/PresentDay/Python-JV')
from WateryModules import *

'''
In this script I create Fig 1 for the Watery paper. 
Two panels, on one panel (left) the M31 map from PAndAS is to be shown, 
while on the right panel the Aq-A halo is displayed. They should have 
similar scale for easy comparison. 
'''

# The master figure frame
fig = p.figure(figsize=(9,6), dpi = 120, facecolor = 'w', edgecolor = 'k')
# the subplots locations and grid
gs = p.GridSpec(1,2, height_ratios=[1.7,1])

# the M31 bit

# M31 data
ra_m31 = 0.18648115831219983    # in radians
dec_m31 = 0.72028283794580406   # in radians
ra_m31_deg = rad2deg(ra_m31)
dec_m31_deg = rad2deg(dec_m31)
gal_vel_m31 = -109.260841724    # km/s

p.subplot(gs[0])
# p.subplot(1,2,1)

# the location of the M31 map
database = '/data/users/jovan/disk1/jv/M31 Custers Ploting Project/SpecPlot/'

##reading the raw map
themap = pf.open('/data/users/jovan/disk1/jv/codenameMakedon/maps/lot_all_2011_subs_fix.fits')
map = themap[0].data
#Smoothing the background map
smap = gaussian_filter(map,2.2,0)
p.imshow(smap,vmin=1,vmax=121,origin='lower',cmap='binary')


pywcsObject = pywcs.WCS(themap[0].header)
x_pix_m31,y_pix_m31 = pywcsObject.wcs_world2pix([ra_m31_deg],[dec_m31_deg],0)


ax  = p.gca() # axis parameters modifier
fig = p.gcf() # figures parameters modifier

# rendering the x/y axis invisible
# ax.yaxis.set_visible(False)
# ax.xaxis.set_visible(False)
# This just makes the ticks an empty list, so preserves the axis..
ax.axes.get_xaxis().set_ticks([])
ax.axes.get_yaxis().set_ticks([])
# labels
p.xlabel(r'$\xi$',  fontsize = 10)
p.ylabel(r'$\eta$', fontsize = 10)

# setting the axis limits;
# in this case this selects which portion of the map will be displayed
p.ylim([130,1465]) # set limits on y axis
p.xlim([270,1545]) # set limits on x axis

# Adding the compass on the plot
# arrow pointing north
p.arrow(410,1310,0,90,lw=1.0,fill=True,length_includes_head=True,head_length=13,head_width= 7, color = 'black')
# arrow pointing east
p.arrow(410,1310,-90,0,lw=1.0,fill=True,length_includes_head=True,head_length=13,head_width= 7, color = 'black')
# label for North, 'N'
p.text(399,1415,'N',fontsize = 7)
# label for East, 'E'
p.text(290,1300,'E',fontsize = 7)

# adding a circle to the plot to guide the eye
rad100 = Circle((x_pix_m31,y_pix_m31), radius =433.3, facecolor = 'none',edgecolor = 'purple', lw = 1.0)
p.setp(rad100, ls='dashed')  # settin up additional parameters for the circle, the linestyle
p.gcf().gca().add_artist(rad100) # adding the circle to the plot

# label2 is the text showing scale of the plot
label2 = p.text(1240, 285, '3 deg = 41 kpc',rotation='horizontal',fontsize=5)
#line showing the scale of the plot
p.plot([1255,1427],[327,327],lw = 1.0,color = 'black')



# the Aquarius map
p.subplot(gs[1])

# # choose a halo
halo         = 'A'
database     = '/data/users/jovan/Aquarius/PresentDay/halo-' + halo + '/'
dataProducts = database + 'dataProducts/'

# Selecting the particles which might contain GCs 
WParticleSelector(database + 'full-data-binary.npy',
                  dataProducts + 'Particle.Data.npy')

# Centring the particles, so that the origin is at the most dense point
# This option can be commented out if not needed
WGalaxyCentring(dataProducts + 'Particle.Data.npy')

# Scaling the galaxy to Aq-A
WGalaxyScaling(dataProducts + 'Particle.Data.npy',
               dataProducts + 'ScaledParticle.Data.npy',
               halo)

# Observing the particles, given a specific location of the observer
observer = array([1000., 0., 0.])

# The transformation matrix, i.e. how the observer perceives the system.
mtrans = transformation_matrix(observer)

# Observing the data
WDataObservation(dataProducts + 'ScaledParticle.Data.npy',
                 mtrans, observer,
                 dataProducts + 'Particle.ObservedData.npy',
                 backfeed = False)

# creating the plot
data2D = load(dataProducts + 'Particle.ObservedData.npy')
xx     = data2D[0]
yy     = data2D[1]
vv     = data2D[2]
del data2D

# limits and axis labels
p.xlim([150, -150])
p.ylim([-150, 150])
p.xlabel(r'pseudo-$\xi$',  fontsize = 10)
p.ylabel(r'pseudo-$\eta$', fontsize = 10)

Z, xedges, yedges = histogram2d(xx,yy,bins=200,range=([-150,150],[-150,150]))
Z =log10(Z.T + 1)
# Z, xedges, yedges, binnumber = bin_st_2d(xx,
#                                          yy,
#                                          values = vv,
#                                          statistic = mean,
#                                          bins = 300,
#                                          range = ([-150,150],[-150,150]))

# define the extent of the map
extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
# plotting the map
p.imshow(Z,
         interpolation = 'none',
         extent        = extent,
         origin        = 'lower',
         cmap          = 'binary',
         vmin          = 0,
         vmax          = 0.75*(Z).max())

ax  = p.gca() # axis parameters modifier
fig = p.gcf() # figures parameters modifier
# rendering the x/y axis invisible
# ax.yaxis.set_visible(False)
# ax.xaxis.set_visible(False)
ax.axes.get_xaxis().set_ticks([])
ax.axes.get_yaxis().set_ticks([])

# adding a circle to the plot to guide the eye
rad100 = Circle((0,0), radius =100, facecolor = 'none',edgecolor = 'purple', lw = 1.0)
p.setp(rad100, ls='dashed')  # settin up additional parameters for the circle, the linestyle
p.gcf().gca().add_artist(rad100) # adding the circle to the plot

p.tight_layout()

# Saving the plot to disk
p.savefig('/data/users/jovan/papers/Watery/figures/M31vsAq-A.pdf',dpi=300,
          bbox_inches = 'tight')


print 'The rent is too DAMN high!'
print