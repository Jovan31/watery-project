import pylab as p
from scipy import *
from minorticks import minorticks
from matplotlib.patches import Circle

'''
This simple script will just plot all the average 'stellar' profiles of the 
Aquarius haloes on one plot. The average profiles have already been calculated.
'''

# Choose a selection of haloes
haloes = ['A','B','C','D','E']

# database     = '/data/users/jovan/Aquarius/PresentDay/halo-'
# dataProducts = database + '/dataProducts/'

# define the colours
clrs = ['blue','green','red','gold','magenta','orange']

# Set up the figure
p.figure(figsize=(6,4), dpi = 120, facecolor = 'w', edgecolor = 'k')

# nicing up the axis
# custom x-tics, in case the AutoLocator messes things up
p.xticks(range(0,160,10))
# label the subplots
p.xlabel(r'average $R_{\rm proj}$', fontsize  = 12)
p.ylabel(r'log$_{10}$($N$/kpc$^2$)', fontsize  = 12)
# limits on the ais
p.xlim([25,155])
# p.ylim() for now let the  Wy axis run wild

# calculating the middle of each bin (takeover from WateryProfiler)
bins = arange(30.,150.,1)
x = zeros(len(bins)-1)
for q in range(len(bins)-1):
    x[q] = (bins[q]+bins[q+1])*0.5

# # calculate the area of each bin
# area = zeros(len(bins)-1)
# for q in range(len(bins)-1):
#     area[q] = pi*(bins[q+1]**2.0 - bins[q]**2.0)

# Now loop through the haloes and plot each in turn
for i,halo in enumerate(haloes):
    # read the profile from disks
    database = '/data/users/jovan/Aquarius/PresentDay/halo-' + halo + '/'
    dataProducts = database + 'dataProducts/'
    y = load(dataProducts+'avgProfile'+halo+'.npy')
    y = log10(y)

    # plot the profile as a line plot
    p.plot(x,
           y,
           c = clrs[i],
           lw = 1,
           label = 'Aq-'+halo)

# plot the M31 profile
m31 = log10(x**(-3.34)) +3.5
# for comparison, plotting profiles that have power law of -3p(3) and  -4p(4)
p3 = log10(x**(-3.00))  +3.5
p4 = log10(x**(-4.00))  +3.5

# plot these babies
p.plot(x, m31, c = 'black', lw = 1, ls = '-', label = 'M31')
p.plot(x, p3, c = 'black', lw = 0.5, ls = ':', label = r'$\gamma=3$')
p.plot(x, p4, c = 'black', lw = 0.5, ls = '--', label = r'$\gamma=4$', dashes=(2,2))

# legend
p.legend(loc = 3, frameon = False, prop={'size':10})

# configuring the ticks with my custom rutine
minorticks(ftsize=10, maw=1.0,mal=2.3,miw=0.5,mil=1.7)

# save the plot to disk
outdir = '/data/users/jovan/Aquarius/PresentDay/halo-A/'
p.savefig(outdir + 'plots/masterProfiles.pdf', 
          dpi = 120,
          bbox_inches = 'tight')

# That should do it..
print
print 'The rent is too DAMN high!'
print