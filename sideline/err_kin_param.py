import sys

# the dark corner of python...
sys.path.append('/data/users/jovan/Aquarius/PresentDay/Python-JV')
from WateryModules import *


'''
This is just a simple script that calculates the average uncertainty of the 
kinematic free parameters in each of the haloes

'''
# The haloes to be considered
haloes = ['A','B','C','D','E']

# location of the data
database     = '/data/users/jovan/Aquarius/PresentDay/halo-'

totalErrAmp = zeros(len(haloes))
totalErrAxi = zeros(len(haloes))
totalErrSig = zeros(len(haloes))

for i,v in enumerate(haloes):
  data = cPickle.load(open(database+v+'/WateryFlowRes.cPickle','rb'))
  
  # read the uncertainties in the parameters
  eeAmp = array(zip(*data)[15])
  eeAxi = array(zip(*data)[16])
  eeSig = array(zip(*data)[17])

  # print the average uncertainties per halo
  print 'Mean uncertainties for halo:', v
  print 'Amplitude >>>', '%-2.3f' % (mean(eeAmp))
  print 'Rota axis >>>', '%-2.3f' % (mean(eeAxi))
  print 'Velo disp >>>', '%-2.3f' % (mean(eeSig))

  # add the mean values to the total results
  totalErrAmp[i] = mean(eeAmp)
  totalErrAxi[i] = mean(eeAxi)
  totalErrSig[i] = mean(eeSig)

# print the averages of all haloes
print 'Mean uncertainties amongst all haloes:'
print 'Amplitude >>>', '%-2.3f' % (mean(totalErrAmp))
print 'Rota axis >>>', '%-2.3f' % (mean(totalErrAxi))
print 'Velo disp >>>', '%-2.3f' % (mean(totalErrSig))
print

# finish 
print 'The rent is too DAMN high!'
print