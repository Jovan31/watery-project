from scipy import *
from readcol import readcol

'''
This script converts the text output of those idl routines that Amina gave me 
to a numpy binary table for easier handling.

Simple code, no fuss.

Version: 0.9
'''

# the location of the data
database = '/data/users/jovan/Aquarius/PresentDay/halo-E/idl-output-text/'

# reading in the data
print 'Reading in the data...'
pos        = readcol(database + 'output_position', twod   = False)
vel        = readcol(database + 'output_velocity', twod   = False)
dmid       = readcol(database + 'output_dmid', twod       = False)
progenitor = readcol(database + 'output_progenitor', twod = False)

print
print 'Creating the master array...'
ma = zeros((9,len(pos[0])))

ma[0,:] = pos[0]
ma[1,:] = pos[1]
ma[2,:] = pos[2]
ma[3,:] = vel[0]
ma[4,:] = vel[1]
ma[5,:] = vel[2]
ma[6,:] = dmid[0]
ma[7,:] = progenitor[0]
ma[8,:] = progenitor[1]


print
print 'Saving the master array to disk as a numpy binary file...'
save(database + 'full-data-binary.npy', ma)

print
print 'The rent is too DAMN high!'
print

