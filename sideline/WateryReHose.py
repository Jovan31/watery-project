import sys

# the dark corner of python
sys.path.append('/data/users/jovan/Aquarius/PresentDay/Python-JV/')
from WateryModules import *
import HoseMods as HM

'''
WateryReHose

This is similar to WateryHose, but that code reads in the already created 
mock GC systems, and does the analysis on those
'''

# This is just for measuring the time it takes to execute the code
datum = datetime.datetime.now()

# choosing a halo
halo = 'A'
print 'Aquarius halo chosen: ', halo

# location of the data
database     = '/data/users/jovan/Aquarius/PresentDay/halo-' + halo + '/'
dataProducts = database + 'dataProducts/hose/'

# Selecting the particles which might contain GCs
WParticleSelector(database + 'full-data-binary.npy',
                  dataProducts + 'Particle.Data.npy',backfeed = False)

# Centring the particles, so that the origin is at the most dense point
WGalaxyCentring(dataProducts + 'Particle.Data.npy')

# this is where everything is kept; at the end it will be turned to an numpy
# array and saved to disk as a numpy binary file
results = []

# Defining the network of observers (degrees)
obsnet = WPerspective(principalAxis=True, datadir=database, halo=halo)
rad    = 1000.

for i,v in enumerate(obsnet):
  x_obs = int(round(rad*sin(deg2rad(v[0]))*cos(deg2rad(v[1]))))
  y_obs = int(round(rad*sin(deg2rad(v[0]))*sin(deg2rad(v[1]))))
  z_obs = int(round(rad*cos(deg2rad(v[0]))))

  # Observing the particles, given a specific location of the observer
  observer = array([x_obs, y_obs, z_obs])

  # The transformation matrix, i.e. how the observer sees the system
  mtrans = load(database+'dataProducts/transMat/'+\
                'TM'+'t'+str(int(v[0]))+'p'+str(int(v[1]))+'.npy')
  

  # Determining the systemic velocity of the halo from this point of view
  galvel   = load(dataProducts + 'Particle.ObservedData.npy')[2]
  galRproj = load(dataProducts + 'Particle.ObservedData.npy')[4]
  vsys     = mean(galvel[galRproj < 30.0])

  # clean up
  del galvel, galRproj

  flow_control = 100 # number of repetitions for this perspective

  for k in range(flow_control):
    print
    print 'OBSERVER: ', observer, '   ITERATION :', k
    print

    # The names of the GC systems to the loaded
    # the son = 'system output name'
    son = dataProducts + 'systems/'+\
                         't'+str(int(v[0]))+\
                         'p'+str(int(v[1]))+\
                         'run'+str(k)+'.npy'

    # Load the GC observed data
    GCdata = load(son)

    x        = GCdata[0]
    y        = GCdata[1]
    vel      = GCdata[2]
    posAng   = GCdata[4]
    dm_id    = GCdata[5]

    # clean up
    del GCdata

    # the kinematic analysis
    amp, eamp, axi, eaxi, sig, esig = HM.SimpleKinematics(vel,posAng,vsys)

    # Plotting the results of this instance
    # the 'plot output name'
    pon = database + 'plots/hose_instances/'+\
                     't'+str(int(v[0]))+\
                     'p'+str(int(v[1]))+\
                     'run'+str(k)+'.pdf'


    # To avoid the matplotlib memory issue, create a fork of the progarm
    # to execute separately
    pid = os.fork()
    if pid == 0:
      print 'Forking...'
      # Create the diagnostic plot
      HM.SimpleHaloPlot(x, y,
                        vel, posAng,
                        amp, axi, 
                        sig, vsys,
                        halo, mtrans,
                        oname = pon)
      os._exit(0)
    else:
      os.waitpid(pid, 0)


    # Calculate the Inertia tensor eigenvalues and eigenvectors
    # Also calulate the angular momentum (magnitude and spin)
    eigval, eigvec, angmom = WInertia(dm_id,
                                      dataProducts + 'Particle.Data.npy')

    # Checking the distribution of position angles of the GCs,
    # in other works the quadrants test
    quad1, quad2, quad3, quad4 = quad_clust_counter(posAng)

    # Saving the relevant results for this observation of this
    # perspective. The output should get updated to include more
    # stuff too...
    results.append([v[0],      # theta (polar angle)
                   v[1],       # phi (azimuthal angle)
                   k,          # instance
                   amp,        # final amplitude
                   axi,        # final rotation axis
                   sig,        # final velocity dispersion
                   eigval,     # array of InerTensr eigenvalues
                   eigvec,     # Inertia Tensor eigenvectors
                   angmom,     # angular momentum vector
                   quad1,      # number of clusters in quadrant 1
                   quad2,      # number of clusters in quadrant 2
                   quad3,      # number of clusters in quadrant 3
                   quad4])     # number of clusters in quadrant 4


    # clean up
    del quad1, quad2, quad3, quad4, amp, axi, sig, eamp, esig, eaxi
    del x, y, posAng, vel, eigvec, eigval, angmom

  print
  print 'Analysis of observation ', observer, ' is complete.'
  print

# saving the list to disk with cPickle
cPickle.dump(results, open(database + 'WateryHoseRes.cPickle', 'wb'))

# Analyse the results, produce plots and latex friendly tables
WAnalysis(obsnet = obsnet,
          datadir = database,
          datafile = 'WateryHoseRes.cPickle',
          halo = halo)

datum1 = datetime.datetime.now()
print
print 'Watery successfully completed in: ', datum1 - datum
print 'The rent is too DAMN high!!!'
print