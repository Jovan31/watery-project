import sys
import astropy.io.fits as pf
# import vaex as vx

# the dark corner of python
sys.path.append('/data/users/jovan/Aquarius/PresentDay/Python-JV/')
from WateryModules import *

'''
This creates map of a an Aquarius halo from a certain perspective, 
weighted by the radial velocities of each 'point'.

Also useful to teach interacting with the vaex API.
'''

# choosing a halo
halo = 'A'
print 'Aquarius halo chosen: ', halo

# location of the data
database = '/data/users/jovan/Aquarius/PresentDay/halo-' + halo + '/'
dataProducts = database + 'dataProducts/'

# Selecting the particles which might contain GCs
WParticleSelector(database + 'full-data-binary.npy',
                  dataProducts + 'Particle.Data.npy',backfeed = False)

# Centring the particles, so that the origin is at the most dense point
WGalaxyCentring(dataProducts + 'Particle.Data.npy')

# Scaling the galaxy to Aq-A
WGalaxyScaling(dataProducts + 'Particle.Data.npy',
               dataProducts + 'ScaledParticle.Data.npy',
               halo)

# manually enter the perspective of observation (theta, phi)
obs      = array([0.0,0.0])
# the observer in the cartesian coordinates
rad      = 1000.
x_obs    = int(rad*sin(deg2rad(obs[0]))*cos(deg2rad(obs[1])))
y_obs    = int(rad*sin(deg2rad(obs[0]))*sin(deg2rad(obs[1])))
z_obs    = int(rad*cos(deg2rad(obs[0])))
observer = array([x_obs, y_obs, z_obs])

# load the appropriate transformation matrix
mtrans = load(dataProducts+'transMat/'+\
              'TM'+'t'+str(int(obs[0]))+'p'+str(int(obs[1]))+'.npy')

# Observing the data
WDataObservation(dataProducts + 'ScaledParticle.Data.npy',
                 mtrans, observer,
                 dataProducts + 'Particle.ObservedData.npy',
                 backfeed = False)

# loading the relevant parmeters
x = load(dataProducts + 'Particle.ObservedData.npy')[0]
y = load(dataProducts + 'Particle.ObservedData.npy')[1]
v = load(dataProducts + 'Particle.ObservedData.npy')[2]

# converting them to fits columns
col_x = pf.Column(name='x', format='E', array=x)
col_y = pf.Column(name='y', format='E', array=y)
col_v = pf.Column(name='v', format='E', array=v)

# making the table
cols  = pf.ColDefs([col_x,col_y,col_v])
tbhdu = pf.BinTableHDU.from_columns(cols)

# save this baby to disk
tbhdu.writeto(dataProducts + 'Particle.ObservedData.fits')

print 
print 'The rent is too DAMN high!'
print