import sys
import datetime

# the dark corner of python...
sys.path.append('/data/users/jovan/Aquarius/PresentDay/Python-JV')

from WFunctions import *
from WateryModules import *

'''
This is an instance of the Watery code, simply written for small testing 
purposes. Initially, this is written to determine the average number of 
GCs generated in each of the haloes
'''

# the resulting list, storing how many clusters were drawn with each iteration
res = []

# This is just for measuring the time it takes to execute this code
datum = datetime.datetime.now()

haloes = ['A', 'B', 'C', 'D', 'E']

for halo in haloes:
    print 'Processing halo' , halo
    # location of the data
    database = '/data/users/jovan/Aquarius/PresentDay/halo-' + halo + '/'

    # Selecting the particles which might contain GCs 
    WParticleSelector(database + 'full-data-binary.npy', 
                      database + 'Particle.Data.npy',backfeed = False)

    # Centring the particles, so that the origin is at the most dense point
    # This option can be commented out if not needed
    WGalaxyCentring(database + 'Particle.Data.npy')

    # Observing the particles, given a specific location of the observer
    observer = array([1000., 0., 0.])

    # The transformation matrix, i.e. how the observer perceives the system.
    mtrans = transformation_matrix(observer)

    # Observing the data
    WDataObservation(database + 'Particle.Data.npy',
                     mtrans, observer,
                     database + 'Particle.ObservedData.npy',  
                     backfeed = False)

    for k in range(100):
        WGlobularSelector(database + 'Particle.ObservedData.npy',
                          database + 'GC.ObservedData.npy')

        GCdata = load(database + 'GC.ObservedData.npy')
        temp   = len(GCdata[0])
        res.append(temp)

        # clean up
        del GCdata, temp

# now let's find out what the statistics are..
res = array(res)
print mean(res), std(res)

print 
print 'The rent is too DAMN high!'
print