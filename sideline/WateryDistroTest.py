import datetime
from WateryModules import *
from BayesianKinematics2D import BayKin

'''
The Watery code.
Separate code for debugging any of the packages. 

Initially written to figure out bugs or missing stuff from WGlobularSelector
'''


# This is just for measuring the time it takes to execute this code
datum = datetime.datetime.now()

# location of the data
database = '/data/users/jovan/Aquarius/PresentDay/halo-A/'

# Selecting the particles which might contain GCs 
WParticleSelector(database + 'full-data-binary.npy', 
                  database + 'Particle.Data.npy',backfeed = False)

# Observing the particles, given a specific location of the observer
observer = array([707., 0.,707.])

# The transformation matrix, i.e. how the observer perceives the system.
mtrans = transformation_matrix(observer)


# Observing the data
WDataObservation(database + 'Particle.Data.npy',
                 mtrans, observer,
                 database + 'Particle.ObservedData.npy',  
                 backfeed = False)

# Here is where the 'flow' bit starts. The GC will be draw repeatedy so I 
# figure out what is happening

flow_control = 1000 # number of repetitions that the GC system will be created

for k in range(flow_control):
  print 
  print 'OBSERVER: ', observer, '   ITERATION :', k
  print


  # Selecting the actual GCs from the observed data
  WGlobularSelector(database + 'Particle.ObservedData.npy',
                    database + 'GC.ObservedData.npy')

  # # Load the GC observed data
  # GCdata = load(database + 'GC.ObservedData.npy')

# x        = GCdata[0]
# y        = GCdata[1]
# vel      = GCdata[2]
# # Rproj    = GCdata[3]
# posAng   = GCdata[4]
# # dm_id    = GCdata[5]
# # str_id   = GCdata[6]
# # str_mass = GCdata[7]

# # clean up
# del GCdata

# # adding "observational" errors
# err_vel = ones(len(vel)) * 15.0

# # Kinematic analysis of the GC sample

# # the parameter space to be explored
# # Amplitude space
# rangeAmp = arange(0.0, 150.0, 2.0)
# # Velocity dispersion space
# rangeSig = arange(0.0, 200.0, 2.0)
# # Rotation axis space
# rangeAxi = arange(0.0, 2.0*pi+0.10, 0.15)

# # Bayesian analysis
# postAmp, postAxi, postSig = BayKin(vel = vel, 
#                                    err_vel = err_vel, 
#                                    posAng = posAng, 
#                                    rangeAmp = rangeAmp, 
#                                    rangeAxi = rangeAxi, 
#                                    rangeSig = rangeSig)

# # Results of the kinematic analysis
# WResults(postAmp, rangeAmp, postAxi, rangeAxi, postSig, rangeSig)

# # Plotting the results
# # path to the full observed data
# full2Ddata = database + 'Particle.ObservedData.npy' 
# WHaloPlot(x, y, vel, err_vel, 
#           posAng, postAmp, rangeAmp, postAxi, rangeAxi, postSig, rangeSig, 
#           full2Ddata)



datum1 = datetime.datetime.now()
print 
print 'Watery successfully completed in: ', datum1 - datum
print 'The rent is too DAMN high!!!'
print