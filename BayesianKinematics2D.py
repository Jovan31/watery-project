import pp
from numpy import *
from scipy.integrate import simps

def BayKin(vel,
           err_vel,
           posAng,
           vsys,
           rangeAmp = arange(0.0, 150.0, 2.0),
           rangeAxi = arange(0.0, 2.0*pi+0.10, 0.15),
           rangeSig = arange(0.0, 200.0, 2.0),
           R0 = 1.0):
  '''
  This is a module for my entire Bayesian machinery for determining the
  kinematic properties of a GC system, in 2D. The original version was
  written for testing the kinematic properties of the M31 outer halo GC
  system.

  This is the faster, simplified version which only fits the possible rotation
  amplitude and the accompanying rotation axis, and the rotation corrected
  velocity dispersion of the system. The more complicated version also looked
  how the velocity dispersion changes as a function of projected radius.
  This is not implemented here, although it might be sometime in the future.

  The input arguments are the following:
  vel = array of velocities of the GCs;
  vel_err = array of the uncertainties of the above velocities;
  posAng = array of the position angles (in radians) of the GCs considered;
  vsys   = the systemic velocity of the halo in question;
  rangeAmp = a range of values for which the amplitude is searched;
  rangeAxi = a range of values for which the rotation axis is searched;
  rangeSig = a range of values for which the velocity dispersion is searched;
  R0 = a single number, the radius after which the halo starts.

  The code works as follows:
  First the Log of the Likelihood is calculated.
  The Evidence (aka the master normalization function), is not computed.
  Finally, the posterior probability distribution functions for each model
  parameter (amplitude, rotation axis, velocity dispersion) are returned.
  The posterior probability dispersion functions are "normalized" in a
  brute-force manner, after they are computed.

  UPDATE: CODE IS NOW PARALLEL (tested)

  The output of the code:
  posterior distribution function for the amplitude;
  posterior distribution function for the rotation axis;
  posterior distribution function for the velocity dispersion.
  '''

  print 'Starting BayesianKinematics2D...'
  print

  ## Calculating the Likelihood values
  print 'Calculating the Log-Likelihood values...'

  # Storing the Likelihood values
  LogL = []
  for amp in rangeAmp:
    # print "Model Likelihood calculation: ", '%i %s %i' % (amp,
    #                                                       ' / ',
    #                                                       max(rangeAmp))s
    for axi in rangeAxi:
      for sig0 in rangeSig:
        sig2 = err_vel**2.0 + sig0**2.0
        # rotation = vsys + amp * sin(posAng - axi)
        rotation = vsys + amp * sin(axi - posAng)
        lgLikelihood = -0.5*log(2.0*pi*sig2) - \
        (vel-rotation)**2.0/(2.0*sig2)
        lgLikelihood = sum(lgLikelihood)
        LogL.append([lgLikelihood,amp,axi,sig0])

  # Converting the LogL into an array for easier handling
  LogL = array(LogL)
  LogL = transpose(LogL)

  # If the LogL function gets too negative for computational convenience,
  # this offests it towards more positive values in order for the maths to work
  if max(LogL[0]) < -700:
    LogL[0] += -max(LogL[0]) + 50
  else:
    pass

  # Clean up
  del amp, lgLikelihood, axi, sig0, sig2
  print 'Log-Likelihood function calculation completed!'
  print

  # The Evidence
  print 'Calculating the Evidence...'

  # The Evidence is not calculated here. This might be implemented at a later
  # stage if needed.
  Evidence = 1.0

  print 'Calculation of the master Evidence is completed!'
  print 'The Evidence is: ', Evidence
  print

  ## Calculating the normalized posterior probability distribution functions
  # for each model parameter
  print 'Calculating the posterior distribution functions for the '
  print 'amplitude, rotation axis and velocity dispersion...'


  # The Amplitude pdf
  def calcpostAmp(LogL, rangeAmp, rangeAxi, rangeSig, Evidence):
    priors = (max(rangeSig)-min(rangeSig)) * (max(rangeAxi)-min(rangeAxi))

    #The Likelihood function multiplied by the priors
    y = scipy.exp(LogL[0]) / priors

    # Temporary storage when some of the axis are collapsed
    integr_axi = scipy.zeros(len(rangeSig))
    integr_sig = scipy.zeros(len(rangeAmp))

    # The integration, the magic happens here
    for Amp in rangeAmp:
      for Sig in rangeSig:
        ind = (LogL[1] == Amp) & (LogL[3] == Sig)
        integr_axi[rangeSig.tolist().index(Sig)] = scipy.integrate.simps(y[ind], dx = scipy.diff(rangeAxi)[0])
      integr_sig[rangeAmp.tolist().index(Amp)] = scipy.integrate.simps(integr_axi, dx = scipy.diff(rangeSig)[0])

    # The posterior distribution function of the Amplitude
    postAmp = integr_sig / Evidence
    postAmp = postAmp/sum(postAmp)
    return postAmp

  # Rotation axis pdf
  def calcpostAxi(LogL, rangeAmp, rangeAxi, rangeSig, Evidence):
    priors = (max(rangeSig)-min(rangeSig)) * (max(rangeAmp)-min(rangeAmp))

    #The Likelihood function multiplied by the priors
    y = scipy.exp(LogL[0]) / priors

    # Temporary storage when some of the axis are collapsed
    integr_sig = scipy.zeros(len(rangeAmp))
    integr_amp = scipy.zeros(len(rangeAxi))

    # The integration, the magic happens here
    for Axi in rangeAxi:
      for Amp in rangeAmp:
        ind = (LogL[1] == Amp) & (LogL[2] == Axi)
        integr_sig[rangeAmp.tolist().index(Amp)] = scipy.integrate.simps(y[ind], dx = scipy.diff(rangeSig)[0])
      integr_amp[rangeAxi.tolist().index(Axi)] = scipy.integrate.simps(integr_sig, dx = scipy.diff(rangeAmp)[0])

    # The Posterior distribution function for the Rotation Axis
    postAxi = integr_amp / Evidence
    postAxi = postAxi/sum(postAxi)
    return postAxi

  # Velocity dispersion pdf
  def calcpostSig(LogL, rangeAmp, rangeAxi, rangeSig, Evidence):
    priors = (max(rangeAmp)-min(rangeAmp)) * (max(rangeAxi)-min(rangeAxi))

    # The Likelihood function multiplied by the priors
    y = scipy.exp(LogL[0]) / priors

    #Temporary storage when some of the axis are collapsed
    integr_axi = scipy.zeros(len(rangeAmp))
    integr_amp = scipy.zeros(len(rangeSig))

    # The Integration: The magic happens here
    for Sig in rangeSig:
      for Amp in rangeAmp:
        ind = (LogL[1] == Amp) & (LogL[3] == Sig)
        integr_axi[rangeAmp.tolist().index(Amp)] = scipy.integrate.simps(y[ind], dx = scipy.diff(rangeAxi)[0])
      integr_amp[rangeSig.tolist().index(Sig)] = scipy.integrate.simps(integr_axi, dx = scipy.diff(rangeAmp)[0])

    # The posterior dispersion function of the velocity dispersion
    postSig = integr_amp / Evidence
    postSig = postSig/sum(postSig)
    return postSig


  # The parallelization begins here...
  ppservers = ()
  job_server = pp.Server(ppservers=ppservers)

  postResults = []
  # The magic happens here in parallel...
  postResults.append(job_server.submit(calcpostAmp, (LogL, rangeAmp, rangeAxi, rangeSig, Evidence), (), ('scipy', 'scipy.integrate')))
  postResults.append(job_server.submit(calcpostAxi, (LogL, rangeAmp, rangeAxi, rangeSig, Evidence), (), ('scipy', 'scipy.integrate')))
  postResults.append(job_server.submit(calcpostSig, (LogL, rangeAmp, rangeAxi, rangeSig, Evidence), (), ('scipy', 'scipy.integrate')))

  for jobs in postResults:
    jobs()

  # reading in the final results before returning
  postAmp = postResults[0]()
  postAxi = postResults[1]()
  postSig = postResults[2]()

  # closing the job_server
  job_server.destroy()

  print 'Posterior pdfs calculations are completed...'
  print
  print 'BayesianKinematics2D completed successfully!'
  print

  return postAmp, postAxi, postSig

  ## End of BayKin subroutine