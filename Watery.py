import datetime
from WateryModules import *
from BayesianKinematics2D import BayKin

'''
The Watery code.
'''


# This is just for measuring the time it takes to execute this code
datum = datetime.datetime.now()

# choosing a halo
halo = 'A'
print 'Aquarius halo chosen: ', halo

# location of the data
database = '/data/users/jovan/Aquarius/PresentDay/halo-' + halo + '/'
dataProducts = database + 'dataProducts/'

# # Selecting the particles which might contain GCs 
WParticleSelector(database + 'full-data-binary.npy', 
                  dataProducts + 'Particle.Data.npy')

# Centring the particles, so that the origin is at the most dense point
# This option can be commented out if not needed
WGalaxyCentring(dataProducts + 'Particle.Data.npy')

# Scaling the galaxy to Aq-A
WGalaxyScaling(dataProducts + 'Particle.Data.npy',
               dataProducts + 'ScaledParticle.Data.npy',
               halo)

# Observing the particles, given a specific location of the observer
observer = array([0., 0., 1000.])

# The transformation matrix, i.e. how the observer perceives the system.
mtrans = transformation_matrix(observer)


# Observing the data
WDataObservation(dataProducts + 'ScaledParticle.Data.npy',
                 mtrans, observer,
                 dataProducts + 'Particle.ObservedData.npy',  
                 backfeed = False)


# # Selecting the actual GCs from the observed data
WGlobularSelector(dataProducts + 'Particle.ObservedData.npy',
                  dataProducts + 'GC.ObservedData.npy')

# test completed

# Load the GC observed data
GCdata = load(dataProducts + 'GC.ObservedData.npy')

x        = GCdata[0]
y        = GCdata[1]
vel      = GCdata[2]
# Rproj    = GCdata[3]
posAng   = GCdata[4]
dm_id    = GCdata[5]
# str_id   = GCdata[6]
# str_mass = GCdata[7]

# clean up
del GCdata

# adding "observational" errors
err_vel = ones(len(vel)) * 15.0

# Kinematic analysis of the GC sample

# the parameter space to be explored
# Amplitude space
rangeAmp = arange(0.0, 250.0, 3.0)
# Velocity dispersion space
rangeSig = arange(0.0, 180.0, 3.0)
# Rotation axis space
rangeAxi = arange(0.0, 2.0*pi+0.10, 0.15)

# Determining the systemic velocity of the halo from this point of view
galvel   = load(dataProducts + 'Particle.ObservedData.npy')[2]
galRproj = load(dataProducts + 'Particle.ObservedData.npy')[4]
vsys     = mean(galvel[galRproj < 30.0])

# Bayesian analysis
postAmp, postAxi, postSig = BayKin(vel = vel, 
                                   err_vel = err_vel, 
                                   posAng = posAng, 
                                   vsys = vsys,
                                   rangeAmp = rangeAmp, 
                                   rangeAxi = rangeAxi, 
                                   rangeSig = rangeSig)

# Results of the kinematic analysis
WResults(postAmp,rangeAmp,postAxi,rangeAxi,postSig,rangeSig, statistic='mean')

# Plotting the results
# path to the full observed data
full2Ddata = dataProducts + 'Particle.ObservedData.npy' 

WHaloPlot(x, y, 
          vel, err_vel, 
          posAng, vsys,
          postAmp, rangeAmp, 
          postAxi, rangeAxi, 
          postSig, rangeSig, 
          halo, mtrans)

# This is here just for completeness and testing.
# The returned data are not used in any particular way.
WInertia(dm_id,dataProducts + 'Particle.Data.npy')

datum1 = datetime.datetime.now()
print 
print 'Watery successfully completed in: ', datum1 - datum
print 'The rent is too DAMN high!!!'
print