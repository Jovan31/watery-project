import datetime
from myClassesAq import *
from WateryModules import *
from readcol import readcol
from BayesianKinematics2D import BayKin


'''
This code reads the output of the cluster-selector.py code. It transform the 
input data in an appropriate format and then it does some sort of kinematic 
analysis to test for rotation. Then it runs through my Bayesian method for 
determining kinematic parameters. Finally the results are shown in a figure.

Version: 0.1
'''

# This is just for measuring the time it takes to execute this code
datum = datetime.datetime.now()

################################################################################
## THE DATA
################################################################################
database = '/data/users/jovan/Aquarius/PresentDay/halo-A/'
GCdata = readcol(database + 'GC.data', comment = '#', twod = True)

# where the original GC data is stored, in a class called GC
GC_list = []
for i in GCdata:
    temp_GC = GC(x=i[0], y=i[1], z=i[2], 
                vel_x=i[3], vel_y=i[4], vel_z=i[5], 
                str_id=i[6], dm_id=i[7])
    GC_list.append(temp_GC)
    del temp_GC

# clean up
del GCdata

# the streams
with open(database + 'selected_streams.dill', 'rb') as f:
        stream_list = dill.load(f)

################################################################################
## DATA MANIPULATION
################################################################################

# The position of the observer
observer = array([1000., 0., 0.])

#create transformation matrix for how the observer sees the particles
mtrans = transformation_matrix(observer)

# calculating line-of-sight velocities, and conversion to "standard coordinates" 
# as if the GCs were observed
# total number of GCs
num_GC = len(GC_list)
# here the line-of-sight velocities are store
losvel = [None] * num_GC
# here the new x,y coords will be stored, the "standard-like coordinate".
standard_coords = [None] * num_GC

for i in range(num_GC):
    losvel[i] = los_velocity(GC_list[i], observer)
    standard_coords[i] = coords_transform(mtrans, GC_list[i].pos)

# convert the line-of-sight velocity and new_coords 
# lists to arrays for convenience
losvel = array(losvel)
standard_coords = transpose(array(standard_coords))
# since the line-of-sight velocity are error-less 
# (this is a simulation after all), I will add a crude +/- 15 km/s 
# "observational" error myself
err_vel = ones(len(losvel)) * 15.

# Calculating the position angle of the GC as seen by the observer
# Theta is in circular polar coordinates, centred on the galaxy.
# Theta is in radians, measured from (the arbitrary) North to East 
# as seen by the observer.
posAngle = zeros(num_GC)    # array to store the position angles in.
posAngle = posAngCal(standard_coords[0], standard_coords[1])


# Calculating the projected radius of the GC, if needed for whatever reason
projR = zeros(num_GC)
projR = (standard_coords[0]**2.0 + standard_coords[1]**2.0)**0.5



################################################################################
## BAYESIAN KINEMATIC TEST
################################################################################

# Here I use the module I created from the original Bayesian Kinematic code, 
# to test the kinematics of a GC system. 

# the parameter space to be explored
# Amplitude space
rangeAmp = arange(0.0, 150.0, 2.0)
# Velocity dispersion space
rangeSig = arange(0.0, 200.0, 2.0)
# Rotation axis space
rangeAxi = arange(0.0, 2.0*pi+0.10, 0.15)

postAmp, postAxi, postSig = BayKin(losvel, err_vel, posAngle)

################################################################################
## RESULTS OF THE KINEMATIC ANALYSIS
################################################################################

WResults(postAmp, rangeAmp, postAxi, rangeAxi, postSig, rangeSig)

################################################################################
## PLOTTING THE RESULTS
################################################################################

# WQuickPlot(standard_coords, losvel, err_vel,
#            postAmp, rangeAmp, postAxi, rangeAxi, postSig, rangeSig,
#            stream_list, observer)

datum1 = datetime.datetime.now()
print 
print 'Job complete in: ', datum1 - datum
print 'The rent is too DAMN high!!!'
print

