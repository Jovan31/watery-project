'''
Relatively short classes and functions needed for the Aquarius project
'''

# import dill
import pylab as p
from scipy import *
import random as rd
from matplotlib.patches import Circle



################################################################################
class GC(object):
    '''
    This is a class which contains one particle per object, which is deemed to
    contain a globular cluster within it. All needed transformations will be
    done to the selected particles of this class to get them into "observable"
    shape, so I can do the analysis on them, such as rotation test etc..
    '''
    def __init__(self, x, y, z, vel_x, vel_y, vel_z, str_id, dm_id):
        self.str_id = str_id
        self.x = x
        self.y = y
        self.z = z
        self.vel_x = vel_x
        self.vel_y = vel_y
        self.vel_z = vel_z
        self.str_id = str_id
        self.dm_id = dm_id
        self.pos = array([self.x, self.y, self.z])
        self.vel = array([self.vel_x, self.vel_y, self.vel_z])
    def __str__(self):
        return 'DM particle containing a GC, id: %i' % (self.dm_id)
    def changePos(self, newx = None, newy = None, newz = None):
        if newx:
            self.x = newx
        if newy:
            self.y = newy
        if newz:
            self.z  = newz
        self.pos = array([self.x, self.y, self.z])
    def changeVel(self, newvel_x = None, newvel_y = None, newvel_z = None):
        if newvel_x:
            self.vel_x = newvel_x
        if newvel_y:
            self.vel_y = newvel_y
        if newvel_z:
            self.vel_z = newvel_z
        self.vel = array([self.vel_x, self.vel_y, self.vel_z])
################################################################################
class GC2D(object):
    '''
    This class holds a GC as seen from an observer. It should contain all
    needed properties of such a cluster.
    '''
    def __init__(self, x, y, losvel, str_id, dm_id):
        self.x = x
        self.y = y
        self.losvel = losvel
        self.str_id = str_id
        self.dm_id = dm_id
        self.Rproj = (self.x**2.0 + self.y**2.0)**0.5
        self.pos_ang = posAngCal([self.x], [self.y])[0]
    def __str__(self):
        return 'Observed GC, id: %i' % (self.dm_id)
    def changePos(self, newx = None, newy = None, newpos_ang = None):
        if newx:
            self.x = newx
        if newy:
            self.y = newy
    def changeVel(self, newlosvel = None):
        if newlosvel:
            self.losvel = newlosvel
################################################################################
def los_velocity(GC,origin):
    '''
    Calculates the line of sight velocity for a GCs provided an origin
    (the point of view of the observer)
    '''

    # shift for origin
    shifted_x = GC.x - origin[0]
    shifted_y = GC.y - origin[1]
    shifted_z = GC.z - origin[2]

    # calculate spherical polar coordinates
    r = (shifted_x**2.0 + shifted_y**2.0 + shifted_z**2.0)**0.5
    theta = arccos(shifted_z/r) - deg2rad(90.)
    phi = arctan(shifted_y/shifted_x)

    # this is dot product between the radius vector of the position of the GC as
    # seen from the origin with the velocity vector of the GC, effectively
    # projecting the velocity along the radius vector (line-of-sight)
    los = cos(theta)*cos(phi) * GC.vel_x + cos(theta) * sin(phi) * GC.vel_y - \
          sin(theta) * GC.vel_z

    # return the result
    return los

################################################################################
def los_velocity2(pos,vel,origin):
    '''
    Calculates the line of sight velocity for a GCs provided an origin
    (the point of view of the observer)

    arguments:
    pos = array-like, 3d positions
    vel = array-like, 3d velocities
    origin = the location of the observer

    returns the line-of-sight velocity
    '''

    # shift for origin
    shifted_x = pos[0] - origin[0]
    shifted_y = pos[1] - origin[1]
    shifted_z = pos[2] - origin[2]

    # calculate spherical polar coordinates
    r = (shifted_x**2.0 + shifted_y**2.0 + shifted_z**2.0)**0.5
    theta = arccos(shifted_z/r) - deg2rad(90.)
    phi = arctan(shifted_y/shifted_x)

    # this is dot product between the radius vector of the position of the GC as
    # seen from the origin with the velocity vector of the GC, effectively
    # projecting the velocity along the radius vector (line-of-sight)
    los = cos(theta)*cos(phi) * vel[0] + cos(theta) * sin(phi) * vel[1] - \
          sin(theta) * vel[2]

    # return the result
    return los

################################################################################
def perpendicular_vector(v):
    r""" Finds an arbitrary perpendicular normalized vector to *v*."""
    # for two vectors (x, y, z) and (a, b, c) to be perpendicular,
    # the following equation has to be fulfilled
    #     0 = ax + by + cz

    # x = y = z = 0 is not an acceptable solution
    if v[0] == v[1] == v[2] == 0:
        raise ValueError('zero-vector')

    # If one dimension is zero, this can be solved by setting that to
    # non-zero and the others to zero. Example: (4, 2, 0) lies in the
    # x-y-Plane, so (0, 0, 1) is orthogonal to the plane.
    if v[0] == 0:
        return array([-1., 0., 0.])
    if v[1] == 0:
        return array([0., 1., 0.])
    if v[2] == 0:
        return array([0., 0., 1.])

    # arbitrarily set a = b = 1
    # then the equation simplifies to
    #     c = -(x + y)/z
    # normalizing the vector
    temp =  array([1., 1., -1.0 * (v[0] + v[1]) / v[2]])
    norm_vect = sqrt(temp[0]**2.0 + temp[1]**2.0 + temp[2]**2.0)
    return temp / norm_vect


################################################################################
def transformation_matrix(xdirection):
    '''
    This function determines the transformation of data to a new coordinate
    system where the observer is located along the xdirection (the x axis of the
    new coordinate system). The new coordinate system shares the origin with the
    original one. This transformation is done in order to observe the data from
    the point of view of the observer.
    '''
    # normalize xdirection
    norm_xdir = sqrt(xdirection[0]**2.0+xdirection[1]**2.0+xdirection[2]**2.0)
    xdirection = xdirection / norm_xdir

    # find the ydirection - any normalized vector perpendicular to dxirection
    ydirection = perpendicular_vector(xdirection)
    # check if ydirection is normalized
    if round((ydirection[0]**2.0+ydirection[1]**2.0+ydirection[2]**2.0)**0.5
             )!=1.0:
        raise ValueError('ydirection not normalized!')

    # now find the zdirection by taking the cross product of the
    # xdirection and  ydirection
    zdirection = cross(xdirection,ydirection)

    # normalize zdirection
    norm_zdir = sqrt(zdirection[0]**2.0+zdirection[1]**2.0+zdirection[2]**2.0)
    zdirection = zdirection/norm_zdir

    trans_matrix = matrix([xdirection,ydirection,zdirection])

    print
    print 'Transformation matrix used:'
    print trans_matrix
    print

    # now create the transformation matrix
    return trans_matrix


################################################################################
def coords_transform(mtrans, position):
    '''
    This function takes a transformation matrix and position coordinates
    (an array) and returns a pair of new x and y coordinates, on the plane
    perpendicular to the line-of-sight, passing through the origin (basically
    the new ydirection = newx and new zdirection = new y).The distance to the
    particles, aka the xdirection is ignored.
    '''
    # converting the position array to a matrix array for the coming matrix
    # multiplication.
    position = matrix(position)
    new_position = mtrans * position.T
    new_position = array(new_position.T)
    new_position = new_position[0]
    return new_position[1], new_position[2]

################################################################################
# def save_stuff(obj, filename):
#     '''Saves whatever random stuff with dill => dangerous practice!!!'''
#     with open(filename, 'wb') as output:
#         dill.dump(obj, output)

################################################################################
class Stream(object):
    '''
    This is a class containing a stream of particles. Apart from the str_id and
    mass, the rest of the attributes are arrays containing all the info for each
    particle of this stream.
    '''
    def __init__(self,  str_id, mass, x, y, z, vel_x, vel_y, vel_z, dm_id):
        self.str_id = str_id
        self.mass = mass
        self.x = x
        self.y = y
        self.z = z
        self.vel_x = vel_x
        self.vel_y = vel_y
        self.vel_z = vel_z
        self.str_id = str_id
        self.dm_id = dm_id
    def __str__(self):
        return 'Stream: %i,   total mass: %2.2e '  % (self.str_id, self.mass)
    def test(self):
        if (len(self.x) ==
            len(self.y) ==
            len(self.z) ==
            len(self.vel_x) ==
            len(self.vel_y) ==
            len(self.vel_z) ==
            len(self.dm_id)):
            return True


################################################################################
def posAngCal(x,y):
    ''' Calculates the position angle (in radians) for set of input
    "standard"-like coordinates x,y. The position angles are measured in the
    astronomical standard, such that north is up and east is left.

    More documentation
    '''

    # check if arrays x and y are the same legnth
    try:
        len(x) == len(y)
    except ValueError:
        print "Oops! x and y arguments are of different length!"

    # len of the input/output arrays
    l = len(x)

    # output array with final position angle values
    posAng = zeros(l)

    for i in range(l):
        if (  (x[i] > 0.0) & ( y[i] >= 0.0)):
            posAng[i] = arctan(y[i]/x[i])

        if ( (x[i] > 0.0) & (y[i] < 0.0)):
            posAng[i] = arctan(y[i]/x[i]) + 2.0*pi

        if ( x[i] < 0.0):
            posAng[i] = arctan(y[i]/x[i]) + pi

        if (( x[i] == 0.0 ) & (y[i] > 0.0)):
            posAng[i] = pi/2.0

        if (( x[i] == 0.0) & (y[i] < 0.0)):
            posAng[i] = 3.0*pi/2.0

        if ((x[i] == 0.0) & (y[i] == 0.0)):
            posAng[i] = 0.0

    # # converting the angles to the astronomical system
    for i in range(l):
        posAng[i] = 0.5*pi - posAng[i]
        if posAng[i] < 0.0:
            posAng[i] = posAng[i] + 2.0*pi

    return posAng

################################################################################

def mapper(stream_list, observer, GCpos, GCvel, nbins = 500, contr = False,
           xlimit = [150,-150], ylimit = [-150,150]):

    '''
    Produces a density plot of a list of stream objects. If needed, the density
    contours of each element in the list are plotted as well. Created for the
    purpose of visualizing the "stellar" streams of all relevant progenitors
    in a Aquarius halo. It also plots the selected GCs, colour coded for
    their line-of-sight velocity.

    Key Words:
    stream_list = list of streams used for the analysis..
    observer = position of the observer of the system
    GCpos = positions of the GCs, a 2d array first containing all the x and
            than all the y values
    GCvel = the velocities of the GCs
    nbins = number of bins (visualisation of the map)
    xlimit / ylimit = the axes limits of the resulting figure, they are lists
    contours = If true, contours of the streams are displayed



    Version 0.2
    '''

    # The transformation matrix
    mtrans = transformation_matrix(observer)


    # Determining the largest contributor to the system. This is needed
    # to establish the binning grid for the rest of the data elements.
    largest_contributor = argmax([len(i.x) for i in stream_list])

    xpos = [None] * len(stream_list[largest_contributor].x)
    ypos = [None] * len(stream_list[largest_contributor].x)

    for i in range(len(xpos)):
        xpos[i], ypos[i] = coords_transform(mtrans,
                                        (stream_list[largest_contributor].x[i],
                                        stream_list[largest_contributor].y[i],
                                        stream_list[largest_contributor].z[i],))

    # The important but here are the edges.. the same will be used for all
    # streams

    Z, xxedges, yyedges = histogram2d(xpos, ypos, bins = nbins)


    # the extent of the map
    extent = [xxedges[0], xxedges[-1], yyedges[0], yyedges[-1]]

    del Z, xpos, ypos, largest_contributor

    # setting up the figure properties
    figga = p.figure(figsize = (9,6),dpi = 100,facecolor = 'w',edgecolor = 'K')
    p.axes().set_aspect('equal')

    # limits and labels
    p.xlim(xlimit)
    p.ylim(ylimit)
    p.xlabel(r'$\xi$', fontsize = 10)
    p.ylabel(r'$\eta$', fontsize = 10)

    # colours for the contours if that option is selected
    clrs = ['Crimson', 'DeepPink', 'Tomato', 'Yellow', 'Magenta', 'Lime',
            'Green', 'Cyan', 'MediumBlue', 'SkyBlue', 'Navy', 'SandyBrown']

    # place to keep the processed data of the following loop
    ZZtop = []
    # this is the main part, here where the density map is created
    for i in range(len(stream_list)):
        # selecting a stream
        stream = stream_list[i]
        print 'Processing stream: ', stream.str_id

        # number of particles in this stream
        num_part = len(stream.x)

        # standard coordinates of those particles
        xpos = [None] * num_part
        ypos = [None] * num_part

        for j in range(num_part):
            xpos[j], ypos[j] = coords_transform(mtrans, (stream.x[j],
                                                            stream.y[j],
                                                            stream.z[j]))

        Z, xedges, yedges = histogram2d(xpos, ypos, bins = (xxedges,yyedges))

        # displaying the contours if that options is selected
        if contr == True:
            # the levels of the contours
            levels = (0.3*Z.max(), 0.1*Z.max())
            p.contour(Z.T,
                        levels,
                        origin='lower',
                        extent=extent,
                        colors = clrs[i],
                        lw = 0.7)
        else:
            pass

        # converting the resulting histogram to a matrix to easier handling
        Z = matrix(Z)
        ZZtop.append(Z)

        # clean up
        del Z

    # the sum of all matrices in the ZZtop list
    ZZtop_sum = zeros((nbins,nbins))
    ZZtop_sum = matrix(ZZtop_sum)
    for i in range(len(ZZtop)):
        ZZtop_sum = ZZtop_sum +ZZtop[i]
    # converting the resulting matrix into an np array again for easy handling
    ZZtop_sum = array(ZZtop_sum)
    # Taking the log10 for better visualisation
    ZZtop_sum = log10(ZZtop_sum + 1.0)

    # creating the density map
    p.imshow(ZZtop_sum.T,
                origin = 'lower',
                cmap = 'binary',
                extent = extent,
                vmin = 0,
                vmax = 0.75*(ZZtop_sum.T).max())

    # creating the colour mapped points
    cpoints = zip(GCpos[0], GCpos[1])
    cpoints3 = sorted(zip(GCvel, cpoints))

    for i,pp in enumerate(cpoints3):
        GCvel[i] = pp[0]
        GCpos[0,i] = pp[1][0]
        GCpos[1,i] = pp[1][1]


    # the velocity coloured points, representing the GCs
    p.scatter(GCpos[0], GCpos[1],
                c = GCvel,
                edgecolor = 'black',
                lw = 0.5)

    # two circles to guide the eye
    rad30 = Circle((0, 0),
                    radius =30,
                    facecolor = 'none',
                    edgecolor = 'purple',
                    lw = 1.0)
    rad100 = Circle((0, 0),
                    radius =100,
                    facecolor = 'none',
                    edgecolor = 'purple',
                    lw = 1.0)

    # stuff
    ax = p.gca()
    fig = p.gcf()

    # setting up additional parameters for the circles
    p.setp(rad30, ls='dashed')
    p.setp(rad100, ls='dashed')
    # adding the circles to the plot
    fig.gca().add_artist(rad30)
    fig.gca().add_artist(rad100)

    # add labels
    label1 = p.text(-225, 50, 'Line-of-sight velocity [km/s]',
                    rotation = 'vertical',fontsize = 10)

    p.colorbar()


    return figga

################################################################################

def wmapper(database2D, GCx=[], GCy=[], GCvel=[], nbins = 500, contr = False,
          xlimit = [150, -150], ylimit = [-150, 150]):

    '''
    Produces a density plot of the visible matter in the halo. If needed, the
    density contours of each progenitor are plotted as well. Created for the
    purpose of visualizing the 'stellar' streams of all relevant progenitors in
    an Aquarius halo It also plots the selected GCs, colour coded for their
    line-of-sight velocities. Specially written to work as an integral part of
    the WateryModules.

    Arguments:
    database2D    = path to the entire binary data file of the observed system
    GCx           = array-like, x positions of the GCs in standard coords
    GCy           = array-like, y positions of the GCs in standard coords
    GCvel         = array-like, the line-of-sight velocities of the GCs
    nbins         = number of bins for visualisation of the map
    xlimit/ylimit = the axes limits of the resulting figure, they are lists
    contours      = if true, contours of the streams are displayed

    Version: 0.2

    '''

    # reading the key stream data

    fulldata = load(database2D)
    sx       = fulldata[0]
    sy       = fulldata[1]
    sid      = fulldata[6]
    smass    = fulldata[7]

    # clean up
    del fulldata

    # determining the unique streams and associated masses
    strIDnMass     = zip(sid,smass)
    uni_strIDnMass = transpose(array(list(set(strIDnMass))))
    uni_sid        = uni_strIDnMass[0]
    uni_smass      = uni_strIDnMass[1]

    # Determining the largest contributor to the system. This is needed
    # to establish the binning grid for the rest of the data elements.
    largest_contributor = argmax([len(sx[sid == i]) for i in uni_sid])
    tempx = sx[sid == uni_sid[largest_contributor]]
    tempy = sy[sid == uni_sid[largest_contributor]]

    # The important bits here are the edges.. the same will be used for all
    # streams

    Z, xxedges, yyedges = histogram2d(tempx, tempy, bins = nbins)

    # the extent of the map
    extent = [xxedges[0], xxedges[-1], yyedges[0], yyedges[-1]]

    # clean up
    del Z, largest_contributor, tempx, tempy
    
    # place to keep the processed data of the following loop
    ZZtop = []
    for i in uni_sid:
        # standard coordinates of particles in stream i
        tempx = sx[sid == i]
        tempy = sy[sid == i]

        Z, xedges, yedges = histogram2d(tempx, tempy, bins = (xxedges,yyedges))

        # converting the resulting hustogram to matrix for easier handling
        Z = matrix(Z)
        ZZtop.append(Z)

        # clean up
        del Z, tempy, tempx

    # sum all the matrices in the ZZtop list
    ZZtop_sum = zeros((nbins, nbins))
    ZZtop_sum = matrix(ZZtop_sum)
    for i in range(len(ZZtop)):
        ZZtop_sum = ZZtop_sum + ZZtop[i]
    # converting the resulting matrix into an np array again for easy handling
    ZZtop_sum = array(ZZtop_sum)
    # taking the log10 for better visualisation
    ZZtop_sum = log10(ZZtop_sum + 1.0)

    # setting the figure properties
    figga = p.figure(figsize = (9,6),dpi = 100,facecolor = 'w',edgecolor = 'K')
    p.axes().set_aspect('equal')
    
    # limits and labels
    p.xlim(xlimit)
    p.ylim(ylimit)
    p.xlabel(r'$\xi$', fontsize = 10)
    p.ylabel(r'$\eta$', fontsize = 10)

    # colours for the contours if that option is selected
    clrs = ['Crimson', 'DeepPink', 'Tomato', 'Yellow', 'Magenta', 'Lime',
          'Green', 'Cyan', 'MediumBlue', 'SkyBlue', 'Navy', 'SandyBrown']
    
    # creating the density map
    p.imshow(ZZtop_sum.T,
             origin = 'lower',
             cmap = 'binary',
             extent = extent,
             vmin = 0,
             vmax = 0.75*(ZZtop_sum.T).max())

    # displaying the contours if that option is selected
    if contr == True:
        # the levels of the contours
        levels = (0.3 * Z.max(), 0.1 * Z.max())
        p.contour(Z.T,
                  levels,
                  origin = 'lower',
                  extent = extent,
                  colors = clrs[i],
                  lw = 0.7)
    else:
        pass


    # Creating the colour mapped points
    points = zip(GCx, GCy)
    points3 = sorted(zip(GCvel,points))

    for i,pp in enumerate(points3):
        GCvel[i] = pp[0]
        GCx[i] = pp[1][0]
        GCy[i] = pp[1][1]

    # the velocity coloured points, representing the GCs
    p.scatter(GCx,
              GCy,
              c = GCvel,
              edgecolor = 'black',
              lw = 0.5)

    # two circles to guide the eye
    rad30 = Circle((0, 0),
                    radius =30,
                    facecolor = 'none',
                    edgecolor = 'purple',
                    lw = 1.0)
    rad100 = Circle((0, 0),
                    radius =100,
                    facecolor = 'none',
                    edgecolor = 'purple',
                    lw = 1.0)

    # stuff
    ax = p.gca()
    fig = p.gcf()

    # setting up additional parameters for the circles
    p.setp(rad30, ls='dashed')
    p.setp(rad100, ls='dashed')
    # adding the circles to the plot
    fig.gca().add_artist(rad30)
    fig.gca().add_artist(rad100)

    # add labels
    # add labels
    if len(GCvel) >5:
        label1 = p.text(-225, 50, 'Line-of-sight velocity [km/s]',
                        rotation = 'vertical',fontsize = 10)
        p.colorbar()
    else:
        pass


    return figga

################################################################################
def GCbinner(stream, mass):
    '''
    DEPRECATED, OBSOLETE

    This module randomly decides how many GCs each stream contributes to
    the halo. The decision is based on the mass of the streams (for now).
    Following this, the module calculates how many clusters should be put
    into the pre-determined radial bins to populate the halo. This is done
    in order to mimick the M31 GC system. The clusters are expected to have
    R^-3.34 radial profile.

    arguments
    stream = array-like, the stream ids to be processed
    mass   = array-like, the associated masses of the streams

    output
    GC_quota = array, number of clusters donated by that stream
    GC-bin   = array, number of clusters in each radial bin. There are 6 bins.

    Version: 0.1

    '''

    # number of streams to consider
    l = len(stream)

    # simple check if the number of stream ids is the same as number of mass
    try:
        len(mass) == len(stream)
    except ValueError:
        print '''Oops! The number of stream ids does not equal the number of
                 masses entered into the module! Please check the input
                 arguments before retrying.'''

    # deciding how many GC each stream will contribute
    GC_quota = zeros(l)  # here the quota is stored, one element per stream
    for i in range(l):
        if (mass[i] > 10.**6.) & (mass[i] < 10.**7):
            GC_quota[i] = rd.randint(3,5)
        elif (mass[i] > 10.**7.) & (mass[i] < 10.**8):
            GC_quota[i] = rd.randint(7,10)
        elif mass[i] > 10.**8. :
            GC_quota[i] = rd.randint(8,15)

    # The total number of GCs donated
    Ntot = sum(GC_quota)

    # The next part of the code determines how many clusters are going to
    # populate the pre-determined bins

    # This helps get the master normalization constant required for the
    # appropriate binning of the data. It is obtained experimentally by taking
    # the ration between a test case: the total number of GC generated in a
    # test run, and the appropriate master normalization constant obtained by
    # trial and error. Tested and it works fine for other samples.
    funky = 50./1220.

    C = Ntot / funky

    test_number_GC = 0.
    Bin_quota = zeros(6)
    # for i in range(30,150,20):
    rmin = 30.
    for i in range(6):
        rmax = rmin + 20.
        rmid = rmin + 10.
        area = pi*(rmax**2.0 - rmin**2.0)
        # area = pi*((i+20.)**2.0 - i**2.0)
        Bin_quota[i] = int(round(((rmid)**(-3.34)* C * area)))
        rmin = rmax

    if Ntot == sum(Bin_quota):
        pass
    elif Ntot > sum(Bin_quota):
        temp = rd.randint(0,5)
        Bin_quota[temp] += 1
    else:
        temp = rd.randint(0,5)
        Bin_quota[temp] -= 1

    # print Ntot
    # print Bin_quota

    return GC_quota, Bin_quota

################################################################################

def sig_diff(a,b,da,db):
    '''
    Checks the number of standard deviations between a and b.
    da and db are the Gaussian uncertainties of a and b.
    The number of standard deviations is returned.
    '''

    temp_up = (abs(float(a) - float(b)))
    temp_down = (float(da)**2.0 + float(db)**2.0)**0.5

    return temp_up/temp_down


################################################################################
def azym_sym_test(posAngle, sig = 2):
    '''
    This function checks whether the selected clusters are distributed in a
    azimuthally symmetric manner in the halo. This is done in a simple manner.
    The halo is split in 4 quadrants. The number of GCs in each quadrant is
    found, as well as the Poisson uncertainty. If the all quadrants have equal
    number of GCs up to 1-sigma, 0 is returned. Alternatively, the quadrants
    having the maximum number of clusters are returned (if it is just one
    quadrant, only the one is returned.)

    arguments
    posAngle = array-like, the position angles of the GCs, in radians
    sig = float, the number of standard deviation above which significance
          is claimed. Default value is 2.

    Returns a number: 0 if all quadrants have the same number of GCs up to
    1-sigma, or a number of the quadrants having the most clusters.
    '''

    # number of clusters in each of the 4 quadrants
    q1 = len(posAngle[(posAngle >= 0 ) & (posAngle < pi/2.)])
    q2 = len(posAngle[(posAngle >= pi/2.) & (posAngle < pi)])
    q3 = len(posAngle[(posAngle >= pi) & (posAngle < 3.*pi/2.)])
    q4 = len(posAngle[(posAngle >= 3.*pi/2.) & (posAngle < 2*pi)])

    # Poisson uncertainties in each of the quadrants
    eq1 = q1**0.5
    eq2 = q2**0.5
    eq3 = q3**0.5
    eq4 = q4**0.5

    # Checking if the number of clusters in each of the quadrants matches

    if ((sig_diff(q1,q2,eq1,eq2) < sig) and
        (sig_diff(q1,q3,eq1,eq3) < sig) and
        (sig_diff(q1,q4,eq1,eq4) < sig) and
        (sig_diff(q2,q3,eq2,eq3) < sig) and
        (sig_diff(q2,q4,eq2,eq4) < sig) and
        (sig_diff(q3,q4,eq3,eq4) < sig)):

        return [0]

    else:
        res = []
        val = array([q1,q2,q3,q4])
        err = array([eq1,eq2,eq3,eq4])

        for i in range(len(val)):
            for j in range(i,len(val)):
                if i != j:
                    if ((sig_diff(val[i],val[j],err[i],err[j]) >=sig) and
                        (val[i] > val[j])):
                        res.append(i)
                    elif ((sig_diff(val[i],val[j],err[i],err[j]) >=sig) and
                        (val[i] < val[j])):
                        res.append(j)
                    else:
                        pass

        res = array(res)
        res = res + 1
        res = unique(res)
        # print val
        return res.tolist()


################################################################################
def quad_clust_counter(posAngle):
    '''
    This function is similar to the azym_sym_test, bit in only counts the
    number of clusters present in each of the quadrants. The idea here is that
    distribution of clusters will be analysed at some later stage, and at
    the point in which this function is used, only the number of clusters in
    each quadrant is required.

    Input:
    posAngle  = array_like, position angles of the GCs in radians
    Returns
    q1,q2,q3,q4 = the number of clusters in quadrant1, quad2, quad3 and quad 4
    '''

    # number of clusters in each of the 4 quadrants
    q1 = len(posAngle[(posAngle >= 0 ) & (posAngle < pi/2.)])
    q2 = len(posAngle[(posAngle >= pi/2.) & (posAngle < pi)])
    q3 = len(posAngle[(posAngle >= pi) & (posAngle < 3.*pi/2.)])
    q4 = len(posAngle[(posAngle >= 3.*pi/2.) & (posAngle < 2*pi)])

    return q1, q2, q3, q4


################################################################################
def azym_sym_test_quad(q1, q2, q3, q4, sig = 2):
    '''
    Exactly the same as azym_sym_test, but instead of position angles, the
    input are four numbers, the number of clusters in each quadrant. Basically
    the counting has already been done, and now only the statistics is required.

    arguments
    q1  = float, number of clusters found in quadrant 1
    q2  = float, number of clusters found in quadrant 2
    q3  = float, number of clusters found in quadrant 3
    q4  = float, number of clusters found in quadrant 4
    sig = float, the number of standard deviation above which significance
          is claimed. Default value is 2.

    Returns a number: 0 if all quadrants have the same number of GCs up to
    1-sigma, or a number of the quadrants having the most clusters.
    '''

    # Poisson uncertainties in each of the quadrants
    eq1 = q1**0.5
    eq2 = q2**0.5
    eq3 = q3**0.5
    eq4 = q4**0.5

    # Checking if the number of clusters in each of the quadrants matches

    if ((sig_diff(q1,q2,eq1,eq2) < sig) and
        (sig_diff(q1,q3,eq1,eq3) < sig) and
        (sig_diff(q1,q4,eq1,eq4) < sig) and
        (sig_diff(q2,q3,eq2,eq3) < sig) and
        (sig_diff(q2,q4,eq2,eq4) < sig) and
        (sig_diff(q3,q4,eq3,eq4) < sig)):

        return [0]

    else:
        res = []
        val = array([q1,q2,q3,q4])
        err = array([eq1,eq2,eq3,eq4])

        for i in range(len(val)):
            for j in range(i,len(val)):
                if i != j:
                    if ((sig_diff(val[i],val[j],err[i],err[j]) >=sig) and
                        (val[i] > val[j])):
                        res.append(i)
                    elif ((sig_diff(val[i],val[j],err[i],err[j]) >=sig) and
                        (val[i] < val[j])):
                        res.append(j)
                    else:
                        pass

        res = array(res)
        res = res + 1
        res = unique(res)
        # print val
        return res.tolist()

################################################################################
def azym_sim_test_mean(q1, q2, q3, q4, sig = 2):
    '''
    This is the test that Amina proposed. The mean number of clusters 
    amongst the four quadrants is found first. Then, it is check whether 
    any quadrant deviates by more then \sig\ from that mean.

    arguments
    q1  = float, number of clusters found in quadrant 1
    q2  = float, number of clusters found in quadrant 2
    q3  = float, number of clusters found in quadrant 3
    q4  = float, number of clusters found in quadrant 4
    sig = float, the number of standard deviation above which significance
          is claimed. Default value is 2.

    Returns a number: 0 if all quadrants have the same number of GCs up to
    1-sigma, or a number of the quadrants having the most clusters.
    '''
    
    # Poisson uncertainties in each of the quadrants
    eq1 = q1**0.5
    eq2 = q2**0.5
    eq3 = q3**0.5
    eq4 = q4**0.5

    # creating arrays for easier handling
    q = array([q1,q2,q3,q4])
    eq = array([eq1,eq2,eq3,eq4])

    # finding the mean and standard deviations of the number of GC in the
    # four quadrants
    mq = mean(q)
    stdq = std(q)

    # this is the resulting list
    res = []

    # performing the check: the magic happens here
    for i,v in enumerate(q):
        x = abs(mq - v) / (stdq**2.0 + eq[i]**2.0)**0.5
        if x >= sig:
            res.append(i)

    # This is done in case no quadrant deviates away from the mean by at least
    # \sig\
    if len(res) == 0:
        return [0]
    else:
        res = array(res)
        res = res + 1
        return res.tolist()

################################################################################
def azym_sim_test_mean_2(q1, q2, q3, q4, sig = 2):
    '''
    This is the test that Amina proposed but reloaded. The mean number of 
    clusters amongst the four quadrants is found first. Then, it is check 
    whether any quadrant deviates by more then \sig\ from that mean. 

    The difference in this version is that information is retained to whether 
    the quadrant has an excess or a lack of clusters compared to the mean.


    arguments
    q1  = float, number of clusters found in quadrant 1
    q2  = float, number of clusters found in quadrant 2
    q3  = float, number of clusters found in quadrant 3
    q4  = float, number of clusters found in quadrant 4
    sig = float, the number of standard deviation above which significance
          is claimed. Default value is 2.

    Returns a number: 0 if all quadrants have the same number of GCs up to
    1-sigma, or the number of the quadrants deviating from the mean by the 
    designated amount. If the quadrant is lacking clusters, than a negative 
    number is written.
    '''
    
    # Poisson uncertainties in each of the quadrants
    eq1 = q1**0.5
    eq2 = q2**0.5
    eq3 = q3**0.5
    eq4 = q4**0.5

    # creating arrays for easier handling
    q = array([q1,q2,q3,q4])
    eq = array([eq1,eq2,eq3,eq4])

    # finding the mean and standard deviations of the number of GC in the
    # four quadrants
    mq = mean(q)
    stdq = std(q)

    # this is the resulting list
    res = []

    # performing the check: the magic happens here
    for i,v in enumerate(q):
        x = (v - mq) / (stdq**2.0 + eq[i]**2.0)**0.5
        if x >= sig:
            res.append(i+1)
        elif x <= -sig:
            res.append(-1*(i+1)) 
        else:
            pass

    # This is done in case no quadrant deviates away from the mean by at least
    # \sig\
    if len(res) == 0:
        return [0]
    else:
        res = array(res)
        return res.tolist()

################################################################################
def GCbinner2(database2D):
    '''
    This is the new version of the now obsolete GCbinner function.

    First, the module randomly decides how many GCs each stream contributes to 
    the halo. The decision is based on the mass of the streams. Then the module 
    calculates how many clusters should be put into the pre-determined radial 
    bins to populate the halo GC system. To do this, the module determines the 
    radial number density profile of the 'star' particles. Given the total 
    number of GC in the halo has already been determined, the profile of the GCs
    is a scaled down version of the profile 'measured' for the 'star' particles.

    arguments
    database2D = path to the entire binary data file of the observed system

    output
    GC_quota = array, number of clusters donated by that stream
    GC-bin   = array, number of clusters in each radial bin. There are 6 bins.

    '''

    # reading the relevant parts of the observed data
    data     = load(database2D)
    # selecting only those particles that have Rproj >= 30 kpc
    Rproj = data[4]
    # select the particles in the appropriate range only
    ind   = (Rproj >= 30.) & (Rproj < 150.)

    # clean up
    del Rproj

    # select the appropriate data
    x        = data[0][ind]
    y        = data[1][ind]
    Rproj    = data[4][ind]
    str_id   = data[6][ind]
    str_mass = data[7][ind]

    # clean up
    del data, ind

    # selecting the unique streams, but also keeping knowing the mass of
    # that stream
    str_id_mass     = zip(str_id, str_mass)
    uni_str_id_mass = transpose(array(list(set(str_id_mass))))
    uni_str         = uni_str_id_mass[0]
    uni_mass        = uni_str_id_mass[1]

    # Here i need to flag the streams that are worthy of clusters
    # being drawn from them
    flag = ones(len(uni_str))
    for i,stream in enumerate(uni_str):
        if ((max(Rproj[str_id == stream]) - min(Rproj[str_id == stream]) < 20)):
            flag[i] = 0
        else:
            pass

    # selecting only the streams that donate clusters only
    mass  = uni_str[flag  == 1]
    stream = uni_mass[flag == 1]

    # clean up
    del str_id_mass, uni_str_id_mass

    # number of streams to consider
    l = len(stream)

    # simple check if the number of stream ids is the same as the mass
    try:
        len(mass) == len(stream)
    except ValueError:
        print '''Oops! The number of stream ids does not equal the number of
                 masses entered into the module! Please check the input
                 arguments before retrying.'''

    # deciding how many GC each stream will contribute
    GC_quota = zeros(l)  # here the quota is stored, one element per stream
    for i in range(l):
        if (mass[i] > 10.**6.) & (mass[i] < 10.**7):
            GC_quota[i] = rd.randint(3,5)
        elif (mass[i] > 10.**7.) & (mass[i] < 10.**8):
            GC_quota[i] = rd.randint(7,10)
        elif mass[i] > 10.**8. :
            GC_quota[i] = rd.randint(8,15)

    # The total number of GCs donated
    Ntot = sum(GC_quota)

    # the total number of GC to be drawn is determined, as well as the 
    # contribution of each stream in turn.

    # Now measure the 'stellar' profile of the halo, and determine how to 
    # populate the radial bins with GCs..

    # this is measuring the 'star' profile of the halo
    # creates the bin edges
    bins = arange(30,160,20)
    # bins value (bv), bin edge (be)
    bv, be = histogram(Rproj, bins = bins)
    # normalize the bin values
    bv = bv/float(sum(bv))

    # now populate the bin quota (number of clusters in each radial bin)
    Bin_quota = ones(len(bv)) * Ntot
    # and these should be the bins with the right number of clusters. No noise.
    Bin_quota = rint(Bin_quota * bv)

    # print Ntot
    # print Bin_quota

    # And that's it! Return the results
    return GC_quota, Bin_quota   

################################################################################