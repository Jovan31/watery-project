import datetime
import pylab as p
from myClassesAq import *
from readcol import readcol
from matplotlib.patches import Circle
from BayesianKinematics2D import BayKin
from matplotlib.backends.backend_pdf import PdfPages
from confidenceLimits import confidence, zeroCase, circularCase



'''
This code reads the output of the cluster-selector.py code. It transform the 
input data in an appropriate format and then it does some sort of kinematic 
analysis to test for rotation. Then it runs through my Bayesian method for 
determining kinematic parameters. Finally the results are shown in a figure.

Version: 0.1
'''
# This is just for measuring the time it takes to execute this code
datum = datetime.datetime.now()


################################################################################
## THE DATA
################################################################################
database = '/data/users/jovan/Aquarius/PresentDay/halo-A/'
GCdata = readcol(database + 'GC.data', comment = '#', twod = True)

# where the original GC data is stored, in a class called GC
GC_list = []
for i in GCdata:
    temp_GC = GC(x=i[0], y=i[1], z=i[2], 
                vel_x=i[3], vel_y=i[4], vel_z=i[5], 
                str_id=i[6], dm_id=i[7])
    GC_list.append(temp_GC)
    del temp_GC

# clean up
del GCdata

################################################################################
## DATA MANIPULATION
################################################################################

# creating a .pdf file, needed for the final output
pp_file = PdfPages('/Users/users/jovan/Desktop/plots.pdf')

# The position of the observer: set up along each of the principal axis of 
# the master system
# The code will perform the analysis for each of them in turn
observerx = array([1000., 0., 0.])
observery = array([0., 1000., 0.])
observerz = array([0., 0., 1000.])
observer = array([observerx, observery, observerz])

for ii in observer:

    #create transformation matrix for how the observer sees the particles
    mtrans = transformation_matrix(ii)

    # calculating line-of-sight velocities, and conversion to 
    # "standard coordinates" as if the GCs were observed
    # number of GCs in this analysis
    num_GC = len(GC_list)
    # here the line-of-sight velocities are store
    losvel = [None] * num_GC
    # here the new x,y coords will be stored, the "standard-like coordinate".
    standard_coords = [None] * num_GC  

    for i in range(num_GC):
        losvel[i] = los_velocity(GC_list[i], ii)
        standard_coords[i] = coords_transform(mtrans, GC_list[i].pos)

    # convert the line-of-sight velocity and new_coords 
    # lists to arrays for convenience
    losvel = array(losvel)
    standard_coords = transpose(array(standard_coords))
    # since the line-of-sight velocity are error-less 
    # ( this is a simulation after all), I will add a crude +/- 15 km/s 
    # "observational" error myself
    err_vel = ones(len(losvel)) * 15.

    # Calculating the position angle of the GC as seen by the observer
    # Theta is in circular polar coordinates, centred on the galaxy.
    # Theta is in radians, measured from (the arbitrary) North to East as seen 
    # by the observer.

    # posAngle = zeros(num_GC)         # array to store the position angles in.
    posAngle = posAngCal(standard_coords[0], standard_coords[1])

    # Calculating the projected radius of the GC, if needed for whatever reason
    projR = zeros(num_GC)
    projR = (standard_coords[0]**2.0 + standard_coords[1]**2.0)**0.5



    ############################################################################
    ## BAYESIAN KINEMATIC TEST
    ############################################################################

    # Here I use the module I created from the original Bayesian Kinematic code, 
    # to test the kinematics of a GC system. 

    # the parameter space to be explored
    # Amplitude space
    rangeAmp = arange(0.0, 150.0, 2.0)
    # Velocity dispersion space
    rangeSig = arange(0.0, 200.0, 2.0)
    # Rotation axis space
    rangeAxi = arange(0.0, 2.0*pi+0.10, 0.15)

    postAmp, postAxi, postSig = BayKin(losvel, err_vel, posAngle, 
        rangeAmp, rangeAxi, rangeSig)


    ############################################################################
    ## RESULTS OF THE KINEMATIC ANALYSIS
    ############################################################################

    print 
    print 'Amplitude >>>', confidence(postAmp, rangeAmp, statistic = 'mean')
    midAmp, lowAmp, highAmp = confidence(postAmp, rangeAmp, 
                                            plot_friendly =True, 
                                            statistic = 'mean')
    print
    print 'Rota axis >>>' , (rad2deg(circularCase(postAxi, rangeAxi, 
                                                    statistic = 'peak')[0]), 
                            rad2deg(circularCase(postAxi, rangeAxi, 
                                                    statistic = 'peak')[1]))
    midAxi, lowAxi, highAxi = circularCase(postAxi,rangeAxi, 
                                            plot_friendly = True, 
                                            statistic = 'peak')
    print
    print 'Velo disp >>> ', confidence(postSig, rangeSig, statistic = 'mean')
    midSig, lowSig, highSig = confidence(postSig,rangeSig, 
                                            plot_friendly = True, 
                                            statistic = 'mean')
    print

    ############################################################################
    ## PLOTTING THE RESULTS
    ############################################################################

    # defining the general figure properties, size etc
    figga = p.figure(figsize = (16,9), 
                    dpi = 200, 
                    facecolor = 'w', 
                    edgecolor = 'K')
    # The title of the plot
    p.title('Observer located at: ' + str(ii))
    # defining the subplots arrangement
    gs = p.GridSpec(3,2)

    # the sinusoidal plot
    p.subplot(gs[0,:])
    p.xticks([0,90,180,270,360])
    p.xlabel(r'$\theta_{0}$ ' + '[deg]', fontsize = 10)
    p.ylabel('$V_{\\rm{los}} $ [km s$^{-1}$]', fontsize = 10)
    p.errorbar(rad2deg(posAngle), losvel, 
                yerr = err_vel, 
                fmt = 'o', 
                ecolor = 'black', 
                color = 'black')
    p.plot([0,360], [0,0], color = 'black', lw = 0.3, ls = ':')
    # showing the rotation
    xvalues = linspace(0., 360., 3600.)
    yvalues = midAmp[0] * sin(deg2rad(xvalues) - midAxi[0])
    p.plot(xvalues, yvalues, color = 'black', lw = 1)

    # this is the "map"
    # p.subplot(gs[1,0], aspect = 'equal')
    p.subplot(gs[1,0])

    # creating the colour mapped points
    cpoints = zip(standard_coords[0], standard_coords[1])
    cpoints3 = sorted(zip(losvel, cpoints))

    for i,pp in enumerate(cpoints3):
        losvel[i] = pp[0]
        standard_coords[0,i] = pp[1][0]
        standard_coords[1,i] = pp[1][1]

    p.xlabel(r'$\xi$', fontsize = 10)
    p.ylabel(r'$\eta$', fontsize = 10)

    # the velocity colored GCs
    p.scatter(standard_coords[0], standard_coords[1], 
                c = losvel, 
                edgecolor = 'black', 
                lw = 0.5)
    
    # adding circles to guide the eye
    rad30 = Circle((0, 0), 
                    radius =30, 
                    facecolor = 'none', 
                    edgecolor = 'purple', 
                    lw = 1.0)
    rad100 = Circle((0, 0), 
                    radius =100, 
                    facecolor = 'none', 
                    edgecolor = 'purple', 
                    lw = 1.0)
    # stuff
    ax = p.gca()
    fig = p.gcf()
    # inverting the x-axis
    p.xlim(p.xlim()[::-1])

    # setting up additional parameters for the circles
    p.setp(rad30, ls='dashed')  
    p.setp(rad100, ls='dashed') 
    # addoing the circles to the plot
    fig.gca().add_artist(rad30) 
    fig.gca().add_artist(rad100)

    p.colorbar()

    # this is the pdf for the amplitude
    p.subplot(gs[1,1])
    p.xlabel('$A$ [km s$^{-1}$]', fontsize = 10)
    p.ylabel('Posterior probability', fontsize = 10)
    p.plot(rangeAmp, postAmp, lw =2, color  = 'black')
    # plotting the mean value
    p.plot([midAmp[0], midAmp[0]],[0, midAmp[1]], 
            lw = 0.9 , 
            color = 'black', 
            ls = ':')
    # plotting the confidence limits
    p.plot([lowAmp[0], lowAmp[0]],[0, lowAmp[1]], lw = 0.7 , color = 'blue')
    p.plot([highAmp[0], highAmp[0]],[0, highAmp[1]], lw = 0.7 , color = 'blue')

    # this is the pdf for the rotation axis
    p.subplot(gs[2,0])
    p.xlabel(r'$\theta_{0}$ ' + '[deg]', fontsize = 10)
    p.ylabel('Posterior probability', fontsize = 10)
    p.xticks([0,90,180,270,360])
    p.plot(rad2deg(rangeAxi),postAxi,lw = 2, color = 'black')
    # plotting the peak value
    p.plot([rad2deg(midAxi[0]), rad2deg(midAxi[0])], [0, midAxi[1]], 
            lw = 0.9, 
            color = 'black', 
            ls = ':')
    # plotting the confidence limits
    p.plot([(rad2deg(lowAxi[0])), (rad2deg(lowAxi[0]))], [0, lowAxi[1]], 
            lw = 0.7, 
            color = 'blue')
    p.plot([(rad2deg(highAxi[0])), (rad2deg(highAxi[0]))], [0, highAxi[1]], 
            lw = 0.7, 
            color = 'blue')

    # this is the pdf for the velocity dispersion
    p.subplot(gs[2,1])
    p.xlabel(r'$\sigma_{0}$ ' +'[km s$^{-1}$]', fontsize = 10)
    p.ylabel('Posterior probability', fontsize = 10)
    p.plot(rangeSig,postSig, lw = 2, color = 'black')
    # plotting the mean value
    p.plot([midSig[0], midSig[0]],[0, midSig[1]], 
            lw = 0.9 , 
            color = 'black', 
            ls = ':')
    # plotting the confidence limits
    p.plot([lowSig[0], lowSig[0]],[0, lowSig[1]], lw = 0.7 , color = 'blue')
    p.plot([highSig[0], highSig[0]],[0, highSig[1]], lw = 0.7 , color = 'blue')

    p.tight_layout()
    pp_file.savefig()

# for fun, editing the metadata of the pdf file that is created
metadata = pp_file.infodict()
metadata['Title'] = 'Kinematic Analysis of halo A from the Aquarius simulation'
metadata['Author'] = u'Jovan Veljanoski'
metadata['Keywords'] = 'Kinematics, Bayesian, Plot, Subplots, Aquarius'
metadata['CreationDate'] = datetime.datetime.now()

# close pdf document
pp_file.close()





























datum1 = datetime.datetime.now()
print 
print 'Job complete in: ', datum1 - datum
print 'The rent is too DAMN high!!!'
print