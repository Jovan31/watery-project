# import dill
# from scipy import *
# import random as random
from myClassesAq import *
from readcol import readcol


'''
This code should select some random dark matter particles from the Aquarius 
simulation data that I have been given to be Globular Clusters Globular. 
Following the selection, I should most likely need to visualize the data, and 
prepare it for the kinematic testing.

Version: 0.2
'''

## THE DATA

# reading the data:
database = '/data/users/jovan/Aquarius/PresentDay/halo-A/'
# the data, to save space is saved in a numpy binary file
fulldata = load(database + 'full-data-binary.npy')

# Since the streams can be uniquely identified by their ID, but we need their 
# mass to make selections. Since multiple streams can have the same mass, I 
# make a zip of the name and mass in order to make unique IDs for each stream 
# so they can be nicely selected. 
strIDnMass = zip(fulldata[7],fulldata[8])

# Now, only take the unique combinations, since the name and mass repeat for 
# each particle in each stream  as many times as there are particles in that 
# stream
uni_strIDnMass = transpose(unique(strIDnMass))

# this index selects the mass range of the streams
ind =  uni_strIDnMass[1] > 10**6.
selected_streams =  uni_strIDnMass[:,ind]
# delete unneeded data
del ind

# This is where the list of the streams will be stored, the ones to which 
# magic will be done
streams_list = []
# the storing starts here, and lets pray it works..it
for i in range(len(selected_streams[0])):
    # for selecting the stream particles later
    ind = ((fulldata[7] == selected_streams[0,i]) 
            & (fulldata[8] == selected_streams[1,i]))
    temp_steam = Stream(str_id = selected_streams[0,i], 
                        mass = selected_streams[1,i], 
                        x = fulldata[0][ind], 
                        y = fulldata[1][ind], 
                        z = fulldata[2][ind], 
                        vel_x = fulldata[3][ind], 
                        vel_y = fulldata[4][ind], 
                        vel_z = fulldata[5][ind],
                        dm_id = fulldata[6][ind])
    # add the selected stream object to the list of streams
    streams_list.append(temp_steam)

# clean up
del temp_steam, fulldata, strIDnMass, uni_strIDnMass, selected_streams

# saving the list of selected streams to disk:
save_stuff(streams_list, database+'selected_streams.dill')

# Here the clusters are selected from each of the streams that passed the 
# selection stage. The selection of clusters here is purely random: random 
# particles members of the streams are selected to represent clusters.

# list of GC objects to be stored and examined later
GC_list = []

for i in streams_list:
    # check how many particles are there in each stream
    streamsize = len(i.x)
    # the number of GC selected depends on the mass of the stream
    if ((i.mass > 10**6.) & (i.mass < 10**7.)):
        randomselector = random.sample(xrange(streamsize), 3)
    elif ((i.mass > 10**7.) & (i.mass < 10**8.)):
        randomselector = random.sample(xrange(streamsize), 5)
    elif (i.mass > 10**8.):
        randomselector = random.sample(xrange(streamsize), 10)

    for j in randomselector:
        temp_GC = GC(x = i.x[j],
                    y = i.y[j],
                    z = i.z[j],
                    vel_x = i.vel_x[j],
                    vel_y = i.vel_y[j],
                    vel_z = i.vel_z[j],
                    str_id = i.str_id,
                    dm_id = i.dm_id[j])
        # add the GC object to the list of GCs
        GC_list.append(temp_GC)

# clean up
del temp_GC, randomselector, streamsize


# Saving the cluster data to disk, in a normal text file, to be read later 
# and put once again into a GC class
output = open(database + 'GC.data', 'w')
# the header
print >>output, '%-23s %-23s %-23s %-23s %-23s %-23s %-23s %-23s ' % ('#x', 
        'y', 'z', 'vel_x', 'vel_y', 'vel_z', 'stream-id', 'dm_id')

for i in GC_list:
        print >>output, '%-23f %-23f %-23f %-23f %-23f %-23f %-23f %-23i ' % \
                        (i.x,
                        i.y,
                        i.z,
                        i.vel_x,
                        i.vel_y,
                        i.vel_z,
                        i.str_id,
                        i.dm_id)
output.close()

print
print 'Total of ', len(GC_list), ' GCs selected'
print
print 'The rent is too DAMN high'
print