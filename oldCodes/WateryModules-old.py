import beerstats as bs
from myClassesAq import *
from minorticks import minorticks
from matplotlib.backends.backend_pdf import PdfPages
from confidenceLimits import confidence, zeroCase, circularCase

def WResults(postAmp, rangeAmp, postAxi, rangeAxi, postSig, rangeSig,
             statistic = 'peak', backfeed = False):
    '''
    Displays the results of the BayesianKinematics2D.py code

    arguments:
    postAmp = posterior pdf of the amplitude
    postAxi = posterior pdf of the rotation axis
    postSig = posterior pdf of the velocity dispersion
    rangeAmp / rangeSig = the lattice of the pdfs of the amplitude and velocity
                          dispersion [km/s]
    rangeAxi = the lattice of the pdf of the rotation axis [rad]
    backfeed = boolean, chooses if certain values are returned or only printed

    Version: 0.1
    '''

    print
    print 'Displaying the results of the analysis...'
    print

    print
    print 'Amplitude >>>', confidence(postAmp, rangeAmp, statistic = statistic)
    print
    print 'Rota axis >>>', (rad2deg(circularCase(postAxi, rangeAxi,
                                    statistic = 'peak')[0]),
                            rad2deg(circularCase(postAxi, rangeAxi,
                                    statistic = 'peak')[1]))
    print
    print 'Velo disp >>>', confidence(postSig, rangeSig, statistic = statistic)
    print

    print
    print
    if backfeed == True:
        return confidence(postAmp, rangeAmp, statistic = 'mean')[0], \
        (rad2deg(circularCase(postAxi, rangeAxi, statistic = 'peak')[0])), \
        confidence(postSig, rangeSig, statistic = 'mean')[0]

    elif backfeed == False:
        return
    else:
        print 'unexpected parameter assigned to /backfeed/ argument. Retry.'


################################################################################
def WHaloPlot(GCx, GCy, GCvel, GCvel_err, GCpa,
              postAmp, rangeAmp, postAxi, rangeAxi, postSig, rangeSig,
              database2D, oname = '/Users/users/jovan/Desktop/fig.pdf'):
    '''
    This plots the results of the main code. The output is a pdf containing
    two pages. The first page shows a map of the system as seen by the
    observer, where the underlying stellar populations are shown, as well as
    the selected GCs which are colour coded according to their velocities.

    The dashed circles are only set to guide the eye, and they have radii
    of 30 and 100 kpc (?) respectively.

    The output is saved on the Desktop.

    arguments:
    postAmp / postAxi / postSig = posterior pdfs of the amplitude, rotation
                                  axis, and velocity dispersion as
                                  determined by the BayesianKinematics2D.py
                                  code.
    rangeAmp/ rangeAxi / rangeSig = the grids (lattices) along which the
                                    amplitude, rotation axis, and the
                                    velocity dispersion are searched.
    database2D = path to the entire binary data file of the observed system
    GCx = the x positions of the GCs in standard-like coords
    GCy = the y positions of the GCs in standard-like coords
    GCvel = velocities of the GCs
    GCvel_err = the uncertainties in the GC velocities
    GCpa = the position angle of the GCs, in radians
    oname = output name (this will always be a .pdf file)

    Version: 0.2
    '''

    print 'Plotting the halo...'

    # initiallizing the plot output
    plotoutput = PdfPages(oname)

    # Page One
    # the BigMap comes first to help visualise the system..
    wmapper(database2D, GCx, GCy, GCvel)
    plotoutput.savefig()

    # Page Two

    # annotations for the subplots
    midAmp, lowAmp, highAmp = confidence(postAmp, rangeAmp,
                                            plot_friendly =True,
                                            statistic = 'mean')
    midAxi, lowAxi, highAxi = circularCase(postAxi, rangeAxi,
                                            plot_friendly = True,
                                            statistic = 'peak')
    midSig, lowSig, highSig = confidence(postSig, rangeSig,
                                            plot_friendly = True,
                                            statistic = 'mean')

    # defining the general figure properties, size etc on Page Two
    figga = p.figure(figsize=(9,6),dpi=100,facecolor='w',edgecolor='K')
    # defining the subplots arrangement
    gs = p.GridSpec(2,2)

    # the sinusoidal plot
    p.subplot(gs[0,0])
    p.xticks([0,90,180,270,360])
    p.xlabel(r'$\theta_{0}$ ' + '[deg]', fontsize = 10)
    p.ylabel('$V_{\\rm{los}} $ [km s$^{-1}$]', fontsize = 10)
    p.errorbar(rad2deg(GCpa),
               GCvel,
               yerr = GCvel_err,
               fmt = 'o',
               ecolor = 'black',
               color = 'black',
               ms = 3)
    p.plot([0,360], [0,0], color = 'black', lw = 0.3, ls = ':')
    # showing the rotation
    xvalues = linspace(0., 360., 3600.)
    yvalues = midAmp[0] * sin(deg2rad(xvalues) - midAxi[0])
    p.plot(xvalues, yvalues, color = 'black', lw = 1)

    # this is the pdf for the amplitude
    p.subplot(gs[0,1])
    p.xlabel('$A$ [km s$^{-1}$]', fontsize = 10)
    p.ylabel('Posterior probability', fontsize = 10)
    p.plot(rangeAmp, postAmp, lw =2, color  = 'black')
    # plotting the mean value
    p.plot([midAmp[0], midAmp[0]],[0, midAmp[1]],
            lw = 0.9 ,
            color = 'black',
            ls = ':')
    # plotting the confidence limits
    p.plot([lowAmp[0], lowAmp[0]],[0, lowAmp[1]], lw = 0.7 , color = 'blue')
    p.plot([highAmp[0],highAmp[0]],[0,highAmp[1]], lw = 0.7 ,color = 'blue')

    # this is the pdf for the rotation axis
    p.subplot(gs[1,0])
    p.xlabel(r'$\theta_{0}$ ' + '[deg]', fontsize = 10)
    p.ylabel('Posterior probability', fontsize = 10)
    p.xticks([0,90,180,270,360])
    p.plot(rad2deg(rangeAxi),postAxi,lw = 2, color = 'black')
    # plotting the peak value
    p.plot([rad2deg(midAxi[0]), rad2deg(midAxi[0])], [0, midAxi[1]],
            lw = 0.9,
            color = 'black',
            ls = ':')
    # plotting the confidence limits
    p.plot([(rad2deg(lowAxi[0])), (rad2deg(lowAxi[0]))], [0, lowAxi[1]],
            lw = 0.7,
            color = 'blue')
    p.plot([(rad2deg(highAxi[0])), (rad2deg(highAxi[0]))], [0, highAxi[1]],
            lw = 0.7,
            color = 'blue')

    # this is the pdf for the velocity dispersion
    p.subplot(gs[1,1])
    p.xlabel(r'$\sigma_{0}$ ' +'[km s$^{-1}$]', fontsize = 10)
    p.ylabel('Posterior probability', fontsize = 10)
    p.plot(rangeSig,postSig, lw = 2, color = 'black')
    # plotting the mean value
    p.plot([midSig[0], midSig[0]],[0, midSig[1]],
            lw = 0.9 ,
            color = 'black',
            ls = ':')
    # plotting the confidence limits
    p.plot([lowSig[0], lowSig[0]],[0, lowSig[1]],
            lw = 0.7,
            color = 'blue')
    p.plot([highSig[0], highSig[0]],[0, highSig[1]],
            lw = 0.7,
            color = 'blue')

    p.tight_layout()

    # save Page Two
    plotoutput.savefig()

    # close the document
    plotoutput.close()

    print 'Plot created successfully!'
    print
################################################################################

def WStreamSelector(database, stream_save_filename, threshold = 10**6.,
                    backfeed = True):
    '''
    Selects the streams/progenitors that are massive enough to host GCs and
    stores each of them in a Stream class, in a list.

    arguments:
    database = this is numpy binary file containing all the available data
               for a given Aquarius halo
    stream_save_filename = output dir+filename for the selected stream list
    threshold = above this value, streams are selected. Mass in solar masses
    backfeed = if True, the subroutine returns the selected list of streams,
               if False, the list of streams is only written to the disk.


    Version: 0.1
    '''

    print 'Selecting streams more massive than ', '%e' % threshold, \
          ' solar masses...'
    print

    # reading the data.
    fulldata = load(database)

    # The streams can be uniquely identified by their ID. However, their mass
    # is needed to make selections. Given that multiple streams can have the
    # same mass, a zip is made of the name and mass of each stream, in order to
    # make unique IDs for each stream, and thus they can be nicely selected.
    strIDnMass = zip(fulldata[7], fulldata[8])

    # Selecting only the unique combinations of mass and ID, since the data
    # repeats as many times as there are particles in a given stream
    uni_strIDnMass = transpose(unique(strIDnMass))

    # this index selects the mass range of the streams
    ind =  uni_strIDnMass[1] > threshold
    selected_streams =  uni_strIDnMass[:,ind]

    # delete unneeded data, clean up
    del ind

    # This is where the worthy streams are stored.
    stream_list = []
    # the storing process starts here
    for i in range(len(selected_streams[0])):
        # for selecting the stream particles later
        ind = ((fulldata[7] == selected_streams[0,i])
            & (fulldata[8] == selected_streams[1,i]))
        temp_steam = Stream(str_id = selected_streams[0,i],
                            mass = selected_streams[1,i],
                            x = fulldata[0][ind],
                            y = fulldata[1][ind],
                            z = fulldata[2][ind],
                            vel_x = fulldata[3][ind],
                            vel_y = fulldata[4][ind],
                            vel_z = fulldata[5][ind],
                            dm_id = fulldata[6][ind])
        # add the selected stream object to the list of streams
        stream_list.append(temp_steam)

    # saving the list of the selected streams to disk.
    save_stuff(stream_list, stream_save_filename+'.dill')

    print
    print len(stream_list), 'Streams selected, and saved to disk...'
    print 'WStreamSelector completed successfully.'
    print
    if backfeed == True:
        return stream_list
    elif backfeed == False:
        return
    else:
        print '/backfeed/ argument must be set either to True or False.'

################################################################################

def WParticleSelector(input_database, output_database, threshold = 10**6.,
                      backfeed = True):
    '''
    Selects all particles that belong to stream which have total stellar
    mass larger than the threshold mass. The particles are treated individually,
    and the idea of using objects/classes is abandoned at this stage.

    The input database is needed to have the following columns in this order:
    pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, dm_id, str_id, str_mass.


    arguments:
    database = this is numpy binary file containing all the available data
               for a given Aquarius halo
    stream_save_filename = output dir+filename for the selected stream list
    threshold = above this value, streams are selected. Mass in solar masses
    backfeed = if True, the subroutine returns the properties of the particles.


    Version: 0.1
    '''

    print 'Starting WParticleSelector...'
    print 'Selecting particles belonging to streams more massive than',\
    '%e' % threshold, ' solar masses...'
    print

    fulldata = load(input_database)

    # Selecting data according to some condition. In this case, the primary
    # condition is the mass

    # Selecting only particles that belong to streams which have mass higher
    # than the threshold mass

    ind = fulldata[8] > threshold

    # reading in the particular subset of the data
    x = fulldata[0][ind]
    y = fulldata[1][ind]
    z = fulldata[2][ind]
    vel_x = fulldata[3][ind]
    vel_y = fulldata[4][ind]
    vel_z = fulldata[5][ind]
    dm_id = fulldata[6][ind]
    str_id = fulldata[7][ind]
    str_mass = fulldata[8][ind]

    # clean up
    del fulldata

    # saving the selected particles to disk, as a numpy array
    save(output_database, array([x,
                                 y,
                                 z,
                                 vel_x,
                                 vel_y,
                                 vel_z,
                                 dm_id,
                                 str_id,
                                 str_mass]))

    if backfeed == True:
        print 'WParticleSelector completed successfully.'
        print
        return array([x,y,z,vel_x,vel_y,vel_z,dm_id,str_id,str_mass])
    elif backfeed == False:
        print 'WParticleSelector completed successfully.'
        print
        return
    else:
        print '/backfeed/ argument must be set either to True or False.'
################################################################################

def WDataObservation(input_database, transformation_matrix, observer,
                     output_database, backfeed = True):
    '''
    This observes the data. Effectively, the positions of the particles are
    converted to "standard-like" coordinate system, and the observed
    line-of-sight velocity is calculated for them all.

    Other properties, such as position angle and projected radius are also
    calculated.

    arguments:
    input_database = numpy binary array containing the basic 9 properties of
                     of the particles as exported from the original simulation
    observer = array_like, the location of the observer in the 3D space
    transformation matrix, computed with the transformation_matrix function
    output_database = location of the results, stored as a numpy binary file
    backfeed = toggles whether the results are returned or only written to disk

    output: (array_like)
    the x,y standard coordinates;
    the line-of-sight velocities;
    the position angle for the particles (radians);
    the projected radius of the particles;
    the dm_id;
    the str_id;
    the str_mass.
    '''

    print 'Starting WDataObservation...'

    # reading in the data..
    data     = load(input_database)
    x        = data[0]
    y        = data[1]
    z        = data[2]
    vel_x    = data[3]
    vel_y    = data[4]
    vel_z    = data[5]
    dm_id    = data[6]
    str_id   = data[7]
    str_mass = data[8]

    # clean up..
    del data

    # number of particles being processed
    n = len(x)

    # initializing place-holders the new properties to be derived:
    los = zeros(n)
    xpos = zeros(n)
    ypos = zeros(n)

    for i in range(n):
        # calculating the line-of-sight velocities
        los[i] = los_velocity2(array([x[i],y[i],z[i]]),
                               array([vel_x[i], vel_y[i], vel_z[i]]),
                               observer)
        # calculating the standard coordinates
        xpos[i], ypos[i] = coords_transform(transformation_matrix,
                                            array([x[i],y[i],z[i]]))

    # calculating the projected radius of the particles
    Rproj = (xpos**2.0 + ypos**2.0)**0.5
    # calculating the position angle of the particles (output in radians)
    posAng = posAngCal(xpos,ypos)

    # saving the observed particles to disk
    save(output_database, array([xpos,
                                 ypos,
                                 los,
                                 posAng,
                                 Rproj,
                                 dm_id,
                                 str_id,
                                 str_mass]))

    if backfeed == True:
        print 'WDataObservation completed successfully.'
        print
        return array([xpos, ypos, los, posAng, Rproj, dm_id, str_id,str_mass])
    elif backfeed == False:
        print 'WParticleSelector completed successfully.'
        print
        return
    else:
        print '/backfeed/ argument must be set either to True or False.'
################################################################################

def WGlobularSelector(input_database, output_database, backfeed = False):
    '''
    Here the particular GCs are selected. Each eligible stream is chosen to
    donate a number of GCs depending on its mass, and then the clusters are
    randomly chosen. The GCs are selected in such a way that their radial
    profile matches the radial profile of the M31 halo GCs.

    arguments
    input_database: this is numpy binary file containing the observed data
                    for all particles belonging to streams that are accepted
                    in the analysis

    output
    output_database: a numpy binary file saved to disk, containing the observed 
                     data for the selected GCs


    Version: 0.1
    '''

    print 'Starting WGlobularSelector...'
    print

    # the database
    data = load(input_database)

    # select only those particles that have Rproj > 30 kpc, as the idea is to
    # sample the halo of the Halo :)
    Rproj = data[4]

    # select particles in the appropriate range
    ind   = (Rproj >= 30.) & (Rproj < 150.)

    # clean up
    del Rproj

    # selecting the data
    x        = data[0][ind]
    y        = data[1][ind]
    velocity = data[2][ind]
    posAngle = data[3][ind]
    Rproj    = data[4][ind]
    dm_id    = data[5][ind]
    str_id   = data[6][ind]
    str_mass = data[7][ind]

    # clean up
    del data, ind

    # selecting the unique streams, but also keeping knowing the mass of
    # that stream
    str_id_mass     = zip(str_id, str_mass)
    uni_str_id_mass = transpose(array(list(set(str_id_mass))))
    uni_str         = uni_str_id_mass[0]
    uni_mass        = uni_str_id_mass[1]

    # Here i need to flag the streams that are worthy of clusters
    # being drawn from them
    flag = ones(len(uni_str))
    for i,stream in enumerate(uni_str):
        if ((max(Rproj[str_id == stream]) - min(Rproj[str_id == stream]) < 20)):
            flag[i] = 0
        else:
            pass

    # selecting only the streams that donate clusters only
    uni_str  = uni_str[flag  == 1]
    uni_mass = uni_mass[flag == 1]

    # clean up
    del str_id_mass, uni_str_id_mass

    # determining how much clusters each stream will donate, and how many clusters
    # are to be put in each of the pre-determined radial bins
    # GCQuota, BinQuota = GCbinner(uni_str, uni_mass)
    GCQuota, BinQuota = GCbinner2(input_database)

    # # Remnant from the testing phase, prints stuff,
    # might be useful at some point..
    # for i, stream in enumerate(uni_str):
    #     print   '%-10s %-13i %-10s %-10.1f %-10s %-10.1f %-10s %-1.1e' % \
    #             ('str id: ',
    #             stream,
    #             'rad extent: ',
    #             max(Rproj[str_id == stream]) - min(Rproj[str_id == stream]),
    #             'ang extent: ',
    #             max(posAngle[str_id==stream]) - min(posAngle[str_id==stream]),
    #             'str mass: ',
    #             uni_mass[i],)
    # sys.exit()

    # for i in range(30,150,20):
    #     mi = i
    #     ma = mi + 20
    #     print '*****'
    #     print mi, ma
    #     print
    #     for i,stream in enumerate(uni_str):
    #         print stream, len(dm_id[(str_id==stream)&(Rproj>=mi)&(Rproj<ma)])
    #     print
    # sys.exit()

    ## Here starts the master actual GC selector

    # This is where the properties of the selected GCs are to be stored
    x_sel = []
    y_sel = []
    vel_sel = []
    rad_sel = []
    pos_sel = []
    dm_sel = []
    str_sel = []
    mass_sel = []

    # copy the bin quota to create a counter, so the original is preserved
    BinQuotaCounter = BinQuota.copy()

    # needed for assignment of values to the sub-arrays, via Pepes trick
    len_GCQuota    = arange(len(GCQuota))

    # this checks if the streams have satisfied their quota or not
    ind_quota = GCQuota > 0

    # 1st loop, loop observedr all the bins, so the clusters can be chosen per bin
    binmin = 30 # the innermost bin edge
    for i in range(len(BinQuota)):
        # setting up the bin edges and the centre
        binmax = binmin + 20
        binmid = (binmin + binmax) * 0.5

        # need to check if some streams disappear in the outer part so they
        # have to exhaust their quota faster
        ind_future  = []   # switch for the future
        ind_present = []   # switch for the present
        for j in uni_str:
            ind_temp_future = Rproj[str_id == j] >binmax
            ind_future.append(ind_temp_future.any())
            ind_temp_present = ((Rproj[str_id == j] >=binmin) &
                                (Rproj[str_id == j] <binmax))
            ind_present.append(ind_temp_present.any())

        # converting the selection indices into arrays for better handling
        ind_future  = array(ind_future)
        ind_present = array(ind_present)

        # only taking streams that are active
        ind_future[ind_quota == False]   = False
        ind_present[ind_quota == False]  = False

        # set a counter to know how many clusters to draw to fill each bin
        while BinQuotaCounter[i] > 0:

            # this is the master if statement that selects which streams are
            # eligible for donating a cluster in this particular while iteration
            if ((ind_quota[(ind_future == False)].any() == True)):
                # master index, includes the available streams to draw GC from
                ind_master = (ind_future == False) & ind_quota

            elif (ind_present == False).all() == True:
                print 'Bin devoid of streams reached...'
                print 'Continue command executed'
                BinQuotaCounter[i] -= 1

            else:
                # selecting the appropriate streams (active and available quota)
                ind_master = ind_present & ind_quota

            # Sometimes for whatever reason, the ind_master is not created.
            # This happens rarely but it still needs to be dealth with. 
            # When this happens, a random noise is added, in which the cluster 
            # this itteration is not being chosen.

            if 'ind_master' in locals():
                pass
            else:
                break

            # how many streams will not show up in the outer bins
            l = len(ind_future[ind_master])

            # If the code gets stuck in point where there are no more clusters
	    # to be drawn from in this current bin, but the quota is not 
            # satisfied, this is a quick and dirty method of fixing it. 
            # The code just continues, and at most only up to 2 clusters are 
            # are missing. All in all, this case is hardly ever reached.
            if l == 0:
                break

            # the dice basically decides from which stream a cluster is chosen
            dice = rd.randint(0,l-1)

            # this stream donates a GC so reduce its quota
            GCQuota[len_GCQuota[ind_master][dice]] -= 1

            # Here the algorithm for choosing an actual cluster are set
            # For convenience, select which stream the picking is done from
            temp_str = uni_str[len_GCQuota[ind_master][dice]]
            # check how many particles exist in that stream, but also in the
            # appropriate radial range (in the current bin)
            idx_particles = (str_id==temp_str)&(Rproj>=binmin)&(Rproj< binmax)
            temp_num_part = len(dm_id[idx_particles])

            # The algorithm is not perfect, and sometimes the quota of a stream
            # is not exhausted in time. In that case, those streams just
            # deposit their remaining clusters anywhere, adding random noise
            # to the profile, which is fine, since it typically happens for
            # <3 clusters.
            if temp_num_part == 0:
                idx_particles = (str_id == temp_str )
                temp_num_part = len(dm_id[idx_particles])
                # print 'SPECIAL CASE REACHED'

            # rolling the dice, which particle in this stream to be chosen
            # toss is a synonym for dice, so I avoid repeating the same var
            toss = rd.randint(0,temp_num_part - 1)
            # temporarily choose a particle to be a GC
            temp_dm_id = dm_id[idx_particles][toss]
            # now check if this particular particle has already been chosen.
            # if it has been, choose again until you get a unique new selection
            ticket_master = 0 # This will prevent the code to get stuck if there
                              # are not enough particles in a particular bin...
            bouncer = False   # If the code does get stuck, then exit the
                              #relevan loops
            while ((temp_dm_id in dm_sel)==True and (bouncer==False)==True):
                ticket_master += 1
                toss = rd.randint(0,temp_num_part - 1)
                temp_dm_id = dm_id[idx_particles][toss]
                if ticket_master >31:
                    bouncer = True
                    # print 'Too many repetition in particle choosing...'
                    # print 'Bouncer is kicking you outside the loop...'
                    bouncer = True

            if bouncer == False:

                # now that a unique choice of a particle has been made, store its
                # relevant properties
                x_sel.append(x[dm_id == temp_dm_id][0])
                y_sel.append(y[dm_id == temp_dm_id][0])
                vel_sel.append(velocity[dm_id == temp_dm_id][0])
                rad_sel.append(Rproj[dm_id == temp_dm_id][0])
                pos_sel.append(posAngle[dm_id == temp_dm_id][0])
                dm_sel.append(dm_id[dm_id == temp_dm_id][0])
                str_sel.append(str_id[dm_id == temp_dm_id][0])
                mass_sel.append(str_mass[dm_id == temp_dm_id][0])

            else:
                pass

            # A cluster has been chosen, so lower the current bin quota
            BinQuotaCounter[i] -= 1

            # clean up
            del ind_master, dice, toss, idx_particles, l, temp_num_part
            del temp_dm_id, temp_str

            # checking the quota of the streams
            ind_quota = GCQuota > 0

        # cleanup
        del ind_future, ind_present

        # go to the next bin, basically.
        binmin = binmax

    # converting the lists of the selected GC to numpy arrays
    x_sel = array(x_sel)
    y_sel = array(y_sel)
    vel_sel = array(vel_sel)
    rad_sel = array(rad_sel)
    pos_sel = array(pos_sel)
    dm_sel = array(dm_sel)
    str_sel = array(str_sel)
    mass_sel = array(mass_sel)

    # The clusters have been chosen now, it is time to save the data to disk..
    save(output_database, array([x_sel,
                                 y_sel,
                                 vel_sel,
                                 rad_sel,
                                 pos_sel,
                                 dm_id,
                                 str_sel,
                                 mass_sel]))

    if backfeed == True:
        print 'Returning the data for the selected GCs..'
        return array([x_sel,
                      y_sel,
                      vel_sel,
                      rad_sel,
                      pos_sel,
                      dm_id,
                      str_sel,
                      mass_sel])

        print 'WGlobularSelector completed successfully.'
        print

    elif backfeed == False:
        print 'WGlobularSelector completed successfully'
        print

    else:
        print 'Please set /backfeed/ to either True or False to avoid errors.'




################################################################################

def WGalaxyCentring(database3D, nbins = 500):

    '''
    This module finds the most dense centre of particles in the current halo, 
    and puts the centre of the halo/universe coordinate system at that point.
    
    The algorithm used here was largely provided by Sandy.

    Arguments:
    database3D = numpy binary array containing the basic 9 properties of the
                 particles as exported from the original simulation
    nbins      = the bins parameter for the histogram stacking of things

    Returns:
    The input database3D is overwritten, with the coordinates x,y,z shifted 
    to account for the re-centring. If an error is made, this can be remedied
    by running WParticleSelector again, which will give the original database3D
    back.
    '''
    
    print
    print 'Re-centring the stellar halo...'

    data     = load(database3D)
    x        = data[0]
    y        = data[1]
    z        = data[2]
    vel_x    = data[3]
    vel_y    = data[4]
    vel_z    = data[5]
    dm_id    = data[6]
    str_id   = data[7]
    str_mass = data[8]

    # clean up
    del data

    # binning everything up
    density3d, edges3d = histogramdd(column_stack((x,y,z)), bins=nbins)
    # location of the maximum, in terms of the bins
    x_max_bin, y_max_bin, z_max_bin = unravel_index(argmax(density3d), 
                                                    density3d.shape)

    # reading of the right bin edge value known it's index
    x_max = edges3d[0][x_max_bin]
    y_max = edges3d[1][y_max_bin]
    z_max = edges3d[2][z_max_bin]

    # now the shift is know, so shift the stars...
    x_shift = x - x_max
    y_shift = y - y_max
    z_shift = z - z_max

    
    # Saving the selected particles to disk, as a numpy array
    # Recall that with this, the input database is overwritten
    save(database3D, array([x_shift,
                            y_shift,
                            z_shift,
                            vel_x,
                            vel_y,
                            vel_z,
                            dm_id,
                            str_id,
                            str_mass]))

    print
    print 'Re-centring completed!'
    print


################################################################################

def WAnalysis(datadir, datafile, halo, sigma = 0.8,
              theta = arange(0, 180, 45), phi = arange(0, 180, 45)):
    
    '''
    This is an adaptation of the stand-alone WateryAnalysis code. 
    Here it is implemented so it can be part of the main WateryFlow code.

    (1) created is a multiplot with histograms showing the kinematic ratio 
    (amplitude/velocity_dispersion) for all instances of each perspective 
    the halo is observed for;

    (2) created is a single histogram using all data, in a way it is the 
    average kinematics of the halo as seen from all perspectives;

    (3) created is a latex-friendly table containing the kinematic averaged 
    kinematic information as seen from each perspective of the observed halo;

    (4) Printed to screen is the perspective for which the maximum rotation is 
    observed, and what the kinematic ratio of that perspective is;

    (4.1) Same as (4) but for a selected subsample of the data, meaning that 
    only systems that are similar to that of M31 are kept for the analysis;

    (5) Printed is the mean kinematic ratio of all perspectives, basically 
    the mean of the histogram created in (2);

    (5.1) Same as in (5) but for a selected subsample of the data, meaning that 
    only systems that are similar to that of M31 are kept for the analysis.

    arguments:
    datafile = the aggregate result of WateryFlow prior to any analysis 
               (typically saved to disk as 'WateryFlowRes.npy')
    datadir  = the path to there the datafile is located on disk
    halo     = which halo the analysis is done one, used for saving stuff 
    sigma    = to which significance should the quadrants be taken as deviating
               from the mean in terms of the number of clusters they contain.

    '''

    # loading up the data
    data = load(datadir+datafile)


    ## part (1) 

    print 'Creating the histogram multiplot...'

    # Defining the master figure
    p.figure(figsize=(8,11), dpi=120, facecolor='w', edgecolor='k')
    # defining the grid where the subplots are gonna be placed
    gs = p.GridSpec(5,4)

    # these control the locating of the subplots
    ii = 0
    jj = 0

    # the master loop begins
    for i in theta:
        for j in phi:
            if (i == 0 or i == 180) & (j != 0):
                pass
            else:
                # selecting and reading in the relevant portion of the data
                ind = (data[:,0] == i) & (data[:,1] == j)
                shortdata = data[ind]

                amp = shortdata[:,3]
                axi = shortdata[:,4]
                sig = shortdata[:,5]
                q1  = shortdata[:,6]
                q2  = shortdata[:,7]
                q3  = shortdata[:,8]
                q4  = shortdata[:,9]

                # calculating the main kinematic ratio, between the maximum
                # amplitude and the velocity dispersion
                kinrat = amp/sig

                # the mean of the kinrat in the full sample
                meanKR = bs.location(kinrat)[0]

                # now choosing which systems are most similar to M31
                idx = [True] * len(q1)
                idx = array(idx)

                # constructing the index showing which systems are akin to M31
                for k in range(len(q1)):
                    ta = azym_sim_test_mean_2(q1[k],q2[k],q3[k],q4[k],sig=sigma)
                    
                    if ((len(ta) == 2) and 
                        (any([tmp < 0 for tmp in ta])) and 
                        (any([tmp > 0 for tmp in ta]))):
                        pass
                    else:
                        idx[k] = False

                # the mean of the kinrat in the selected sample
                if len(amp[idx]) >= 10 :
                    meanKRsub = bs.location(kinrat[idx])[0]
                else:
                    meanKRsub = NaN

                # determining the location of the current subplot
                if jj == 4:
                    jj = 0
                    ii += 1
                    # print ii, jj
                    p.subplot(gs[ii,jj])
                    jj += 1
                else:
                    # print ii, jj
                    p.subplot(gs[ii,jj])
                    jj += 1

                # determining the bins of the histogram(s)
                bins = [0,0.2,0.4,0.6,0.8,1,1.2,1.4,1.6]
                # custom y-tics, cause the AutoLocator messes up sometimes..
                p.xticks([0,10,20,30,40,50,60,70])
                # labels of the subplots
                p.xlabel(r'$A/\sigma$', fontsize  = 6)
                p.ylabel('N', fontsize  = 6)
                # histogram, only done to determine the ylim of the subplots
                hh, ee = histogram(kinrat, bins = bins)
                p.ylim([0,int(round(max(hh)/10.0)*10.0) + 10])
                p.xlim([0,1.6])
                # the master histograms
                p.hist(kinrat,
                       bins = bins,
                       histtype = 'stepfilled',
                       color = 'blue',
                       ec = 'None',
                       lw = 2,
                       label = 'System ' + str(int(i)) + ' ' + str(int(j)))
                # the histogram for the data more similar to M31.
                # check first how many points are left, if any.
                if len(kinrat[idx]) == 0:
                    pass
                else:
                    p.hist(kinrat[idx],
                           bins = bins,
                           histtype = 'step',
                           color = 'black',
                           lw = 1.5,
                           ls = 'dashed')
                # this is only done to add a random text in the legend :)
                p.errorbar([],
                           [],
                           lw = 1.5,
                           ls = 'dashed',
                           c = 'white',
                           mfc = 'white',
                           mec = 'white',
                           label = str(len(amp[idx])) + '/100')
                # the average line of the full sample
                p.plot([meanKR,meanKR],
                       [0,int(round(max(hh)/10.0)*10.0)+10],
                       lw = 0.5,
                       ls = '-',
                       color = 'black')
                # the average line of the selected sample
                p.plot([meanKRsub,meanKRsub],
                       [0,int(round(max(hh)/10.0)*10.0)+10],
                       lw = 0.5,
                       ls = ':',
                       color = 'black')
                # legend
                p.legend(loc = 1, frameon=False, prop={'size':5})
                # configuring the ticks with my custom rutine
                minorticks(ftsize=4, maw=1.0,mal=2.0,miw=0.5,mil=1.0)

                # clean up
                del amp, axi, sig, q1, q2, q3, q4, shortdata, meanKRsub, meanKR

    # formatting cause there are lots of subplots
    p.tight_layout()
    # saving the plot to disk
    p.savefig(datadir +'plots/histfig' + halo + '.eps', dpi=120)
    p.clf()

    print 'The histogram multiplot is created successfully!'
    print

    ## part(2)

    print 'Creating an average histogram from all perspectives...'

    # defining the figure right at the top
    p.figure(figsize = (6,4), dpi = 120, facecolor = 'w', edgecolor = 'k')

    # reading in the full data, regardless of perspective
    amp = data[:,3]
    axi = data[:,4]
    sig = data[:,5]
    q1  = data[:,6]
    q2  = data[:,7]
    q3  = data[:,8]
    q4  = data[:,9]

    # checking which systems are a bit more similar to M31
    idx = [True] * len(q1)
    idx = array(idx)

    for k in range(len(q1)):
        ta = azym_sim_test_mean_2(q1[k],q2[k],q3[k],q4[k],sig=sigma)
        
        if ((len(ta) == 2) and 
            (any([tmp < 0 for tmp in ta])) and 
            (any([tmp > 0 for tmp in ta]))):
            pass
        else:
            idx[k] = False

    # the mean of the full sample
    kinrat = amp/sig
    meanKR = bs.location(kinrat)[0]

    # the mean of the selected sample
    meanKRsub = bs.location(kinrat[idx])[0]

    # determining the bins of the histogram
    bins = [0,0.2,0.4,0.6,0.8,1,1.2,1.4,1.6]
    # custom y-tics, cause the AutoLocator messes up sometimes...
    p.xticks([0,10,20,30,40,50,60,70])
    # labels of the subplots
    p.xlabel(r'$A/\sigma$', fontsize  = 10)
    p.ylabel('N', fontsize  = 10)
    # histogram, only done to determine the ylim of the subplots
    hh, ee = histogram(kinrat, bins = bins)
    p.ylim([0,int(round(max(hh)/10.0)*10.0) + 10])
    p.xlim([0,1.6])
    # the master histograms
    p.hist(kinrat,
           bins = bins,
           histtype = 'stepfilled',
           color = 'blue',
           ec = 'None',
           lw = 2,
           label = 'average kinematics for halo ' + halo)
    # the histogram for the data more similar to M31.
    # check first how many points are left, if any.
    if len(kinrat[idx]) == 0:
        pass
    else:
        p.hist(kinrat[idx],
               bins = bins,
               histtype = 'step',
               color = 'black',
               lw = 1.5,
               ls = 'dashed')
    # this is only done to add a random text in the legend :)
    p.errorbar([],
               [],
               lw = 1.5,
               ls = 'dashed',
               c = 'white',
               mfc = 'white',
               mec = 'white',
               label = str(len(amp[idx])) + '/1300')
    # the average line of the full sample
    p.plot([meanKR,meanKR],
           [0,int(round(max(hh)/10.0)*10.0)+10],
           lw = 0.7,
           ls = '-',
           color = 'black')
    # the average line of the selected sample
    p.plot([meanKRsub,meanKRsub],
           [0,int(round(max(hh)/10.0)*10.0)+10],
           lw = 0.7,
           ls = ':',
           color = 'black')
    # legend
    p.legend(loc = 1, frameon=False, prop={'size':8})
    # configuring the ticks with my custom rutine
    minorticks(ftsize=8, maw=1.0,mal=2.5,miw=0.5,mil=1.5)

    # saving the plot to disk
    p.savefig(datadir + 'plots/avghist' + halo + '.eps', dpi = 120)

    print 'The average histogram is created successfully!'
    print

    print 'Mean kinematic ratio for the full sample: ', \
           bs.location(kinrat)[0]
    print 'Mean kinematic ratio for the sele sample: ', \
           bs.location(kinrat[idx])[0]
    print

    # clean up
    del amp, axi, sig, q1,q2,q3,q4, meanKRsub, meanKR, kinrat

    ## part(3)

    print 'Creating results table for all perspectives of the halo...'

    # where the output text file is to be saved
    textdir = datadir + 'latexStuff/'

    # opening the text file
    fout = open(textdir + '100table' + halo, 'w')

    # printing the header
    print >>fout, '%-15s %5s %-10s %5s %16s %5s %5s' % ('Perspective',
                                                        ' $ ',
                                                        'Amp/Sig',
                                                        ' $ ',
                                                        'N*   $ ',
                                                        'Amp/Sig*',
                                                        ' $ ')
    print >>fout, ''

    # the master loop begins
    for i in theta:
        for j in phi:
            if (i == 0 or i == 180) & (j != 0):
                pass
            else:
                # selecting and reading in the relevant portion of the data
                ind = (data[:,0] == i) & (data[:,1] == j)
                shortdata = data[ind]

                amp = shortdata[:,3]
                axi = shortdata[:,4]
                sig = shortdata[:,5]
                q1  = shortdata[:,6]
                q2  = shortdata[:,7]
                q3  = shortdata[:,8]
                q4  = shortdata[:,9]

                kinrat = bs.location(amp/sig)[0]
                kinraterr = bs.scale(amp/sig)[0]

                # selecting systems which are most similar to M31
                idx = [True] * len(q1)
                idx = array(idx)

                # constructing the index point to systems similar to M31
                for k in range(len(q1)):
                    ta = azym_sim_test_mean_2(q1[k],q2[k],q3[k],q4[k],sig=sigma)
                    
                    if ((len(ta) == 2) and 
                        (any([tmp < 0 for tmp in ta])) and 
                        (any([tmp > 0 for tmp in ta]))):
                        pass
                    else:
                        idx[k] = False

                # check if there are sufficient number of similar clusters left
                if len(amp[idx]) >= 10 :
                    kinratsub = bs.location(amp[idx]/sig[idx])[0]
                    kinratsuberr = bs.scale(amp[idx]/sig[idx])[0]
                else:
                    kinratsub = NaN
                    kinratsuberr = NaN

                print >>fout,'%-5s %-5s %-5s %-5s %-12s %9s %-10s %-12s %9s' % (
                            str(i),
                            ' $ ',
                            str(j),
                            ' $ ',
                            str('%2.2f' % (kinrat)) + ' \pm ' + \
                            str('%1.2f' %(kinraterr)),
                            ' $ ',
                            str(len(amp[idx])) + '   $',
                            str('%2.2f' % (kinratsub)) + ' \pm ' + \
                            str('%1.2f' %(kinratsuberr)),
                            ' $ ')

                #clean up
                del kinratsub, kinratsuberr, kinrat, kinraterr
    fout.close()

    print 'Latex-friendly table successfully created.'
    print

    ## part(4)

    kinratres    = zeros(13)
    kinratsubres = zeros(13)
    finAxi       = zeros(13)
    finAxisub    = zeros(13)
    coords       = zeros((13,2))
    counter      = 0

    # the master loop begins
    for i in theta:
        for j in phi:
            if (i == 0 or i == 180) & (j != 0):
                pass
            else:
                # selecting and reading in the relevant portion of the data
                coords[counter,0] = (i)
                coords[counter,1] = (j)

                ind = (data[:,0] == i) & (data[:,1] == j)
                shortdata = data[ind]

                amp = shortdata[:,3]
                axi = shortdata[:,4]
                sig = shortdata[:,5]
                q1  = shortdata[:,6]
                q2  = shortdata[:,7]
                q3  = shortdata[:,8]
                q4  = shortdata[:,9]

                # checking which systems are a bit more similar to M31
                idx = [True] * len(q1)
                idx = array(idx)

                # constructing the index for systems similar to M31
                for k in range(len(q1)):
                    ta = azym_sim_test_mean(q1[k],q2[k],q3[k],q4[k], sig = 1.0)

                    if ta[0] == 0:
                        idx[k] = False
                    else:
                        idx[k] = True

                kinratres[counter] = bs.location(amp/sig)[0]
                finAxi[counter]    = bs.location(axi)[0]
                # check if there are sufficient number of similar clusters left
                if len(amp[idx]) >= 10 :
                    kinratsubres[counter] = bs.location(amp[idx]/sig[idx])[0]
                    finAxisub[counter]    = bs.location(axi[idx])[0]
                else:
                    kinratsubres[counter] = NaN
                    finAxisub[counter]    = NaN

                counter += 1

    print 'Results for the full sample:'
    print 'Maximum kinetic ratio: ', max(kinratres)
    print 'Observed for perspective: ', coords[nanargmax(kinratres)]
    print

    print 'Results for the selected sample'
    print 'Maximum kinetic ratio: ', max(kinratsubres)
    print 'Observed for perspective: ', coords[nanargmax(kinratsubres)]
    print 

    # clean up
    del amp, axi, sig, q1, q2, q3, q4, kinratsubres, kinratres, counter, 
    del finAxi, idx, ta, finAxisub

    ## part(5)

    kinrat  = zeros(13)
    coords  = zeros((13,2))
    counter = 0


    # the master loop begins
    for i in theta:
        for j in phi:
            if (i == 0 or i == 180) & (j != 0):
                pass
            else:
                # selecting and reading in the relevant portion of the data
                coords[counter,0] = (i)
                coords[counter,1] = (j)

                ind = (data[:,0] == i) & (data[:,1] == j)
                shortdata = data[ind]

                amp = shortdata[:,3]
                axi = shortdata[:,4]
                sig = shortdata[:,5]

                kinrat[counter] = bs.location(amp/sig)[0]

                counter += 1

    # finding which perspective sees maximum rotation
    i = coords[nanargmax(kinrat)][0]
    j = coords[nanargmax(kinrat)][1]

    # clean up
    del shortdata, ind, amp, axi, sig, kinrat, coords

    # now select those instances one again (wow this is super sub optimal..)
    ind       = (data[:,0] == i) & (data[:,1] == j)
    shortdata = data[ind]
    amp       = shortdata[:,3]
    sig       = shortdata[:,5]

    # finding the instance  featuring minimum and maximum rotation..
    kinrat = amp/ sig

    print 
    print 'Maximum rotation observed is ', max(kinrat), \
    ' and is seen in instance ' \
    't'+str(int(i))+'p'+str(int(j))+'run'+str(nanargmax(kinrat))+'.pdf'
    print 'Minimum rotation observed is ', min(kinrat), \
    ' and is seen in instance ' \
    't'+str(int(i))+'p'+str(int(j))+'run'+str(nanargmin(kinrat))+'.pdf'
    print 'A system very similar to M31 has a kinematic ration of ', \
    kinrat[nanargmin([abs(tmp - 0.68) for tmp in kinrat])], \
    ' and it is seen in instance', \
    't'+str(int(i))+'p'+str(int(j))+'run'+str(nanargmin([abs(tmp - 0.68) for tmp in kinrat]))+'.pdf'
    print


################################################################################