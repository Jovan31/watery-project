import sys
import datetime
from scipy import stats
from matplotlib import cm
from WateryModules import *
from astropy.stats import funcs as ast_funcs

'''
Creating a global velocity map of an Aquarius stellar halo.
The point is to assess if there is a global rotation of the halo, given a 
specific perspective, as determined by the "Globular clusters" in the 
WateryFlow clode.

'''

# functions
def customclippedmean(a, sigma = 3, iters = 5):
    a_clipped = ast_funcs.sigma_clip(a, sig = sigma, iters = iters)
    return mean(a_clipped)


# This is just for measuring the time it takes to execute this code
datum = datetime.datetime.now()

# choosing a halo
halo = 'C'
print 'Aquarius halo chosen: ', halo

# location of the data
database = '/data/users/jovan/Aquarius/PresentDay/halo-' + halo + '/'

# # Selecting the particles which might contain GCs 
# WParticleSelector(database + 'full-data-binary.npy', 
#                   database + 'Particle.Data.npy',backfeed = False)

# # Centring the particles, so that the origin is at the most dense point
# # This option can be commented out if not needed
# WGalaxyCentring(database + 'Particle.Data.npy')

# # placing the observer
# observer = array([1000., 0., 0.])

# # The transformation matrix, i.e. how the observer perceives the system.
# mtrans = transformation_matrix(observer)


# # Observing the data
# WDataObservation(database + 'Particle.Data.npy',
#                  mtrans, observer,
#                  database + 'Particle.ObservedData.npy',  
#                  backfeed = False)

halodata = load(database + 'Particle.ObservedData.npy')

# take the relevant properties of each particle in this halo
x       = halodata[0]
y       = halodata[1]
vel     = halodata[2]

# clean up
del halodata

# print len(x)
# sys.exit()

# Da figure...
figga = p.figure(figsize = (9,9), dpi =100, facecolor = 'w', edgecolor= 'K')
# p.axes().set_aspect('equal')
p.xlim([150,-150])
p.ylim([-150,150])


nbins = 300
Z, xedges, yedges, binnumber = stats.binned_statistic_2d(x,
                                                         y,
                                                         values = vel,
                                                         statistic = customclippedmean,
                                                         bins = nbins,
                                                         range = ([-150,150],[-150,150]))



# # converting from histogram to scatter point data 
xbin = zeros(nbins*nbins)
ybin = zeros(nbins*nbins)
velbin = zeros(nbins*nbins)
# custom observational error (maybe this should be modified to contain the actual std or something of the sort)
err_velbin = ones(nbins*nbins) * 15.0
# to count if certain field contain nans. If there is a nan, then field is True!
ind = zeros(nbins*nbins)

counter = 0
# this only works if the range has a square shape
for i in range(nbins - 1):
  for j in range(nbins - 1):
    xbin[counter]   = (xedges[i] + xedges[i+1]) * 0.5
    ybin[counter]   = (yedges[j] + yedges[j+1]) * 0.5
    velbin[counter] = Z[i,j]
    ind[counter]    = isnan(Z[i,j])
    counter        += 1

# reformatting the arrays to contain only the fields that have values
xbin       = xbin[ind       == False]
ybin       = ybin[ind       == False]
velbin     = velbin[ind     == False]
err_velbin = err_velbin[ind == False]

# clean up
del ind, x, y, vel

print len(velbin)



# the histogram
p.imshow(Z.T, 
         interpolation = 'none', 
         extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]],
         origin = 'lower', cmap = cm.coolwarm)
# p.colorbar()
# p.show()

# coloured points for the scatter plot
points    = zip(xbin, ybin)
points3   = sorted(zip(velbin,points))

for i,pp in enumerate(points3):
  velbin[i] = pp[0]
  xbin[i]   = pp[1][0]
  ybin[i]   = pp[1][1]

# the scatter plot
p.scatter(xbin,ybin, c=velbin, s = 10, edgecolor = 'black', lw = 0.3, cmap = cm.coolwarm )
p.xlim([150,-150])
p.ylim([-150,150])
p.colorbar()
p.show()

print 'Plotting completed!'