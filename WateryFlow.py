from WateryModules import *
from BayesianKinematics2D import BayKin

'''
Watery Flow.
Here the Watery code will be executed many times in a certain pattern to
get a better sense of the statistics and the results on average, depending
on which particles are selected to be GCs.

Basically repeating the experiment many times from the same and different
perspectives, to get a more robust answer.

Now the code goes through a 'gird' of points of view, in order to observe the
halo from multiple directions.

How the results are saved has now changed. The output is only a single
cPickle file, saved in the relevand halo directory. This is the output (a list),
descibed column by column:
v[0]   = the theta coordinate of the observer
v[1]   = the phi coordinate of the observer
k      = the kth iteration from the given perspective, by default n/100
amp    = the amplitude for the kth run for a given perspective [km/s]
axi    = the axis for the kth run for a given perspective [deg]
dis    = the dispersion for the kth run for a given perspective [km/s]
eigval = the eigenvalues of the Inertia Tensor for a system of GCs
eigvec = the eigenvectors of the Inertia Tensor for a system of GCs
angmom = the angular momentum in 3D for a system of GCs
quad1  = the number of clusters generated in quadrant 1
quad2  = the number of clusters generated in quadrant 2
quad3  = the number of clusters generated in quadrant 3
quad4  = the number of clusters generated in quadrant 4
mvGC   = the mean and stddev of the velocity  of the mock GC system [km/s]
vsys   = the systemic velocity (and stddev) of the central galaxy [km/s]
ee_Amp = the uncertainty estimate of the amplitude [km/s]
ee_Axi = the uncertainty estimate of the rotation axis [deg]
ee_Sig = the uncertainty estimate of the velocity dispersion [km/s]
'''

# This is just for measuring the time it takes to execute the code
datum = datetime.datetime.now()

# choosing a halo
halo = 'C'
print 'Aquarius halo chosen: ', halo

# location of the data
database     = '/data/users/jovan/Aquarius/PresentDay/halo-' + halo + '/'
dataProducts = database + 'dataProducts/'

# Selecting the particles which might contain GCs
WParticleSelector(database + 'full-data-binary.npy',
                  dataProducts + 'Particle.Data.npy')

# Centring the particles, so that the origin is at the most dense point
WGalaxyCentring(dataProducts + 'Particle.Data.npy')

# Scaling the galaxy to Aq-A
WGalaxyScaling(dataProducts + 'Particle.Data.npy',
               dataProducts + 'ScaledParticle.Data.npy',
               halo)

# Defining the network of observers (degrees)
obsnet = WPerspective(principalAxis=True, datadir=database, halo=halo)
rad    = 1000.

# this is where everything is kept; at the end it will be turned to an numpy
# array and saved to disk as a numpy binary file
results = []

# Start of the main phase.. loop through each observer
for i,v in enumerate(obsnet):
  x_obs = int(round(rad*sin(deg2rad(v[0]))*cos(deg2rad(v[1]))))
  y_obs = int(round(rad*sin(deg2rad(v[0]))*sin(deg2rad(v[1]))))
  z_obs = int(round(rad*cos(deg2rad(v[0]))))

  # Observing the particles, given a specific location of the observer
  observer = array([x_obs, y_obs, z_obs])

  # The transformation matrix, i.e. how the observer sees the system
  mtrans = transformation_matrix(observer)
  # save the transformation matrix, so the same can be used for any further
  # transformations from this perspective
  # tmd = transformation matrix directory
  tmd = dataProducts + 'transMat/'
  # check if that directory exists, if not it is created
  if not os.path.exists(tmd):
    os.makedirs(tmd)
  # tmn = transformation matrix name
  tmn = 'TM'+'t'+str(int(v[0]))+'p'+str(int(v[1]))+'.npy'
  # save the transformation maxtrix to disk
  save(tmd+tmn,mtrans)

  # Observing the data
  WDataObservation(dataProducts + 'ScaledParticle.Data.npy',
                   mtrans, observer,
                   dataProducts + 'Particle.ObservedData.npy',
                   backfeed = False)

  # Determining the systemic velocity of the halo from this point of view
  galvel   = load(dataProducts + 'Particle.ObservedData.npy')[2]
  galRproj = load(dataProducts + 'Particle.ObservedData.npy')[4]
  vsys     = array([mean(galvel[galRproj < 30.0]),std(galvel[galvel < 30.0])])

  # Here is where WateryFlow forks from Watery..
  # Clusters will be drawn many times and the analysis will be
  # repeated each sample again and again
  flow_control = 100 # number of repetition for this perspective

  # clean up
  del galvel, galRproj

  # Observe a GCS k times for this perspective
  for k in range(flow_control):
    print
    print 'OBSERVER: ', observer, '   ITERATION :', k
    print

    # The generated GC system will be saved to disk in case futher analysis
    # is needed.
    # the system output directory (sod)
    sod = dataProducts + 'systems/'
    # checking if the directory exists, if not it is created
    if not os.path.exists(sod):
      os.makedirs(sod)
    # the son = 'system output name'
    son = 't'+str(int(v[0]))+\
           'p'+str(int(v[1]))+\
           'run'+str(k)+'.npy'

    # Selecting the actual GCs from the observed data
    WGlobularSelector(dataProducts + 'Particle.ObservedData.npy', sod+son)

    # Load the GC observed data
    GCdata = load(sod+son)

    x        = GCdata[0]
    y        = GCdata[1]
    vel      = GCdata[2]
    posAng   = GCdata[4]
    dm_id    = GCdata[5]

    # clean up
    del GCdata

    # adding "observational" errors
    err_vel = ones(len(vel)) * 15.0

    # Kinematic analysis of the GC sample

    # the parameter space to be explored
    # Amplitude space
    rangeAmp = arange(0.0, 250.0, 3.0)
    # Velocity dispersion space
    rangeSig = arange(0.0, 180.0, 3.0)
    # Rotation axis space
    rangeAxi = arange(0.0, 2.0*pi+0.10, 0.15)

    # Bayesian analysis
    postAmp, postAxi, postSig = BayKin(vel = vel,
                                       err_vel = err_vel,
                                       posAng = posAng,
                                       vsys = vsys[0],
                                       rangeAmp = rangeAmp,
                                       rangeAxi = rangeAxi,
                                       rangeSig = rangeSig)

    # Results of the kinematic analysis, stored for review
    finAmp, finAxi, finSig = WResults(postAmp,
                                      rangeAmp,
                                      postAxi,
                                      rangeAxi,
                                      postSig,
                                      rangeSig,
                                      statistic = 'peak',
                                      backfeed = True)

    # Error estimates of the parameters
    ee_Amp, ee_Axi, ee_Sig = WResultsEE(postAmp,
                                        rangeAmp,
                                        postAxi,
                                        rangeAxi,
                                        postSig,
                                        rangeSig,
                                        statistic = 'peak')

    # Plotting the results of this instance
    # The plot output directory (pod)
    # first check if the correct directory exists (plot output dir)
    pod = database + 'plots/instances/'
    if not os.path.exists(pod):
      os.makedirs(pod)
    # the 'plot output name'
    pon = 't'+str(int(v[0]))+\
          'p'+str(int(v[1]))+\
          'run'+str(k)+'.pdf'

    # To avoid the matplotlib memory issue, create a fork of the progarm
    # to execute separately
    pid = os.fork()
    if pid == 0:
      print 'Forking...'
      # Create the diagnostic plot
      WHaloPlot(x, y,
                vel, err_vel,
                posAng, vsys[0],
                postAmp, rangeAmp,
                postAxi, rangeAxi,
                postSig, rangeSig,
                halo, mtrans,
                oname = pod+pon)
      os._exit(0)
    else:
      os.waitpid(pid, 0)

    # Calculate the Inertia tensor eigenvalues and eigenvectors
    # Also calulate the angular momentum (magnitude and spin)
    eigval, eigvec, angmom = WInertia(dm_id, 
                                      dataProducts+
                                      'ScaledParticle.Data.npy')

    # Checking the distribution of position angles of the GCs,
    # in other works the quadrants test
    quad1, quad2, quad3, quad4 = quad_clust_counter(posAng)

    # For posterity purposes, also save the mean velocity and velocity 
    # dispersion of the GCs in each mock system (mvGC)
    mvGC   = array([bs.location(vel)[0],bs.scale(vel)[0]])

    # Saving the relevant results for this observation of this
    # perspective. The output should get updated to include more
    # stuff too...
    results.append([v[0],      # theta (polar angle)
                   v[1],       # phi (azimuthal angle)
                   k,          # instance
                   finAmp,     # final amplitude
                   finAxi,     # final rotation axis
                   finSig,     # final velocity dispersion
                   eigval,     # array of InerTensr eigenvalues
                   eigvec,     # Inertia Tensor eigenvectors
                   angmom,     # angular momentum vector
                   quad1,      # number of clusters in quadrant 1
                   quad2,      # number of clusters in quadrant 2
                   quad3,      # number of clusters in quadrant 3
                   quad4,      # number of clusters in quadrant 4
                   mvGC,       # the mean velocity of the mock GC system [km/s]
		               vsys,       # the systemic velocity of the main galaxy [km/s]
                   ee_Amp,     # error estimate for the amplitude [km/s]
                   ee_Axi,     # error estimate for the rotation axis [rad]
                   ee_Sig])    # error estimate for the vel disp [kms/]
                             


    # clean up
    del quad1, quad2, quad3, quad4, postSig, postAxi, postAmp
    del x, y, posAng, vel, rangeSig, rangeAxi, rangeAmp, err_vel
    del ee_Sig, ee_Axi, ee_Amp

  print
  print 'Analysis of observation ', observer, ' is complete.'
  print

# saving the list to disk with cPickle
cPickle.dump(results, open(database + 'WateryFlowRes.cPickle', 'wb'))

# Analyse the results, produce plots and latex friendly tables
WAnalysis(obsnet = obsnet,
          datadir = database,
          datafile = 'WateryFlowRes.cPickle',
          halo = halo)

datum1 = datetime.datetime.now()
print
print 'Watery successfully completed in: ', datum1 - datum
print 'The rent is too DAMN high!!!'
print
