# The Watery Project #
Now published: http://www.aanda.org/articles/aa/pdf/2016/08/aa28292-16.pdf

### The idea behind the project ###
The goal of this project is to quantify the amount of systemic net rotation found in mock globular cluster (GC) systems, that I generate around Milky Way sized galaxies. The idea behind the project is to test whether the detection of systemic net rotation of the GC system around the Andromeda galaxy is a statistical miracle, or such configurations can be indeed found with reasonable frequency, and under which conditions. 

### The data used ###
The data used comes from the Aquarius project, which is a suite of high resolution numerical simulations that follow the formation and growth of 6 dark matter halos in a hierarchical LCDM universe. Each halo comprises 10^8 dark matter particles, and has a total mass of 1-2x10^12Msun, values typically estimated for the Milky Way and Andromeda. The dynamical information of the dark matter particles has been coupled to GALFORM, a semi-analytic galaxy formation model (Bower+06), enabling us to track the baryonic content in these galaxies. In the final snapshot of the simulations, each halo has 4-6x10^5 tagged particles. This tagged dataset is what I use for the project. For more information on the particle tagging, see Cooper+10. Note the the Aquarius haloes are only built out of accreted material (streas), and they have no *in situ* component.

### The main work-flow of the project ###
This is the work-flow of the project:

1. Select only those particles that were brought by streams that have a stellar mass larger than 10^6 Msun;
2. Set the origin of the coordinate system of the halo to the centre of mass of that halo;
3. Scale the mass of the given halo to the mass of Aq-A. The scaling is done each position and velocity component on the particles of that halo. This allows us to work on haloes that have equal masses;
4. Set up a network of virtual observers around the halo in question. The points of view are defined in a spherical coordinate system, centred on the specific halo. 
5. Observe the halo in question from each virtual observer. This effectively transforms the 6D phase-space into 3D space: two spatial coordinates (standard, on tangential plane passing through the centre of the halo), and line-of-sight velocity;
6. Create 100 mock GC systems around the specific halo, as observed from a given virtual observer. The number of GCs generated depends on the mass of stellar streams that build up that halo, as well as stellar radial number density of that halo;
7. Fit a Bayesian kinematic model to the data (described in Veljanoski+2014), returning the posterior functions for the maximum rotational amplitude, rotation axis, and velocity dispersions. This function is written to run in parallel (for now, on 3 cores only);
8. Create a diagnostic and informative plot (see below) containing a visual representation of the halo along with the GC created, together with the results of the Bayesian model fitting analysis;
9. Run the aggregate results through a series of analysis functions, meant to quantify the frequency with which one observes a significant rotation signal, and these results are compared to the angular momentum of the GCs and of the complete halo, the rotation axis is compared to the principal axes of the dark mater halo and so on.

### The main files ###
* WateryFlow.py - This merges and runs everything;
* WateryModules.py - this contains the most important functions needed for this projects, most of which are outlined in the steps above;
* WFunctions.py - this contains all other custom function necessary for this project. Things like coordinate transformations, custom particle selectors, custom plotting and binning algorithms and functions, various analysis tests etc..

### Examples ### 
For an example, please consider these two following figures. 

The top figure shows and example of a highly rotating (A/sigma~1.4) mock GC system around Aq-A, as seen from one of the virtual observers. The GCs are shown as points, the colour of which corresponds to their line-of-sight velocities. The stellar component of the galaxy is shown by the gray histogram in the back.

The bottom figure contains 4 panels. The top left shows the line-of-sight velocities of the GCs as a function of their position angle. The best fit rotation curve is fitted through the plots according. The horizontal dashed line represents the systemic velocity of the halo. The remaining three plots (moving clockwise) display the posterior probability distribution functions for the amplitude, velocity dispersion, and rotation axis of the mock GC system as determined by our Bayesian machinery. The peak of each distribution and the 1-sigma uncertainties are indicated by the dashed and solid vertical lines, respectively. 



![fig31.jpg](https://bitbucket.org/repo/yn6rdL/images/1193379022-fig31.jpg)
![fig32.jpg](https://bitbucket.org/repo/yn6rdL/images/1947047783-fig32.jpg)