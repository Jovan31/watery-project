from WFunctions import *

def WResults(postAmp, rangeAmp, postAxi, rangeAxi, postSig, rangeSig,
             statistic = 'peak', backfeed = False):
    '''
    Displays the results of the BayesianKinematics2D.py code

    arguments:
    postAmp = posterior pdf of the amplitude
    postAxi = posterior pdf of the rotation axis
    postSig = posterior pdf of the velocity dispersion
    rangeAmp / rangeSig = the lattice of the pdfs of the amplitude and velocity
                          dispersion [km/s]
    rangeAxi = the lattice of the pdf of the rotation axis [rad]
    backfeed = boolean, chooses if certain values are returned or only printed

    Version: 0.1
    '''

    print
    print 'Displaying the results of the analysis...'
    print

    # Displaying the basic values
    amp = confidence(postAmp, rangeAmp, statistic = statistic)
    axi = (rad2deg(circularCase(postAxi, rangeAxi, statistic = 'peak')[0]),
           rad2deg(circularCase(postAxi, rangeAxi, statistic = 'peak')[1]))
    sig = confidence(postSig, rangeSig, statistic = statistic)

    print
    print 'Amplitude >>>', amp
    print 'Rota axis >>>', axi
    print 'Velo disp >>>', sig
    print

    if backfeed == True:
        return amp[0],axi[0],sig[0]

    elif backfeed == False:
        return
    else:
        print 'unexpected parameter assigned to /backfeed/ argument. Retry.'



################################################################################
def WResultsEE(postAmp, rangeAmp, postAxi, rangeAxi, postSig, rangeSig,
               statistic = 'peak'):
    '''
    Returns 'gaussian'-like mean error estimates for the relevant parameters

    arguments:
    postAmp = posterior pdf of the amplitude [km/s]
    postAxi = posterior pdf of the rotation axis [rad]
    postSig = posterior pdf of the velocity dispersion [km/s]
    rangeAmp / rangeSig = the lattice of the pdfs of the amplitude and velocity
                          dispersion [km/s]
    rangeAxi = the lattice of the pdf of the rotation axis [rad]
    '''

    # doing the primary calculations
    midAmp, lowAmp, highAmp = confidence(postAmp, rangeAmp,
                                            plot_friendly =True,
                                            statistic = statistic)
    midAxi, lowAxi, highAxi = circularCase(postAxi, rangeAxi,
                                            plot_friendly = True,
                                            statistic = 'peak')
    midSig, lowSig, highSig = confidence(postSig, rangeSig,
                                            plot_friendly = True,
                                            statistic = statistic)

    # the amplitude.. but it can go to Zero case so..
    if lowAmp[0] is None:
        ee_amp = highAmp[0]
    else:
        ee_amp = (highAmp[0] - lowAmp[0])*0.5

    # same for the velocity dispersion
    if lowSig[0] is None:
        ee_sig = highSig[0]
    else:
        ee_sig = (highSig[0] - lowSig[0])*0.5

    # now for the rotation aixs
    # (need to correct for the wrapping of the angles)
    ee_axi = (pi - abs(abs(lowAxi[0] - highAxi[0]) - pi))*0.5

    return ee_amp, rad2deg(ee_axi), ee_sig


################################################################################
def WHaloPlot(GCx, GCy, GCvel, GCvel_err, GCpa, vsys,
              postAmp, rangeAmp, postAxi, rangeAxi, postSig, rangeSig,
              halo, mtrans, oname = '/Users/users/jovan/Desktop/fig.pdf'):
    '''
    This plots the results of the main Watery code. The output is a pdf
    containing two pages. The first page shows a map of the system as seen by
    the observer, where the underlying stellar populations are shown, as well as
    the selected GCs which are colour coded according to their velocities.

    The dashed circles are only set to guide the eye, and they have radii
    of 30 and 100 kpc respectively.

    The output is saved on the Desktop.

    arguments:
    postAmp / postAxi / postSig = posterior pdfs of the amplitude, rotation
                                  axis, and velocity dispersion as
                                  determined by the BayesianKinematics2D.py
                                  code.
    rangeAmp/ rangeAxi / rangeSig = the grids (lattices) along which the
                                    amplitude, rotation axis, and the
                                    velocity dispersion are searched.
    GCx        = the x positions of the GCs in standard-like coords
    GCy        = the y positions of the GCs in standard-like coords
    GCvel      = velocities of the GCs
    GCvel_err  = the uncertainties in the GC velocities
    GCpa       = the position angle of the GCs, in radians
    vsys       = the systemic velocity of the galaxy
    halo       = which halo is being analysed, used to set up the path to
                 relevant input files
    mtrans     = the transformation matrix, used to project the dark matter
                 axis onto the plot
    oname      = output name (this will always be a .pdf file)

    '''

    print 'Plotting the halo...'

    # initializing the plot output
    plotoutput = PdfPages(oname)

    # Page One
    # the BigMap comes first to help visualise the system..
    wmapper(halo, mtrans, GCx, GCy, GCvel)
    plotoutput.savefig()

    # Page Two
    kinfig(GCpa, GCvel, GCvel_err, vsys,
           postAmp, rangeAmp, postAxi, rangeAxi, postSig, rangeSig)
    plotoutput.savefig()

    # close the document
    plotoutput.close()

    print 'Plot created successfully!'
    print



################################################################################
def WParticleSelector(input_database, output_database, threshold = 10**6.,
                      backfeed = False):
    '''
    Selects all particles that belong to stream which have total stellar
    mass larger than the threshold mass. The particles are treated individually,
    and the idea of using objects/classes is abandoned at this stage.

    The input database is needed to have the following columns in this order:
    pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, dm_id, str_id, str_mass.


    arguments:
    database = this is numpy binary file containing all the available data
               for a given Aquarius halo
    stream_save_filename = output dir+filename for the selected stream list
    threshold = above this value, streams are selected. Mass in solar masses
    backfeed = if True, the subroutine returns the properties of the particles.


    Version: 0.1
    '''

    print 'Starting WParticleSelector...'
    print 'Selecting particles belonging to streams more massive than',\
    '%e' % threshold, ' solar masses...'
    print

    fulldata = load(input_database)

    # Selecting data according to some condition. In this case, the primary
    # condition is the mass

    # Selecting only particles that belong to streams which have mass higher
    # than the threshold mass

    ind = fulldata[8] > threshold

    # reading in the particular subset of the data
    x = fulldata[0][ind]
    y = fulldata[1][ind]
    z = fulldata[2][ind]
    vel_x = fulldata[3][ind]
    vel_y = fulldata[4][ind]
    vel_z = fulldata[5][ind]
    dm_id = fulldata[6][ind]
    str_id = fulldata[7][ind]
    str_mass = fulldata[8][ind]

    # clean up
    del fulldata

    # saving the selected particles to disk, as a numpy array
    save(output_database, array([x,
                                 y,
                                 z,
                                 vel_x,
                                 vel_y,
                                 vel_z,
                                 dm_id,
                                 str_id,
                                 str_mass]))

    if backfeed == True:
        print 'WParticleSelector completed successfully.'
        print
        return array([x,y,z,vel_x,vel_y,vel_z,dm_id,str_id,str_mass])
    elif backfeed == False:
        print 'WParticleSelector completed successfully.'
        print
        return
    else:
        print '/backfeed/ argument must be set either to True or False.'


################################################################################
def WDataObservation(input_database, transformation_matrix, observer,
                     output_database, backfeed = True):
    '''
    This observes the data. Effectively, the positions of the particles are
    converted to "standard-like" coordinate system, and the observed
    line-of-sight velocity is calculated for them all.

    Other properties, such as position angle and projected radius are also
    calculated.

    arguments:
    input_database = numpy binary array containing the basic 9 properties of
                     of the particles as exported from the original simulation
    observer = array_like, the location of the observer in the 3D space
    transformation matrix, computed with the transformation_matrix function
    output_database = location of the results, stored as a numpy binary file
    backfeed = toggles whether the results are returned or only written to disk

    output: (array_like)
    the x,y standard coordinates;
    the line-of-sight velocities;
    the position angle for the particles (radians);
    the projected radius of the particles;
    the dm_id;
    the str_id;
    the str_mass.
    '''

    print 'Starting WDataObservation...'

    # reading in the data..
    data     = load(input_database)
    x        = data[0]
    y        = data[1]
    z        = data[2]
    vel_x    = data[3]
    vel_y    = data[4]
    vel_z    = data[5]
    dm_id    = data[6]
    str_id   = data[7]
    str_mass = data[8]

    # clean up..
    del data

    # number of particles being processed
    n = len(x)

    # initializing place-holders the new properties to be derived:
    los = zeros(n)
    xpos = zeros(n)
    ypos = zeros(n)

    for i in range(n):
        # calculating the line-of-sight velocities
        los[i] = los_velocity(array([x[i],y[i],z[i]]),
                              array([vel_x[i], vel_y[i], vel_z[i]]),
                              observer)
        # calculating the standard coordinates
        xpos[i], ypos[i] = coords_transform(transformation_matrix,
                                            array([x[i],y[i],z[i]]))

    # calculating the projected radius of the particles
    Rproj = (xpos**2.0 + ypos**2.0)**0.5
    # calculating the position angle of the particles (output in radians)
    posAng = posAngCal(xpos,ypos)

    # saving the observed particles to disk
    save(output_database, array([xpos,
                                 ypos,
                                 los,
                                 posAng,
                                 Rproj,
                                 dm_id,
                                 str_id,
                                 str_mass]))

    if backfeed == True:
        print 'WDataObservation completed successfully.'
        print
        return array([xpos, ypos, los, posAng, Rproj, dm_id, str_id,str_mass])
    elif backfeed == False:
        print 'WParticleSelector completed successfully.'
        print
        return
    else:
        print '/backfeed/ argument must be set either to True or False.'


################################################################################
def WGlobularSelector(input_database, output_database, backfeed = False):
    '''
    Here the particular GCs are selected. Each eligible stream is chosen to
    donate a number of GCs depending on its mass, and then the clusters are
    randomly chosen. The GCs are selected in such a way that their radial
    profile matches the radial profile of the M31 halo GCs.

    arguments
    input_database: this is numpy binary file containing the observed data
                    for all particles belonging to streams that are accepted
                    in the analysis

    output
    output_database: a numpy binary file saved to disk, containing the observed
                     data for the selected GCs


    Version: 0.1
    '''

    print 'Starting WGlobularSelector...'
    print

    # the database
    data = load(input_database)

    # selecting the data
    x        = data[0]
    y        = data[1]
    velocity = data[2]
    posAngle = data[3]
    Rproj    = data[4]
    dm_id    = data[5]
    str_id   = data[6]
    str_mass = data[7]

    # determining how much clusters each stream will donate, and how many
    # clusters are to be put in each of the pre-determined radial bins
    GCQuota, BinQuota, uni_str = GCbinner(input_database)

    # # Remnant from the testing phase, prints stuff,
    # might be useful at some point..
    # for i, stream in enumerate(uni_str):
    #     print   '%-10s %-13i %-10s %-10.1f %-10s %-10.1f %-10s %-1.1e' % \
    #             ('str id: ',
    #             stream,
    #             'rad extent: ',
    #             max(Rproj[str_id == stream]) - min(Rproj[str_id == stream]),
    #             'ang extent: ',
    #             max(posAngle[str_id==stream]) - min(posAngle[str_id==stream]),
    #             'str mass: ',
    #             uni_mass[i],)
    # sys.exit()

    # for i in range(30,150,20):
    #     mi = i
    #     ma = mi + 20
    #     print '*****'
    #     print mi, ma
    #     print
    #     for i,stream in enumerate(uni_str):
    #         print stream, len(dm_id[(str_id==stream)&(Rproj>=mi)&(Rproj<ma)])
    #     print
    # sys.exit()

    ## Here starts the master actual GC selector

    # This is where the properties of the selected GCs are to be stored
    x_sel = []
    y_sel = []
    vel_sel = []
    rad_sel = []
    pos_sel = []
    dm_sel = []
    str_sel = []
    mass_sel = []

    # copy the bin quota to create a counter, so the original is preserved
    BinQuotaCounter = BinQuota.copy()

    # needed for assignment of values to the sub-arrays, via Pepes trick
    len_GCQuota    = arange(len(GCQuota))

    # this checks if the streams have satisfied their quota or not
    ind_quota = GCQuota > 0

    # 1st loop, loop over all bins, so the clusters can be chosen per bin
    binmin = 30 # the innermost bin edge
    for i in range(len(BinQuota)):
        # setting up the bin edges and the centre
        binmax = binmin + 20
        binmid = (binmin + binmax) * 0.5

        # need to check if some streams disappear in the outer part so they
        # have to exhaust their quota faster
        ind_future  = []   # switch for the future
        ind_present = []   # switch for the present
        for j in uni_str:
            ind_temp_future = Rproj[str_id == j] >binmax
            ind_future.append(ind_temp_future.any())
            ind_temp_present = ((Rproj[str_id == j] >=binmin) &
                                (Rproj[str_id == j] <binmax))
            ind_present.append(ind_temp_present.any())

        # converting the selection indices into arrays for better handling
        ind_future  = array(ind_future)
        ind_present = array(ind_present)

        # only taking streams that are active
        ind_future[ind_quota == False]   = False
        ind_present[ind_quota == False]  = False

        # set a counter to know how many clusters to draw to fill each bin
        while BinQuotaCounter[i] > 0:

            # this is the master if statement that selects which streams are
            # eligible for donating a cluster in this particular while iteration
            if ((ind_quota[(ind_future == False)].any() == True)):
                # master index, includes the available streams to draw GC from
                ind_master = (ind_future == False) & ind_quota

            elif (ind_present == False).all() == True:
                print 'Bin devoid of streams reached...'
                print 'Continue command executed'
                BinQuotaCounter[i] -= 1

            else:
                # selecting the appropriate streams (active and available quota)
                ind_master = ind_present & ind_quota

            # Sometimes for whatever reason, the ind_master is not created.
            # This happens rarely but it still needs to be dealth with.
            # When this happens, a random noise is added, in which the cluster
            # this itteration is not being chosen.

            if 'ind_master' in locals():
                pass
            else:
                break

            # how many streams will not show up in the outer bins
            l = len(ind_future[ind_master])

            # If the code gets stuck in point where there are no more clusters
	        # to be drawn from in this current bin, but the quota is not
            # satisfied, this is a quick and dirty method of fixing it.
            # The code just continues, and at most only up to 2 clusters are
            # are missing. All in all, this case is hardly ever reached.
            if l == 0:
                break

            # the dice basically decides from which stream a cluster is chosen
            dice = rd.randint(0,l-1)

            # this stream donates a GC so reduce its quota
            GCQuota[len_GCQuota[ind_master][dice]] -= 1

            # Here the algorithm for choosing an actual cluster are set
            # For convenience, select which stream the picking is done from
            temp_str = uni_str[len_GCQuota[ind_master][dice]]
            # check how many particles exist in that stream, but also in the
            # appropriate radial range (in the current bin)
            idx_particles = (str_id==temp_str)&(Rproj>=binmin)&(Rproj< binmax)
            temp_num_part = len(dm_id[idx_particles])

            # The algorithm is not perfect, and sometimes the quota of a stream
            # is not exhausted in time. In that case, those streams just
            # deposit their remaining clusters anywhere, adding random noise
            # to the profile, which is fine, since it typically happens for
            # <3 clusters.
            if temp_num_part == 0:
                idx_particles = (str_id == temp_str )
                temp_num_part = len(dm_id[idx_particles])
                # print 'SPECIAL CASE REACHED'

            # rolling the dice, which particle in this stream to be chosen
            # toss is a synonym for dice, so I avoid repeating the same var
            toss = rd.randint(0,temp_num_part - 1)
            # temporarily choose a particle to be a GC
            temp_dm_id = dm_id[idx_particles][toss]
            # now check if this particular particle has already been chosen.
            # if it has been, choose again until you get a unique new selection
            ticket_master = 0 # This will prevent the code to get stuck if there
                              # are not enough particles in a particular bin...
            bouncer = False   # If the code does get stuck, then exit the
                              #relevan loops
            while ((temp_dm_id in dm_sel)==True and (bouncer==False)==True):
                ticket_master += 1
                toss = rd.randint(0,temp_num_part - 1)
                temp_dm_id = dm_id[idx_particles][toss]
                if ticket_master >31:
                    bouncer = True
                    # print 'Too many repetition in particle choosing...'
                    # print 'Bouncer is kicking you outside the loop...'
                    bouncer = True

            if bouncer == False:

                # now that a unique choice of a particle has been made, store
                # its relevant properties
                x_sel.append(x[dm_id == temp_dm_id][0])
                y_sel.append(y[dm_id == temp_dm_id][0])
                vel_sel.append(velocity[dm_id == temp_dm_id][0])
                rad_sel.append(Rproj[dm_id == temp_dm_id][0])
                pos_sel.append(posAngle[dm_id == temp_dm_id][0])
                dm_sel.append(dm_id[dm_id == temp_dm_id][0])
                str_sel.append(str_id[dm_id == temp_dm_id][0])
                mass_sel.append(str_mass[dm_id == temp_dm_id][0])

            else:
                pass

            # A cluster has been chosen, so lower the current bin quota
            BinQuotaCounter[i] -= 1

            # clean up
            del ind_master, dice, toss, idx_particles, l, temp_num_part
            del temp_dm_id, temp_str

            # checking the quota of the streams
            ind_quota = GCQuota > 0

        # cleanup
        del ind_future, ind_present

        # go to the next bin, basically.
        binmin = binmax

    # converting the lists of the selected GC to numpy arrays
    x_sel = array(x_sel)
    y_sel = array(y_sel)
    vel_sel = array(vel_sel)
    rad_sel = array(rad_sel)
    pos_sel = array(pos_sel)
    dm_sel = array(dm_sel)
    str_sel = array(str_sel)
    mass_sel = array(mass_sel)

    # The clusters have been chosen now, it is time to save the data to disk..
    save(output_database, array([x_sel,
                                 y_sel,
                                 vel_sel,
                                 rad_sel,
                                 pos_sel,
                                 dm_sel,
                                 str_sel,
                                 mass_sel]))

    if backfeed == True:
        print 'Returning the data for the selected GCs..'
        return array([x_sel,
                      y_sel,
                      vel_sel,
                      rad_sel,
                      pos_sel,
                      dm_id,
                      str_sel,
                      mass_sel])

        print 'WGlobularSelector completed successfully.'
        print

    elif backfeed == False:
        print 'WGlobularSelector completed successfully'
        print

    else:
        print 'Please set /backfeed/ to either True or False to avoid errors.'




################################################################################
def WGalaxyCentring(database3D):

    '''
    This module finds the most dense centre of particles in the current halo,
    and puts the centre of the halo/universe coordinate system at that point.

    The algorithm used here was largely provided by Sandy.

    Arguments:
    database3D = numpy binary array containing the basic 9 properties of the
                 particles as exported from the original simulation

    Returns:
    The input database3D is overwritten, with the coordinates x,y,z shifted
    to account for the re-centring. If an error is made, this can be remedied
    by running WParticleSelector again, which will give the original database3D
    back.
    '''

    print
    print 'Re-centring the stellar halo...'

    data     = load(database3D)
    x        = data[0]
    y        = data[1]
    z        = data[2]
    vel_x    = data[3]
    vel_y    = data[4]
    vel_z    = data[5]
    dm_id    = data[6]
    str_id   = data[7]
    str_mass = data[8]

    # clean up
    del data

    x_cm = sum(x)/len(x)
    y_cm = sum(y)/len(y)
    z_cm = sum(z)/len(z)

    x_shift = x - x_cm
    y_shift = y - y_cm
    z_shift = z - z_cm

    # Saving the selected particles to disk, as a numpy array
    # Recall that with this, the input database is overwritten
    save(database3D, array([x_shift,
                            y_shift,
                            z_shift,
                            vel_x,
                            vel_y,
                            vel_z,
                            dm_id,
                            str_id,
                            str_mass]))

    print
    print 'Re-centring completed!'
    print


################################################################################
def WAnalysis(obsnet , datadir, datafile, halo, sigma = 0.8):

    '''
    This module analyses the results of the main products of the WateryFlow
    code. It creates the values and figures of interest.

    The entire task is now outsorced to individual tasks that do the tasks.

    arguments:
    obsnet   = the list of observer location (each point has (theta,phi))
    datafile = the aggregate result of WateryFlow prior to any analysis
               (typically saved to disk as 'WateryFlowRes.npy')
    datadir  = the path to there the datafile is located on disk
    halo     = which halo the analysis is done one, used for saving stuff
    sigma    = to which significance should the quadrants be taken as deviating
               from the mean in terms of the number of clusters they contain.

    '''

    # open a text file to write the results in
    fres = open(datadir+'textRes.txt','w')
    print >>fres, 'Results of the analysis of halo ', halo
    print >>fres, ''

    # WAnalysisTask1(obsnet, datadir, datafile, halo, sigma, fres)
    # WAnalysisTask2(obsnet, datadir, datafile, halo, sigma, fres)
    # WAnalysisTask3(obsnet, datadir, datafile, halo, sigma)
    # WAnalysisTask4(obsnet, datadir, datafile, halo, sigma, fres)
    # WAnalysisTask5(obsnet, datadir, datafile, halo, sigma, fres)
    # WAnalysisTask6(obsnet, datadir, datafile, halo, sigma, fres)
    # WAnalysisTask7(obsnet, datadir, datafile, halo, sigma, fres)
    # WAnalysisTask8(obsnet, datadir, datafile, halo, sigma, fres)
    # WAnalysisTask9(obsnet, datadir, datafile, halo, sigma)
    # WAnalysisTask10(obsnet, datadir, datafile, halo, sigma)
    # WAnalysisTask11(obsnet, datadir, datafile, halo, sigma)
    # WAnalysisTask12(obsnet, datadir, datafile, halo, sigma)
    # WAnalysisTask13(obsnet, datadir, datafile, halo, sigma)

    # close the results file
    print >>fres, ''
    print >>fres, ''
    print >>fres, ''
    print >>fres, 'File created on: ', datetime.datetime.now()
    print >>fres, 'E-O-F'
    fres.close()

    print
    print 'WAnalysis complete successfully..'

################################################################################
def WPerspective(theta=arange(0.,180.,45.),
                 phi=arange(0.,180.,45.),
                 principalAxis = False,
                 datadir = '',
                 halo = ''):
    '''
    In this function the network of points is created. This are the locations
    of the points of view from where an Aquarius halo is to be observed.
    The points of view are defined in a spherical polar coordinate system
    centred on the Aquarius halo most dense point. The distance from which
    the halos are observed is not defined here, just the location of the
    observers on the surface of a sphere.

    There is a regular grid of points. Onto this grid, added are the positions
    of observers located along the principal axis.

    arguments
    datadir       = path to the main data location for the specific Aq halo
    principalAxis = specifies whether the principal axis to be added to the
                    network of points
    halo          = string, one letter, If principalAxis keyword is true,
                    this needs to be specified, it points towards the halo
                    being considered

    output
    obs           = list of observers containing the theta and phi coordinates
                    in degrees
    '''

    # location of the observer on the surface of the spehere (theta,phi)
    # this is the regular grid
    obs = [] # first make it a list, then it will get converted to an array

    # creating the observers...
    for i in theta:
        for j in phi:
            if (i == 0 or i == 180) & (j != 0):
                pass
            else:
                obs.append([i,j])

    # now, if needed, add the observers located along the principal axis..
    if principalAxis == True:
        print 'Adding observers along the principle axis of the halo..'
        pa              = load(datadir+'principalAxis/pa' + halo +'.npy')
        theta_principal = rint(rad2deg(pa[0]))
        phi_principal   = rint(rad2deg(pa[1]))

        # do a quick check in case something is wrong with the number of
        # principal axis..
        if ((len(theta_principal) == len(phi_principal)) and
            (len(theta_principal) == 3)):
            pass
        else:
            print '''There is something wrong with the Principal Axis.
            Check the input file'''
            sys.exit(1)

        for k in range(3):
            obs.append([theta_principal[k],phi_principal[k]])

    else:
        pass

    # convert the obs variable to array for easier handling
    obs = array(obs)
    print
    return obs

################################################################################
def WHalo2DBinner(database,nbins=300,R0=30,
                  spatialRange=([-150,150],[-150,150])):
    '''
    Written for the Waterfall code. The function takes the full observed
    database of the halo that contains each individual 'star'-like particle.
    The data is binned and the 10-itters 3-sigma mean for the input statistic
    (velocity) and individual arrays containing x,y positions and the relevant
    statistic (velocity). Also the uncertainty for the velocity in each 2d bin
    is returned. Originally written because applying BayesianKinematics2D on
    each star-like particle takes way to long for my taste.

    arguments
    database     = the numpy binary database that contains the 'observed' halo
                   from a given perspective
    nbins        = int, number of bins
    R0           = beyond this radius in kpc the halo starts.
    spatialRange = (2,2) array_like, the leftmost and rightmost edges of the
                   bins along each dimension (if not specified explicitly in the
                   bins parameters): [[xmin, xmax], [ymin, ymax]]. All values
                   outside of this range will be considered outliers and not
                   tallied in the histogram.


    returns
    posAng = the position angle of each bin
    vbin   = the sigma clipped mean of the relevant statistic
    ebin   = the standard deviation of the relevant statistic
    '''

    print 'Binning the halo..'

    # reading in the full data
    data = load(database)
    # reading in the projected radius of the particles
    Rproj = data[4]
    # define the selection index
    ind = Rproj >= R0

    # reading in the selected data
    x   = data[0][ind]
    y   = data[1][ind]
    vel = data[2][ind]

    # clean up
    del data

    # The initial binning, determines the bin edges and the value of the
    # relevant statistic in each bin
    value, xedges, yedges, binnumber = bin_st_2d(x,
                                                 y,
                                                 values = vel,
                                                 statistic = customclippedmean,
                                                 bins = nbins,
                                                 range = spatialRange)

    # now do the same thing, just to get the uncertainty in the statistic
    err, xtmp, ytmp, binnumber = bin_st_2d(x,
                                           y,
                                           values = vel,
                                           statistic = std,
                                           bins = nbins,
                                           range = spatialRange)

    # Now converting the histogram to scatter points data
    xbin = zeros(nbins*nbins)
    ybin = zeros(nbins*nbins)
    vbin = zeros(nbins*nbins)
    ebin = zeros(nbins*nbins)
    # index with which empty bins will be discarded
    ind = zeros(nbins*nbins)

    counter = 0

    for i in range(nbins-1):
        for j in range(nbins-1):
            xbin[counter] = (xedges[i] + xedges[i+1]) * 0.5
            ybin[counter] = (yedges[j] + yedges[j+1]) * 0.5
            vbin[counter] = value[i,j]
            ebin[counter] = err[i,j]
            ind[counter]  = isnan(value[i,j]) | isnan(err[i,j])
            counter      += 1

    # reformatting the arrays to contain only the fields that have values
    xbin = xbin[ind == False]
    ybin = ybin[ind == False]
    vbin = vbin[ind == False]
    ebin = ebin[ind == False]

    # since the remaining of the code will require the position angle only
    # this is calculated and returned instead of x,y coordinates

    posAng = posAngCal(xbin,ybin)

    # and that's it..
    # the things in brackets eliminates the uncertainties that are zero,
    # which removes any effectes that were not accounted for before.
    return posAng[ebin != 0.], vbin[ebin != 0.], ebin[ebin != 0.]

################################################################################
def WWholeHaloPlot(halo, posAng, vel, err_vel,
                   postAmp, rangeAmp, postAxi, rangeAxi, postSig, rangeSig,
                   mtrans, nbins = 300,
                   oname='/Users/users/jovan/Desktop/halo.pdf'):
    '''
    This plots the results of the Waterfall code. The output is a .pdf
    containing two pages. The first page shows a map of the halo as seen by
    the observer. The underlying population has been binned up, in the same
    manner as in the kinematic analysis. The bins are colour coded according
    to their velocity (10-itters, 3-sig clipped velocity).

    The dashed circles are only set the guide the eye, and they have radii of
    30 and 100 kpc respectively.

    The 2nd page contains the results from the Bayesian Analysis.

    The output is saved by default to the Desktop, but the user can specify
    an output name/directory.

    arguments
    halo                          = which halo is being analysed, used to set
                                    up the path to relevant input files
    posAng                        = position angle of the stars/clusters [rad]
    vel                           = velocities of the stars/clusters
    err_vel                       = uncertinties in the velocities
    postAmp / postAxi / postSig   = posterior pdfs of the amplitude, rotation
                                  axis, and velocity dispersion as
                                  determined by the BayesianKinematics2D.py
                                  code.
    rangeAmp/ rangeAxi / rangeSig = the grids (lattices) along which the
                                    amplitude, rotation axis, and the
                                    velocity dispersion are searched
    nbins                         = number of bins
    mtrans                        = the transformation matrix, used to project
                                    the dark matter axis onto the plot
    oname                         = the output name of the plot (always .pdf)

    returns
    figga = the figure

    '''

    print 'Plotting the halo...'

    # initializing the plot output
    plotoutput = PdfPages(oname)

    # Page One
    # The BigMap is created first, to visualise the system...
    wkinmapper(halo,nbins,mtrans)
    plotoutput.savefig()

    # Page two
    kinfighalo(posAng, vel, err_vel,
               postAmp, rangeAmp, postAxi, rangeAxi, postSig, rangeSig)
    plotoutput.savefig()

    # close the document
    plotoutput.close()

    print 'Plot created successfully!'
    print


################################################################################
# def WHaloAnalysis(obsnet,datadir,datafile,halo):

#     '''
#     This module analyses the output of the main part of Waterfall. It creates
#     the significant output, the actual result of the total process.

#     (1)

#     (2)

#     arguments:
#     obsnet   = the list of observer location (each point has (theta,phi))
#     datafile = the aggregate result of WateryFlow prior to any analysis
#                (typically saved to disk as 'WateryFlowRes.npy')
#     datadir  = the path to there the datafile is located on disk
#     halo     = which halo the analysis is done one, used for saving stuff

#     '''

#     # loading the data
#     data = load(datadir+datafile)

#     # Part (1): creating the latex results table

#     # define where the test
#     textdir = datadir + 'latexStuff/'

#     # opening the text file
#     fout = open(textdir + 'haloTable'+halo, 'w')

#     # printing the header
#     print >>fout, '%-15s %5s %-10s' % ('Perspective',
#                                        ' $ ',
#                                        'Amp/Sig')
#     print >>fout, ''

#     # the master loop begins
#     for i,v in enumerate(obsnet):
#         # selecting and reading in the relevant portion of the data
#         ind = (data[:,0] == v[0]) & (data[:,1] == v[1])



################################################################################
def WInertia(ids, database):

    '''
    Calculates the Inertia tensor for a system of particles - a mock GC system -
    and then returns the eigenvalues and eigenvectos of that tensor.

    The same function also calculates the angular momentum of the system.
    The returned values are the magnitude of the angular momentum and the
    'spin', the unit vector in the direction of the angular momentum vector.

    This function assumes that the velocities from the database are in [km/s]
    but that the positions are in kpc, and thus these are converted into [m].

    arguments:
    ids       = array_like, the ids of the dark matter particles
    database  = string, poing to the location of the original 3D database

    returns:
    eigval    = the (complex) eigenvalues of the inertia tensor of the GC system
    eigvec    = the normalized eigenvectos of the inertia of the GC system
    angmomMag = the magnitude of the angular momentum of the GC system [???]
    '''

    # loading the database
    fulldata = load(database)
    x        = fulldata[0]
    y        = fulldata[1]
    z        = fulldata[2]
    x_vel    = fulldata[3]
    y_vel    = fulldata[4]
    z_vel    = fulldata[5]
    dm_id    = fulldata[6]

    # identify which elements are to be retained
    ind      = in1d(dm_id,ids)

    # take only the appropriate elements
    x        = x[ind] #* 3.08567758 * 10.0**19.0
    y        = y[ind] #* 3.08567758 * 10.0**19.0
    z        = z[ind] #* 3.08567758 * 10.0**19.0
    x_vel    = x_vel[ind]
    y_vel    = y_vel[ind]
    z_vel    = z_vel[ind]

    # clean up
    del fulldata, dm_id, ind

    # converting the arrays of coordinates for the GCs into more conveniet
    # format: r = (x,y,z) ; v = (x_vel,y_vel,z_vel)
    r = zeros((3,len(x)))
    v = zeros((3,len(x)))
    for i,q in enumerate([x,y,z]):
        r[i] = q
    for i,q in enumerate([x_vel,y_vel,z_vel]):
        v[i] = q
    r = r.T
    v = v.T

    print
    print 'Calculating the eigenvalues and eigenvectos of the Inertia Tensor...'

    # Initializing the inertia tensor
    InertiaTensor = zeros((3,3))

    # Calculating the inertia tensor, this is the main point of this function
    for i in range(len(r)):
      tmp = dot(r[i],r[i])*identity(3) - (kron(r[i],r[i])).reshape(3,3)
      InertiaTensor += tmp

    # now find the eigenvalues and eigenvectors of the inertia tensor
    eigval, eigvec = linalg.eig(InertiaTensor)

    print 'Eigenvalues and eigenvectors calculated successfully.'
    print 'Calculating the angular momentum of the system...'

    angmom = zeros(3)
    for i in range(len(r)):
        tmp = cross(r[i],v[i])
        angmom += tmp

    # the angular momentum in all its glory
    # (multiplied by the mass of the particles - see if this is needed)
    angmom = angmom * len(v)

    # # calculating the magnitude of the angular momentum vector
    # angmomMag  = linalg.norm(angmom)

    # # calculating the 'spin', the unit vector in the direction of the
    # # angular momentum vector
    # spin = angmom/angmomMag

    print 'The angular momentum is calculating successfully.'
    print

    return eigval, eigvec, angmom


################################################################################
def WInertia_fullhalo(database, datafile, radlim = 30.):

    '''
    Similar to WInertia, calculates the Inertia tensor for the full halo, with
    an option to give a cutout in radial distance.

    The same function also calculates the angular momentum of the system.
    The returned values are the magnitude of the angular momentum and the
    'spin', the unit vector in the direction of the angular momentum vector.

    This function assumes that the velocities from the database are in [km/s]
    but that the positions are in kpc, and thus these are converted into [m].

    arguments:
    halo      = string, stating which halo is to be worked on
    database  = string, poing to the location of the original 3D database
    radlim    = float, beyond which in distance are the stuff to be calculated

    returns:
    eigval    = the (complex) eigenvalues of the inertia tensor of the GC system
    eigvec    = the normalized eigenvectos of the inertia of the GC system
    angmomMag = the magnitude of the angular momentum of the GC system [???]
    '''

    print
    print 'Reading, centering and selecting the appropriate particles..'

    fulldata  = load(database + datafile)
    x         = fulldata[0]
    y         = fulldata[1]
    z         = fulldata[2]
    vx        = fulldata[3]
    vy        = fulldata[4]
    vz        = fulldata[5]

    # clean up
    del fulldata

    # selecting IDs outside the 30 kpc limit
    r  = sqrt(x**2.0 + y**2.0 + z**2.0)
    ind = r >= radlim

    x   = x[ind]
    y   = y[ind]
    z   = z[ind]
    vx  = vx[ind]
    vy  = vy[ind]
    vz  = vz[ind]

    # clean up
    del r, ind

    print 'Calculating the eigenvalues and eigenvectos of the Inertia Tensor...'

    # converting the arrays of coordinates for the GCs into more conveniet
    # format: r = (x,y,z) ; v = (x_vel,y_vel,z_vel)
    r = zeros((3,len(x)))
    v = zeros((3,len(x)))
    for i,q in enumerate([x,y,z]):
        r[i] = q
    for i,q in enumerate([vx,vy,vz]):
        v[i] = q
    r = r.T
    v = v.T

    # Initializing the inertia tensor
    InertiaTensor = zeros((3,3))

    # Calculating the inertia tensor, this is the main point of this function
    for i in range(len(r)):
      tmp = dot(r[i],r[i])*identity(3) - (kron(r[i],r[i])).reshape(3,3)
      InertiaTensor += tmp

    # now find the eigenvalues and eigenvectors of the inertia tensor
    halo_eigval, halo_eigvec = linalg.eig(InertiaTensor)

    print 'Calculating the angular momentum of the system...'

    halo_angmom = zeros(3)
    for i in range(len(r)):
        tmp = cross(r[i],v[i])
        halo_angmom += tmp


    print 'Inertia tensor and angular momentum calculation succeeded!'
    print

    # clean up
    del InertiaTensor, tmp, r, v, x, y, z, vx, vy, vz

    return halo_eigval, halo_eigvec, halo_angmom




################################################################################
def WGalaxyScaling(database3D, output_database, halo):
    '''
    The function scales an Aquarius halo to the mass of Aq-A.
    The scaling is done on each position and velocity component.

    The scaling is always relative to Aq-A

    arguments:
    database3D      = the numpy binary containing the basic 9 properties of the
                      particles in the same format as the data file exported
                      from the original simulation.
    output_database = the scaled properties of the particles, same format as
                      the input database
    halo            = the Aq halo that is being scaled


    returns:
    A new numpy binary is saved to disk, with the scaled galaxy. The binary
    has the same format as the data file exported from the original simulation.

    '''

    print
    print 'Scaling Aq-'+halo+' to Aq-A ..'

    # to select which halo is to be scaled
    halolist = array(['A','B','C','D','E'])
    # the masses of each halo, needed for calculating the scaling constant
    masslist = array([1.84,0.82,1.77,1.74,1.19])
    # select which the relevant halo
    indhalo  = halolist == halo
    # calculate the scaling factor
    scaling_factor = (masslist[0]/masslist[indhalo][0])**(0.333)
    # for testing (for now)
    print 'The scaling factor is:', scaling_factor

    # reading in the data
    data = load(database3D)
    x        = data[0]
    y        = data[1]
    z        = data[2]
    vel_x    = data[3]
    vel_y    = data[4]
    vel_z    = data[5]
    dm_id    = data[6]
    str_id   = data[7]
    str_mass = data[8]

    # clean up
    del data

    # do the scaling
    xx        = x * scaling_factor
    yy        = y * scaling_factor
    zz        = z * scaling_factor
    vel_xx    = vel_x * scaling_factor
    vel_yy    = vel_y * scaling_factor
    vel_zz    = vel_z * scaling_factor

    # The scalling is done. Now save everything to disk, in the same format
    # as the input database
    save(output_database, array([xx,
                                 yy,
                                 zz,
                                 vel_xx,
                                 vel_yy,
                                 vel_zz,
                                 dm_id,
                                 str_id,
                                 str_mass]))

    print
    print 'Galaxy scaling completed!'
    print



################################################################################



